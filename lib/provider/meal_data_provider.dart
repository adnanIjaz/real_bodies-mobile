import 'package:flutter/cupertino.dart';
import 'package:real_bodies/models/meal.dart';

class MealDataProvider extends ChangeNotifier {
  Meal? _meal;
  double? _size;
  final double initialCalories;
  final double initialFat;
  final double initialProtein;
  final double initialCarbohydrates;

  MealDataProvider(Meal mealData)
      : initialCarbohydrates = double.parse(mealData.carbohydrates!),
        initialCalories = double.parse(mealData.calories!),
        initialFat = double.parse(mealData.fat!),
        initialProtein = double.parse(mealData.protein!) {
    _meal = mealData;
    _meal!.quantity = '1';
    _size = 100;
  }

  setQuantity(int quantity) {
    _meal!.quantity = quantity.toString();
    setCalories();
    notifyListeners();
  }

  setSize(double size) {
    _size = size;
    setCalories();
    notifyListeners();
  }

  setCalories() {
    String calories =
        (int.parse(meal!.quantity!) * (_size! * initialCalories / 100))
            .toStringAsFixed(1);
    String protein =
        (int.parse(meal!.quantity!) * (_size! * initialProtein / 100))
            .toStringAsFixed(1);
    String carbohydrates =
        (int.parse(meal!.quantity!) * (_size! * initialCarbohydrates / 100))
            .toStringAsFixed(1);
    String fat = (int.parse(meal!.quantity!) * (_size! * initialFat / 100))
        .toStringAsFixed(1);

    _meal!.calories = calories;
    _meal!.protein = protein;
    _meal!.carbohydrates = carbohydrates;
    _meal!.fat = fat;

    notifyListeners();
  }

  Meal? get meal => _meal;

  double? get size => _size;
}