import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/services/database.dart';
import 'package:real_bodies/services/date_time.dart';
import 'package:http/http.dart' as http;


class WeeklyProgressProvider extends ChangeNotifier{
  static const String TAG = 'Weekly_Progress_Provider';
  String? _week;
  List<WeekProgress>? _weekProgressSummary;

  final dbHelper = DatabaseHelper.instance;

  WeeklyProgressProvider(int? id) {
    getWeek();
    query(id);
  }

  void getWeek() async {
    _week = await DateUtility.getWeekDirect();
    notifyListeners();
  }

  void query(int? id) async {
    try {
      String week = await DateUtility.getWeekDirect();
      final response = await http.get(URL.weeklyProgress +
          "get_weekly_progress" +
          "&user_id=" +
          id.toString() +
          "&week=$week");
      print('$TAG Response body = ${response.body}');
      Iterable list = json.decode(response.body);
      _weekProgressSummary =
          list.map((model) => WeekProgress.fromJson(model)).toList();
    }
    catch(e) {
      print('$TAG $e');
    }
    //final allRows = await dbHelper.queryAllRows();
    var week = await DateUtility.getWeekDirect();
    final allRows = await dbHelper.getSearched(week);
    print('weekly_progress_provider: query all rows:');
//   _weekProgressSummary = List.generate(allRows.length, (i) {
//      return WeekProgress(
//        date: allRows[i]['date'],
//        goal: allRows[i]['goal'],
//        actual: allRows[i]['actual'],
//        remaining: allRows[i]['remaining']
//      );
//    });

    _weekProgressSummary!.forEach((f) => print(f.remaining));
    allRows.forEach((row) => print(row));
    notifyListeners();
  }

  String? get week => _week;

  List<WeekProgress>? get weekProgressSummary => _weekProgressSummary;
}

class WeekProgress {
  final String? date;
  final String? goal;
  final String? actual;
  final String? remaining;

  WeekProgress({this.date, this.goal, this.actual, this.remaining});

  factory WeekProgress.fromJson(Map<String, dynamic> json) {
    return WeekProgress(
        date: json['date'] ?? 'N/A',
        goal: json['goal'] ?? 'N/A',
        actual: json['actual'] ?? 'N/A',
        remaining: json['remaining'] ?? 'N/A');
  }

}