import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:real_bodies/models/exercise.dart';
import 'package:real_bodies/models/url.dart';
import 'package:http/http.dart' as http;


class ExerciseDiaryProvider extends ChangeNotifier {
  List<ExerciseModel> _exerciseList = [];
  List<ExerciseModel> _loggedExercises = [];

  ExerciseDiaryProvider(String id) {
    getLoggedExercises(id);
  }

  void getLoggedExercises(String id) async {
    try {
      print(id);
      print('exercise_diary_provider: reached');
      URL urldomain = URL();
      //var url = urldomain.domain + "get_exercise_plan";
      var url = urldomain.domain + "get_today_exercise";

      final response = await http.get(url + "&id=$id");
      print('ExerciseDiaryProvider getLoggedExercises Response:${response.body}');

      Iterable list = json.decode(response.body);
      print('yes');
      _exerciseList = list.map((model) => ExerciseModel.fromJson(model)).toList();
//      _exerciseList.forEach((f){
//       if(f.log=='1') {
//         _loggedExercises.add(f);
//       }
//      });
      notifyListeners();
    } catch(e) {
       print('Exercise Diary Provider exception: $e');
       notifyListeners();
    }
  }

  List<ExerciseModel> get loggedExercises => _exerciseList;

}