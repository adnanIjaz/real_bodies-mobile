import 'package:flutter/cupertino.dart';

class DateProvider extends ChangeNotifier {

  String? _createdAtDate;

  setCurrentDate(String? date) {
    _createdAtDate = date;
    notifyListeners();
  }

  String? get createdAtDate => _createdAtDate;
}