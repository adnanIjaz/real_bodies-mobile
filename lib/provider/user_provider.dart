

import 'package:real_bodies/models/user.dart';

class UserProvider  {
  late User _user;

  UserProvider(String name, int id, String calories, String weight) {
    _user = User(name: name, userID: id, calories: calories, weight: weight);
  }

  //New commit

  int? get id => _user.userID;

  String? get name => _user.name;

  String? get calories => _user.calories;

  String? get weight => _user.weight;
}