import 'package:flutter/cupertino.dart';

class ExerciseQueueProvider extends ChangeNotifier {
  bool _rest = true;

  String? _message;

  finishRest() {
    _rest = false;
    notifyListeners();
  }

  setMessage(String msg) {
    _message = msg;
    notifyListeners();
  }

  bool get rest => _rest;
}