import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:real_bodies/models/macro_nutrient.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/models/weekly_meal.dart';
import 'package:real_bodies/services/date_time.dart';

class FoodLogProvider extends ChangeNotifier {
  static const TAG = 'food_log_provider:';
  MacroNutrients _targetNutrients =
      MacroNutrients(proteins: 0, carbohydrates: 0, fats: 0);
  MacroNutrients _actualNutrients =
      MacroNutrients(proteins: 0, carbohydrates: 0, fats: 0);
  double _totalNutrients = 0;
  String? _days;
  String? _avgCalories;

  FoodLogProvider(int? id) {
    getNutrients(id);
    checkinfo(id);
  }

  void getDaysAndAvg(int? id) async {
    try {
      final response = await http
          .get(URL.urlmain + "get_day_average" + "&id=" + id.toString());
      print('$TAG getDayAndAvg: ${response.body}');
      var jsonResponse = json.decode(response.body);
      _days = jsonResponse[0]['days'].toString();
      _avgCalories =
          double.parse(jsonResponse[0]['average_calories']).toStringAsFixed(2);
    } catch (e) {
      print('$TAG getDaysAndAvg: $e');
    }
  }

  void getNutrients(int? id) async {
    try {
      print('$TAG fetch meal plan: $id');
      String date = await DateUtility.getCreatedAtDate();
      print('meal plan created at date: $date');
      //print('Time before call ${DateTime.now()}');
      final response2 = await http.get(URL.mealUrl +
          'get_nutrients' +
          "&user_id=$id" +
          "&week=" +
          DateUtility.getWeek(date));
      //print('Time after call ${DateTime.now()}');
      //print('Response2 body: ${response2.body}');
      //CustomPrint.printWrapped('response2:${response2.body}');
      var jsonResponse = json.decode(response2.body);
      var meal = jsonResponse['meal'];

      print(json.decode(json.encode(meal)));
      Iterable list = json.decode(json.encode(meal));

      List<WeeklyMealPlan> wmpl =
          list.map((model) => WeeklyMealPlan.fromJson(model)).toList();
      wmpl.forEach((f) {
        //
        // _targetNutrients.proteins += double.parse(f.proteins!);
        // _targetNutrients.fats += double.parse(f.fats!);
        // _targetNutrients.carbohydrates += double.parse(f.carbohydrate!);

        targetNutrients.proteins =
            targetNutrients.proteins! + double.parse(f.proteins!);

        targetNutrients.fats = targetNutrients.fats! + double.parse(f.fats!);
        targetNutrients.carbohydrates =
            targetNutrients.carbohydrates! + double.parse(f.carbohydrate!);
//      print(f.carbohydrate);
      });
      print('nutrients carbs ' + _targetNutrients.carbohydrates.toString());
      // if(this.mounted)

      _totalNutrients = _targetNutrients.proteins! +
          _targetNutrients.fats! +
          _targetNutrients.carbohydrates!;

      //  wmpl.forEach((f)=>print(f.type));
      notifyListeners();
    } catch (e) {
      print('Food Exercise Diary Error ouccured $e');
      //throw Exception("End of line error");

    }
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    try {
      if (int.parse(s) is int) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print('food_exercise_diary (isNumeric): cannot see for numerics');
      return false;
    }
  }

  void checkinfo(int? id) async {
    var urldomain = URL();
    try {
      var trainingWeek = await DateUtility.getWeekDirect();
      print("$TAG" + id.toString());
      //print(DateTime.now().toString());
      var url = urldomain.domain + "get_food_record";
      final response = await http.get(url + "&id=" + id.toString());
      print(url + "&id=" + id.toString());
      print('$TAG Response body :${response.body}');

      if (isNumeric(response.body)) {
        print('integer parsed');
        // takeCal='0';
        _actualNutrients.proteins = 0;
        _actualNutrients.carbohydrates = 0;
        _actualNutrients.fats = 0;
        // caloriesBurnt = response.body;
        //takeCal = (double.parse(takeCal) -  double.parse(caloriesBurnt)).toInt().toString();
        //remaining = (double.parse(widget.calorie) - double.parse(takeCal) + double.parse(caloriesBurnt)).toInt().toString();
        var date = DateUtility.getCurrentDateWithDbFormat();
//        bool check = await dbHelper.alreadyPresent(date);
//        if (check) {
//          print('$TAG: yes');
//          _update(takeCal,remaining);
//        }
//        else {
//          print('$TAG: No');
//          _insert(takeCal, remaining);
//        }
      } else if (response.body == "null") {
        // takeCal='0';
        _actualNutrients.proteins = 0;
        _actualNutrients.carbohydrates = 0;
        _actualNutrients.fats = 0;
        // print(takeCal+" val");
      } else {
        print('Food exercise diary Else has been entered ');
        var jsonResponse = json.decode(response.body);
        var takecalo = jsonResponse[0]["totalIntaked"];
        var calorieExercise = jsonResponse[0]["excerciseburned"];
        _actualNutrients.proteins =
            double.parse(jsonResponse[0]["totalConsumed_protein"]);
        _actualNutrients.fats =
            double.parse(jsonResponse[0]["totalConsumed_fats"]);
        _actualNutrients.carbohydrates =
            double.parse(jsonResponse[0]["totalConsumed_carbo"]);
        // takeCal = takecalo;
        //caloriesBurnt = calorieExercise;
//        if(caloriesBurnt == null || caloriesBurnt=="") {
//          caloriesBurnt ="0";
//        }
        //takeCal = (double.parse(takeCal) -  double.parse(caloriesBurnt)).toInt().toString();
        // await DBProvider.db.insertTarget(takeCal);
        print("dfsdfsdf" + takecalo);
        //remaining = (double.parse(widget.calorie) - double.parse(takeCal) + double.parse(caloriesBurnt)).toInt().toString();
        var date = DateUtility.getCurrentDateWithDbFormat();
        //bool check = await dbHelper.alreadyPresent(date);
//        if (check) {
//          print('$TAG: yes');
//          _update(takeCal,remaining);
//        }
//        else {
//          print('$TAG: No');
//          _insert(takeCal, remaining);
//        }
      }

//  setState(() {

//  });

      //refresh();
      notifyListeners();
    } catch (e) {
      print(" food exercise diary $e");
    }
  }

  MacroNutrients get targetNutrients => _targetNutrients;

  MacroNutrients get actualNutrients => _actualNutrients;

  double get totalNutrients => _totalNutrients;

  String? get days => _days;

  String? get avgCalories => _avgCalories;
}
