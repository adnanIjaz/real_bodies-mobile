import 'package:flutter/cupertino.dart';
import 'package:real_bodies/models/food.dart';
import 'package:real_bodies/pages/search_food.dart';

class FoodDataProvider extends ChangeNotifier {
  static const TAG = 'food data provider:';
  Food? _food;
  final double? initialCalories;
  final double? initialCarbohydrates;
  final double? initialProteins;
  final double? initialFats;
  final double? initialFatsPoly;
  final double? initialFatsSaturated;
  final double? initialFatsMono;
  final double? initialSodium;
  final double? initialSize;

  FoodDataProvider(Food foodData)
      : initialCalories = foodData.calories,
        initialProteins = foodData.proteins,
        initialCarbohydrates = foodData.carbohydrates,
        initialFats = foodData.fat,
        initialFatsMono = foodData.fatMono,
        initialFatsPoly = foodData.fatPoly,
        initialSize = foodData.size,
        initialSodium = foodData.sodium,
        initialFatsSaturated = foodData.fatSaturate{
    _food = foodData;
    print('$TAG initialCalories $initialCalories');
  }

  setQuantity(int quantity) {
    _food!.quantity = quantity;
    setCalories();
    notifyListeners();
  }

  setSize(double size) {
    _food!.size = size;
    setCalories();
    notifyListeners();
  }

  setCalories() {
    double calories =
        _food!.quantity! * (_food!.size! * initialCalories! / initialSize!);
    double proteins =
        _food!.quantity! * (_food!.size! * initialProteins! / initialSize!);
    double carbohydrates = _food!.quantity! *
        (_food!.size! * initialCarbohydrates! / initialSize!);
    double fats =
        _food!.quantity! * (_food!.size! * initialFats! / initialSize!);
    double fatsPoly =
        _food!.quantity! * (_food!.size! * initialFatsPoly! / initialSize!);
    double fatSaturated = _food!.quantity! *
        (_food!.size! * initialFatsSaturated! / initialSize!);
    double fatMono =
        _food!.quantity! * (_food!.size! * initialFatsMono! / initialSize!);
    double sodium =
        _food!.quantity! * (_food!.size! * initialSodium! / initialSize!);

    _food!.calories = calories;
    _food!.proteins = proteins;
    _food!.carbohydrates = carbohydrates;
    _food!.fat = fats;
    _food!.fatPoly = fatsPoly;
    _food!.fatSaturate = fatSaturated;
    _food!.fatMono = fatMono;
    _food!.sodium = sodium;

    notifyListeners();
  }

  Food? get food => _food;
//  double get initialCal => initialCalories;
//  double get initialCarb => initialCarbohydrates;
//  double get initialPro => initialProteins;
//  double get initialFat => initialFats;
//  double get initialFp => initialFatsPoly;
//  double get initialFs => initialFatsSaturated;
//  double get initialFm => initialFatsMono;
//  double get initialS => initialSodium;
}