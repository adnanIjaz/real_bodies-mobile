class RBUtility {
  static String getPreciseCalories(String calories) {
    try {
      double caloriesInDouble = double.parse(calories);
      return caloriesInDouble.toStringAsFixed(0);
    } catch(e) {
      print('Real bodies utilities getPreciseCalories $e');
      return 'e-n/a';
    }
  }
}