import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:real_bodies/realbodyui/stripe.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DateUtility{

  static String getCurrentDate(){
    var now =  DateTime.now().toString();
    DateTime todayDate = DateTime.parse(now);
    String formattedDate = DateFormat('EEEE ,MMM dd').format(todayDate);
    print(formattedDate);
    return formattedDate;
  }

  static String getCurrentDateWithDbFormat() {
    DateTime nowTime = DateTime.now();
    var formatter = DateFormat('yyyy-MM-dd');
    String formatted = formatter.format(nowTime);
    return formatted;
  }

  static DateTime convertDateFromString(String strDate){
    DateTime todayDate = DateTime.parse(strDate);
    print(todayDate);
    return todayDate;
  }

  static String chartDate(String strDate) {
    var dateBreak = strDate.split("-");
    String year = dateBreak[0].substring(2,4);
    String month = dateBreak[1];
    String day = dateBreak[2];

    String formattedDate = '$day/$month/$year';
    print(formattedDate);
    return formattedDate;
  }


  static String getWeek(String date) {
    int week;
    DateTime createdDate = DateTime.parse(date);
    print('dateutil created at date $createdDate');
    final date2 = DateTime.now();
    int difference = date2.difference(createdDate).inDays;
    print('difference $difference');
    if(difference < 7) {
      return '1';
    }
    else if(difference%7==0) {
      week = (difference~/7);
      print('week: $week');
      return week.toString();
    }
    else {
      week = (difference~/7) + 1;
      print('rounded week: $week');
      return week.toString();
    }
  }

  static Future<String> getWeekDirect() async{
    int week;
    var date = await getCreatedAtDate();
    DateTime createdDate = DateTime.parse(date);
    print('dateutil created at date $createdDate');
    final date2 = DateTime.now();
    int difference = date2.difference(createdDate).inDays;
    print('difference $difference');
    if(difference < 7) {
      return '1';
    }
    else if(difference%7==0) {
      week = (difference~/7);
      print('week: $week');
      return week.toString();
    }
    else {
      week = (difference~/7) + 1;
      print('rounded week: $week');
      return week.toString();
    }

  }

   static getCreatedAtDate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return bool
    String createdAtDate = prefs.getString('created_at');
    print('dateutil created at date loaded: $createdAtDate');
    return createdAtDate;
  }

  static String getTimeInMinutes(String time) {
    String minutes = (int.parse(time) ~/ 60).toString();
    String remainingSeconds = (int.parse(time) % 60).toInt().toString();
    if(remainingSeconds=='0'){
      remainingSeconds = '00';
    }
    return '$minutes:$remainingSeconds';
  }
}