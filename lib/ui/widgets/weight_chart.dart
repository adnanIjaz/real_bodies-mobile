import 'dart:math' as math;

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:real_bodies/services/date_time.dart';

class WeightChart extends StatefulWidget {
  final List? weightList;

  WeightChart({this.weightList});

  @override
  _WeightChartState createState() => _WeightChartState();
}

class _WeightChartState extends State<WeightChart> {
 // List myList =['09','09','March','09/98/77','09/98/77','09/98/77','09/98/77'];


  //double max;
  @override
  Widget build(BuildContext context) {
    List<BarChartGroupData> data = [];
    for (int i = 0; i < widget.weightList!.length; i++) {
      data.add(BarChartGroupData(x: i, barRods: [
        BarChartRodData(
            y: double.parse(widget.weightList![i][2]),
            color: Colors.lightBlueAccent)
      ], showingTooltipIndicators: [
        0
      ]));
    }
    print('this is the length of weightList ${widget.weightList!.length}');
    List weightDouble = [];
    for (int i = 0; i < widget.weightList!.length; i++) {
      weightDouble.add(double.parse(widget.weightList![i][2]));
    }
    double minN = weightDouble[0];
    double maxN = weightDouble[0];
    weightDouble.skip(1).forEach((b) {
      minN = minN.compareTo(b) >= 0 ? b : minN;
      maxN = maxN.compareTo(b) >= 0 ? maxN : b;
    });
    print(maxN);
    print(DateUtility.chartDate(widget.weightList![0][1]));
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        //height: 200,
        width:
            //700,
            100 * widget.weightList!.length.toDouble(),
        child: BarChart(
          BarChartData(
            alignment: BarChartAlignment.spaceEvenly,
            maxY: maxN + 60,
            barTouchData: BarTouchData(
              enabled: false,
              touchTooltipData: BarTouchTooltipData(
                tooltipBgColor: Colors.transparent,
                tooltipPadding: const EdgeInsets.all(0),
                tooltipBottomMargin: 8,
                getTooltipItem: (
                    BarChartGroupData group,
                    int groupIndex,
                    BarChartRodData rod,
                    int rodIndex,
                    ) {
                  return BarTooltipItem(
                    rod.y.round().toString(),
                    TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  );
                },
              ),
            ),
            titlesData: FlTitlesData(
              show: true,
              bottomTitles: SideTitles(
                showTitles: true,
                textStyle: TextStyle(
                    color: const Color(0xff7589a2), fontWeight: FontWeight.bold, fontSize: 14),
                margin: 20,
                getTitles: (double value) {
                  return DateUtility.chartDate(
                      widget.weightList![value.toInt()][1]);
                },
              ),
              leftTitles: SideTitles(showTitles: false),
            ),
            borderData: FlBorderData(
              show: false,
            ),
            //  groupsSpace: 100,
            barGroups: data,
          ),
        ),
      ),
    );
  }
}

//  List<LinearSales> data=[];
//
//   List<charts.Series<LinearSales, DateTime>> seriesList;
//   bool animate;
//
//   List<charts.Series<LinearSales, DateTime>> _createSampleData() {
//    // for(int i = 0; i<widget.weightList.length; i++){
//    //   data.add(LinearScale())
//    // }
//   // var data = [];
////     data=[
////
////      new LinearSales(new DateTime(2017, 9, 3), 25),
////      //new LinearSales(new DateTime(2017, 9, 4), 100),
////     ];
//
//
//     return [
//      charts.Series<LinearSales, DateTime>(
//        data: data,
//        id: 'Sales',
//        colorFn: (_, __) => charts. MaterialPalette.blue.shadeDefault,
//        domainFn: (LinearSales sales, _) => sales.year,
//        measureFn: (LinearSales sales, _) => sales.sales,
//      )
//     ];
//   }
//@override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    for(int i = 0; i<widget.weightList.length; i++){
//      print(widget.weightList[i][2]);
//    }
//    for(int i = 0; i<widget.weightList.length; i++){
//      data.add(LinearSales(DateUtility.convertDateFromString(widget.weightList[i][1]),int.parse(widget.weightList[i][2])));
//    }
//  }
//  @override
//  Widget build(BuildContext context) {
//      print("weight_chart: "+widget.weightList[0][1]);
//      seriesList = List<charts.Series<LinearSales, DateTime>>();
//     _createSampleData();
//    return new Container(
//      width: 200,
//      height: 200,
//      color: Colors.white,
//      child:
//    charts.TimeSeriesChart(_createSampleData(),
//        behaviors: [
//
//          charts.SlidingViewport(),
//          charts.PanAndZoomBehavior(),
//        ],
//        animate: false,
////      domainAxis: new charts.OrdinalAxisSpec(
////          viewport: new charts.OrdinalViewport('2018', 4)),
//
//    ),
//
//    );
//  }
//
//
//
//}
//
//
//
//
//
///// Sample linear data type.
//class LinearSales {
//  final DateTime year;
//  final int sales;
//
//  LinearSales(this.year, this.sales);
//}
