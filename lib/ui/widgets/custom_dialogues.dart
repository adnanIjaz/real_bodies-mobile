import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:real_bodies/provider/food_data_provider.dart';

class FoodDialogue extends StatefulWidget {

  @override
  _FoodDialogueState createState() => _FoodDialogueState();
}

class _FoodDialogueState extends State<FoodDialogue> {
     final servingTextController = TextEditingController();
  final quantityTextController = TextEditingController();
  String? initialValue;
  String? servingInitialValue;

  @override
  @override
  void dispose() {
    // TODO: implement dispose
    servingTextController.dispose();
    quantityTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
   // var serving = FoodValues.of(context).servings;
       initialValue = '1';
    quantityTextController.text = initialValue!;
    quantityTextController.selection =
        new TextSelection(baseOffset: 0, extentOffset: initialValue!.length);

    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      child: Consumer<FoodDataProvider>(builder: (context, provider, snapshot) {
        print('food dialogue ${provider.food!.size!.round()}');
        servingTextController.text = provider.food!.size!.round().toString();
        quantityTextController.text = provider.food!.quantity.toString();
        return Container(
          height: 150,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                        SizedBox(height: 15,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Serving '),
                            SizedBox(width: 10,),
                            Container(
                              height: 23,
                              width: 30,
                              child: Center(
                                child: TextField(
                                  decoration: InputDecoration(
                                    contentPadding:
                                    EdgeInsets.only(bottom: 14),
                                  ),
                                  textAlignVertical: TextAlignVertical.center,
                                  textAlign: TextAlign.center,
                                  controller: servingTextController,
                                  keyboardType: TextInputType.number,
                                  autofocus: true,
                                  onSubmitted: (text){
                                  //  print('food dialogue text submitted ${quantityTextController.text}');
                                   // provider.setQuantity(quantityTextController.text == '0' ? 1 : int.parse(quantityTextController.text));
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 20,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Quantity'),
                            SizedBox(width: 10,),
                            Container(
                              height: 23,
                              width: 30,
                              child: Center(
                                child: TextField(
                                  decoration: InputDecoration(
                                    contentPadding:
                                    EdgeInsets.only(bottom: 14),
                                  ),
                                  textAlignVertical: TextAlignVertical.center,
                                  textAlign: TextAlign.center,
                                  controller: quantityTextController,
                                  keyboardType: TextInputType.number,
                                  autofocus: true,
                                  onSubmitted: (text){
                                    Navigator.of(context).pop();
                                  } ,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 10,),
                FlatButton(child: Text('Okay'),
                  onPressed: (){
                    provider.setQuantity(quantityTextController.text == '0' ? 1 : int.parse(quantityTextController.text));
                    provider.setSize(servingTextController.text =='0' ? 100 : double.parse(servingTextController.text));
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        }
      ),
    );
  }
}
