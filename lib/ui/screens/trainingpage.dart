import 'package:flutter/material.dart';
import 'package:real_bodies/models/exercise.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/pages/video_example.dart';
import 'package:real_bodies/realbodyui/exercise_plan_full.dart';
import 'package:real_bodies/realbodyui/exercise_queue.dart';
import 'package:real_bodies/realbodyui/exercise_video_player.dart';
import 'package:real_bodies/services/date_time.dart';
import 'package:real_bodies/theme/palette.dart';
import 'package:real_bodies/ui/widgets/custom_flat_button.dart';
import 'package:http/http.dart' as http;
import 'package:real_bodies/config/extentions.dart';

class TrainingPage extends StatefulWidget {
  final int? id;
  final String? calories;
  final List<ExerciseModel>? exerciseList;
  final String? trainingDay;
  final String? weight;
  final String? week;

  // final Function() notifyParent;

  TrainingPage({
    this.week,
    this.weight,
    this.id,
    this.exerciseList,
    this.trainingDay,
    this.calories,
    //this.notifyParent
  });
  @override
  _TrainingPageState createState() => _TrainingPageState();
}

class _TrainingPageState extends State<TrainingPage> {
  Future<String>? week;

  @override
  void initState() {
    super.initState();
    week = DateUtility.getWeekDirect();
  }

  @override
  Widget build(BuildContext context) {
    //List<charts.Series> seriesList;
    bool animate;

//     DonutPieChart(this.seriesList, {this.animate});
//
//     /// Creates a [PieChart] with sample data and no transition.
//     factory DonutPieChart.withSampleData() {
//       return new DonutPieChart(
//         _createSampleData(),
//         // Disable animations for image tests.
//         animate: false,
//       );
//     }

//    List<charts.Series<LinearSales, int>> _createSampleData() {
//      final data = [
//        new LinearSales(0, 100),
//        new LinearSales(1, 75),
//        new LinearSales(2, 25),
//        new LinearSales(3, 5),
//      ];
//
//      return [
//        new charts.Series<LinearSales, int>(
//          id: 'Sales',
//          domainFn: (LinearSales sales, _) => sales.year,
//          measureFn: (LinearSales sales, _) => sales.sales,
//          data: data,
//        )
//      ];
//    }

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double abovePadding = MediaQuery.of(context).padding.top;

//    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//        statusBarColor: Colors.green
//    ));

            return Container(
              width: width,
              height: height - kToolbarHeight,
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 10,
                    child: ListView(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              // color: Colors.blue,
                              height: height * 0.12,
                              width: width * 1,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                //  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                children: <Widget>[
                                  Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        "${widget.trainingDay}",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 27.0),
                                      )),
                                  SizedBox(
                                    height: height * 0.02,
                                  ),
                                  Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                      //  "Week " + snapshot.data,
                                        "Week " + widget.week!,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 21.0),
                              )),
                                  Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        //  "Week " + snapshot.data,
                                        "Day " + widget.exerciseList![0].day.toString(),
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17.0),
                              )),

                                ],
                              ),
                            ),
                            Container(
                              // height: height * 0.61,
                              width: width * 1,
                              child: Column(
                                children: <Widget>[
                                  ListView.builder(
                                      shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: widget.exerciseList!.length,
                              itemBuilder: (BuildContext context, int index) {
                                return ExerciseCard(
                                    image: widget.exerciseList![index].image,
                                    calories: widget.calories,
                                    userId: widget.id,
                                    id: widget.exerciseList![index].id,
                                    name: widget.exerciseList![index].name,
                                    log: widget.exerciseList![index].log,
                                    index: index,
                                    reps: widget.exerciseList![index].sets,
                                    sets: widget.exerciseList![index].sets,
                                    weight: widget.weight,
                                    week: widget.week,
                                    duration:
                                        widget.exerciseList![index].duration);
                              }),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20),
                      child: Container(
                        width: width * 0.9,
                        // color: Colors.orange,
                        child: FlatButton(
                          color: Palette.mainPurple,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12.0),
                            child: FittedBox(
                              fit: BoxFit.contain,
                              child: Text(
                                "Start",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  decoration: TextDecoration.none,
                                  fontSize: 26,
                                  fontWeight: FontWeight.w800,
                                  fontFamily: "OpenSans",
                                ),
                              ),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                            side: BorderSide(
                              color: Colors.white,
                              width: 2,
                            ),
                          ),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => ExerciseQueue(exerciseList: widget.exerciseList,index: 0,userId: widget.id.toString(),calories: widget.calories,weight: widget.weight)));
                            // widget.notifyParent();
                          },
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );

          }

  }


class ExerciseCard extends StatefulWidget {
  final int? userId;
  final int? id;
  final String? weight;
  final String? name;
  final String? log;
  final String? reps;
  final String? sets;
  final String? duration;
  final int? index;
  final String? image;
  final String? week;
  final String? calories;

  ExerciseCard(
      {this.reps,
      this.calories,
      this.sets,
      this.index,
      this.userId,
      this.id,
      this.name,
      this.weight,
      this.log,
      this.duration,
      this.image,
      this.week});

  @override
  _ExerciseCardState createState() => _ExerciseCardState();
}

class _ExerciseCardState extends State<ExerciseCard> {
  bool _buttonLog = false;
  String getTimeInMinutes(String time) {
    String minutes = (int.parse(time) ~/ 60).toString();
    String remainingSeconds = (int.parse(time) % 60).toInt().toString();
    if (remainingSeconds == '0') {
      remainingSeconds = '00';
    }
    return '$minutes:$remainingSeconds min';
  }

  String getMainIndex() {
    int mainIndex = 1 + widget.index!;
    return mainIndex.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5.0,
      child: ListTile(
        title: Text(
          '${widget.name!.capitalize()}',
          style: TextStyle(fontSize: 19.0, fontWeight: FontWeight.bold),
        ),
        subtitle: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(''),
              ],
            ),
            Row(
              children: <Widget>[
                Text(getTimeInMinutes(widget.duration!)),
              ],
            ),
            Row(
              children: <Widget>[
                Text(""),
              ],
            ),
            Row(
              children: <Widget>[
                Container(
                  height: 40,
                  width: 70,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: CustomFlatButton(
                      title: "View",
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      textColor: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChewieDemo()),
                        );
                      },
                      splashColor: Colors.black12,
                      borderColor: Colors.white,
                      borderWidth: 2,
                      color: Palette.mainPurple,
                    ),
                  ),
                ),
                widget.log == "1"
                    ? Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Icon(Icons.done),
                      )
                    : Container(
                        height: 40,
                        width: 70,
                        child: FittedBox(
                          fit: BoxFit.contain,
                          child:

                              //    widget.log=='1'||_buttonLog?
                              //  Icon(Icons.done) :
                              CustomFlatButton(
                            title: "Log",
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            textColor: Colors.white,
                            onPressed: () {
//                          logExercise(widget.userId, this.widget.id);
//                          setState(() {
//                            _buttonLog = true;
//                          });
                            print('trainingpage user id = ${widget.userId}');
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ExerciseInfo(
                                        name: widget.name,
                                        calories: widget.calories,
                                        reps: widget.reps,
                                        sets: widget.sets,
                                        index: widget.index! + 1,
                                        userId: widget.userId.toString(),
                                        image: widget.image,
                                        exerciseId: widget.id.toString(),
                                        duration: widget.duration,
                                        weight: widget.weight,
                                        week: widget.week)),
                              );
                            },
                            splashColor: Colors.black12,
                            borderColor: Colors.white,
                            borderWidth: 2,
                            color: Palette.mainPurple,
                          ),
                        ),
                      ),
              ],
            )
          ],
        ),
        leading: Container(
          height: 20,
          width: 20,
          color: Palette.mainPurple,
          child: Center(
              child: Text(
            getMainIndex(),
            style: TextStyle(color: Colors.white),
          )),
        ),
        trailing: Icon(
          Icons.report,
          color: Colors.grey,
          size: 30.0,
        ),
        /* SizedBox(
                                    width: 50,
                                    height: 50,
                                    child: RaisedButton(
                                      child: const Icon(Icons.report_problem, color: Colors.grey),
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(30.0)),
                                      onPressed: () {
                                       // Navigator.push(context, MaterialPageRoute(builder: (context) => Schedule(id: widget.id)),);
                                      },
                                    ),
                                  ) */
      ),
    );
  }
}

void logExercise(int id, int exerciseId) async {
  URL urldomain = URL();
  print(exerciseId);
  var url = urldomain.domain + "update_exercise";

  final response = await http.get(
      url + "&id=" + id.toString() + "&exercise_id=" + exerciseId.toString());

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON.
    // r Post.fromJson(json.decode(response.body));
    print('Response body:${response.body}');
  } else {
    // If that call was not successful, throw an error.
    throw Exception('trainingPage:(ExerciseCard) Failed to save exercise');
  }
}
