import 'package:real_bodies/models/url.dart';

class ExerciseModel {
  int? id;
  String? name;
  String? log;
  int? day;
  String? reps;
  String? sets;
  String? duration;
  String? image;
  String? week;
  String? round;
  String? videoLink;

  ExerciseModel({this.videoLink,
    this.sets,
    this.reps,
    this.id,
    this.name,
    this.log,
    this.duration,
    this.image,
      this.week,
      this.day,
      this.round});

  factory ExerciseModel.fromJson(Map<String, dynamic> json) {
    int jsonDay = 0;
    try {
      jsonDay = int.parse(json['day']);
    } catch (e) {
      print('ExerciseModel day int error: $e');
    }
    return ExerciseModel(
        id: int.parse(json['id'] ?? '0'),
        name: json['exercise_name'] ?? 'n/a',
        videoLink: json['video_link'],
        log: json['log'] ?? 'n/a',
        reps: json['reps'] ?? '0',
        sets: json['sets'] ?? '0',
        duration: json['time'] ?? '0',
        round: json['round'],
        // image: URL.imageUrl+json['image']??'n/a',
        // week: json['week']??'n/a',
        day: jsonDay);
  }
}
