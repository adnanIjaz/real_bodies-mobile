class MacroNutrients {
  double? proteins;
  double? carbohydrates;
  double? fats;

  MacroNutrients({this.proteins, this.carbohydrates, this.fats});
}