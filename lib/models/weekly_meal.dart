

class WeeklyMealPlan {
  String? id;
  String? foodName;
  String? type;
  String? proteins;
  String? carbohydrate;
  String? fats;
  final List<Ingredients>? ingredients;

  WeeklyMealPlan(
      {this.id, this.foodName, this.type, this.ingredients, this.fats, this.proteins, this.carbohydrate});

  factory WeeklyMealPlan.fromJson(Map<String, dynamic> json){
    var list = json['ingredients'] as List;
    print(list.runtimeType);
    List<Ingredients> ingredientsList = list.map((i) => Ingredients.fromJson(i))
        .toList();
    return WeeklyMealPlan(
      foodName: json['food_name'] ?? 'n/a',
      type: json['type'] ?? 'none',
      proteins: json['protiens'],
      carbohydrate: json['carbs'],
      fats:         json['fats'],
      id:           json['id']??'n/a',
      ingredients:  ingredientsList,
    );
  }
}

class Ingredients{
  String? name;

  Ingredients({this.name});

  factory Ingredients.fromJson(Map<String, dynamic> json) {
    return Ingredients(
      name: json['name'] ?? 'n/a',
    );
  }
}