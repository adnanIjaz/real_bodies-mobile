class Meal {
  String? id;
  String? name;
  List<String>? ingredients;
  String? quantity;
  String? calories;
  String? carbohydrates;
  String? protein;
  String? fat;

  Meal({this.id,
    this.name,
    this.ingredients,
    this.quantity,
    this.fat,
    this.carbohydrates,
    this.calories,
    this.protein});

  factory Meal.fromJson(Map<String, dynamic> json) {
    var ingredientsFromJson = json['diet_list'] as List;
    List<String> ingredientsList = new List<String>.from(ingredientsFromJson);
    return Meal(
        id: json['id'],
        name: json['diet_title'],
        quantity: json['diet_qty'],
        calories: json['diet_calories'],
        carbohydrates: json['diet_carbs'],
        protein: json['diet_protein'],
        fat: json['diet_fat'],
        ingredients: ingredientsList);
  }
}
