
import 'dart:io';
import 'package:moor/moor.dart';
import 'package:moor/ffi.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;

part 'moor_database.g.dart';
// use the annotation below to generate the name of the class with the following name
// @DataClassName('somename')
class Foods extends Table {
  // you can use .call() as well this is the shorter syntax
  // auto increment also sets this as a primary key automatically
  // use , max: 50 for maximum length constraints
  IntColumn? get id => integer().autoIncrement()();

  TextColumn? get foodId => text().withLength(min: 1)();

  TextColumn? get name => text().withLength(min: 1)();

  TextColumn? get size => text().withLength(min: 1)();

  TextColumn? get quantity => text().withLength(min: 1)();

  TextColumn? get carbohydrates => text().withLength(min: 1)();

  TextColumn? get fats => text().withLength(min: 1)();

  TextColumn? get protein => text().withLength(min: 1)();

  TextColumn? get fatSaturated => text().withLength(min: 1)();

  TextColumn? get fatPoly => text().withLength(min: 1)();

  TextColumn? get fatMono => text().withLength(min: 1)();

  TextColumn? get sodium => text().withLength(min: 1)();

  TextColumn? get calories => text().withLength(min: 1)();


// the following code may use to create custom primary key but there may be a warning
//  @override
//  Set<Column> get primaryKey => {id, name};
}

@DataClassName("DMeal")
class Meals extends Table {
  // you can use .call() as well this is the shorter syntax
  // auto increment also sets this as a primary key automatically
  // use , max: 50 for maximum length constraints
  IntColumn? get id => integer().autoIncrement()();

  TextColumn? get foodId => text().withLength(min: 1)();

  TextColumn? get name => text().withLength(min: 1)();
}

LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'db.sqlite'));
    return VmDatabase(file);
  });
}

// the moor database class which is to be generated
@UseMoor(tables: [Foods, Meals])
class RealBodiesDatabase extends _$RealBodiesDatabase{

   RealBodiesDatabase() : super(_openConnection());

   @override
   int get schemaVersion => 5;

   Future<List<Food>> getAllFoods(int limit) => (select(foods)..orderBy([(f) => OrderingTerm(expression: f.id, mode: OrderingMode.desc)])..limit(limit)).get();
   Stream<List<Food>> watchAllFoods() => select(foods).watch();
   // you can use int as well as it will return the integer future
   Future insertFood(Food food) => into(foods).insert(food);

   Future<List<DMeal>> getAllMeals(int limit) => (select(meals)..orderBy([(m) => OrderingTerm(expression: m.id, mode: OrderingMode.desc)])..limit(limit)).get();
   Future insertMeal(DMeal dmeal) => into(meals).insert(dmeal);

}
