// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'moor_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Food extends DataClass implements Insertable<Food> {
  final int id;
  final String foodId;
  final String? name;
  final String size;
  final String quantity;
  final String carbohydrates;
  final String fats;
  final String protein;
  final String fatSaturated;
  final String fatPoly;
  final String fatMono;
  final String sodium;
  final String calories;

  Food({required this.id,
    required this.foodId,
    required this.name,
    required this.size,
    required this.quantity,
    required this.carbohydrates,
    required this.fats,
    required this.protein,
    required this.fatSaturated,
    required this.fatPoly,
    required this.fatMono,
    required this.sodium,
    required this.calories});

  factory Food.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return Food(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      foodId:
      stringType.mapFromDatabaseResponse(data['${effectivePrefix}food_id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      size: stringType.mapFromDatabaseResponse(data['${effectivePrefix}size']),
      quantity: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}quantity']),
      carbohydrates: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}carbohydrates']),
      fats: stringType.mapFromDatabaseResponse(data['${effectivePrefix}fats']),
      protein:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}protein']),
      fatSaturated: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}fat_saturated']),
      fatPoly: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}fat_poly']),
      fatMono: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}fat_mono']),
      sodium:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}sodium']),
      calories: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}calories']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || foodId != null) {
      map['food_id'] = Variable<String>(foodId);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String?>(name);
    }
    if (!nullToAbsent || size != null) {
      map['size'] = Variable<String>(size);
    }
    if (!nullToAbsent || quantity != null) {
      map['quantity'] = Variable<String>(quantity);
    }
    if (!nullToAbsent || carbohydrates != null) {
      map['carbohydrates'] = Variable<String>(carbohydrates);
    }
    if (!nullToAbsent || fats != null) {
      map['fats'] = Variable<String>(fats);
    }
    if (!nullToAbsent || protein != null) {
      map['protein'] = Variable<String>(protein);
    }
    if (!nullToAbsent || fatSaturated != null) {
      map['fat_saturated'] = Variable<String>(fatSaturated);
    }
    if (!nullToAbsent || fatPoly != null) {
      map['fat_poly'] = Variable<String>(fatPoly);
    }
    if (!nullToAbsent || fatMono != null) {
      map['fat_mono'] = Variable<String>(fatMono);
    }
    if (!nullToAbsent || sodium != null) {
      map['sodium'] = Variable<String>(sodium);
    }
    if (!nullToAbsent || calories != null) {
      map['calories'] = Variable<String>(calories);
    }
    return map;
  }

  FoodsCompanion toCompanion(bool nullToAbsent) {
    return FoodsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      foodId:
          foodId == null && nullToAbsent ? const Value.absent() : Value(foodId),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      size: size == null && nullToAbsent ? const Value.absent() : Value(size),
      quantity: quantity == null && nullToAbsent
          ? const Value.absent()
          : Value(quantity),
      carbohydrates: carbohydrates == null && nullToAbsent
          ? const Value.absent()
          : Value(carbohydrates),
      fats: fats == null && nullToAbsent ? const Value.absent() : Value(fats),
      protein: protein == null && nullToAbsent
          ? const Value.absent()
          : Value(protein),
      fatSaturated: fatSaturated == null && nullToAbsent
          ? const Value.absent()
          : Value(fatSaturated),
      fatPoly: fatPoly == null && nullToAbsent
          ? const Value.absent()
          : Value(fatPoly),
      fatMono: fatMono == null && nullToAbsent
          ? const Value.absent()
          : Value(fatMono),
      sodium:
          sodium == null && nullToAbsent ? const Value.absent() : Value(sodium),
      calories: calories == null && nullToAbsent
          ? const Value.absent()
          : Value(calories),
    );
  }

  factory Food.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Food(
      id: serializer.fromJson<int>(json['id']),
      foodId: serializer.fromJson<String>(json['foodId']),
      name: serializer.fromJson<String>(json['name']),
      size: serializer.fromJson<String>(json['size']),
      quantity: serializer.fromJson<String>(json['quantity']),
      carbohydrates: serializer.fromJson<String>(json['carbohydrates']),
      fats: serializer.fromJson<String>(json['fats']),
      protein: serializer.fromJson<String>(json['protein']),
      fatSaturated: serializer.fromJson<String>(json['fatSaturated']),
      fatPoly: serializer.fromJson<String>(json['fatPoly']),
      fatMono: serializer.fromJson<String>(json['fatMono']),
      sodium: serializer.fromJson<String>(json['sodium']),
      calories: serializer.fromJson<String>(json['calories']),
    );
  }

  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'foodId': serializer.toJson<String>(foodId),
      'name': serializer.toJson<String?>(name),
      'size': serializer.toJson<String>(size),
      'quantity': serializer.toJson<String>(quantity),
      'carbohydrates': serializer.toJson<String>(carbohydrates),
      'fats': serializer.toJson<String>(fats),
      'protein': serializer.toJson<String>(protein),
      'fatSaturated': serializer.toJson<String>(fatSaturated),
      'fatPoly': serializer.toJson<String>(fatPoly),
      'fatMono': serializer.toJson<String>(fatMono),
      'sodium': serializer.toJson<String>(sodium),
      'calories': serializer.toJson<String>(calories),
    };
  }

  Food copyWith({int? id,
    String? foodId,
    String? name,
    String? size,
    String? quantity,
    String? carbohydrates,
    String? fats,
    String? protein,
    String? fatSaturated,
    String? fatPoly,
    String? fatMono,
    String? sodium,
    String? calories}) =>
      Food(
        id: id ?? this.id,
        foodId: foodId ?? this.foodId,
        name: name ?? this.name,
        size: size ?? this.size,
        quantity: quantity ?? this.quantity,
        carbohydrates: carbohydrates ?? this.carbohydrates,
        fats: fats ?? this.fats,
        protein: protein ?? this.protein,
        fatSaturated: fatSaturated ?? this.fatSaturated,
        fatPoly: fatPoly ?? this.fatPoly,
        fatMono: fatMono ?? this.fatMono,
        sodium: sodium ?? this.sodium,
        calories: calories ?? this.calories,
      );
  @override
  String toString() {
    return (StringBuffer('Food(')
          ..write('id: $id, ')
          ..write('foodId: $foodId, ')
          ..write('name: $name, ')
          ..write('size: $size, ')
          ..write('quantity: $quantity, ')
          ..write('carbohydrates: $carbohydrates, ')
          ..write('fats: $fats, ')
          ..write('protein: $protein, ')
          ..write('fatSaturated: $fatSaturated, ')
          ..write('fatPoly: $fatPoly, ')
          ..write('fatMono: $fatMono, ')
          ..write('sodium: $sodium, ')
          ..write('calories: $calories')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          foodId.hashCode,
          $mrjc(
              name.hashCode,
              $mrjc(
                  size.hashCode,
                  $mrjc(
                      quantity.hashCode,
                      $mrjc(
                          carbohydrates.hashCode,
                          $mrjc(
                              fats.hashCode,
                              $mrjc(
                                  protein.hashCode,
                                  $mrjc(
                                      fatSaturated.hashCode,
                                      $mrjc(
                                          fatPoly.hashCode,
                                          $mrjc(
                                              fatMono.hashCode,
                                              $mrjc(
                                                  sodium.hashCode,
                                                  calories
                                                      .hashCode)))))))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Food &&
          other.id == this.id &&
          other.foodId == this.foodId &&
          other.name == this.name &&
          other.size == this.size &&
          other.quantity == this.quantity &&
          other.carbohydrates == this.carbohydrates &&
          other.fats == this.fats &&
          other.protein == this.protein &&
          other.fatSaturated == this.fatSaturated &&
          other.fatPoly == this.fatPoly &&
          other.fatMono == this.fatMono &&
          other.sodium == this.sodium &&
          other.calories == this.calories);
}

class FoodsCompanion extends UpdateCompanion<Food> {
  final Value<int> id;
  final Value<String> foodId;
  final Value<String?> name;
  final Value<String> size;
  final Value<String> quantity;
  final Value<String> carbohydrates;
  final Value<String> fats;
  final Value<String> protein;
  final Value<String> fatSaturated;
  final Value<String> fatPoly;
  final Value<String> fatMono;
  final Value<String> sodium;
  final Value<String> calories;
  const FoodsCompanion({
    this.id = const Value.absent(),
    this.foodId = const Value.absent(),
    this.name = const Value.absent(),
    this.size = const Value.absent(),
    this.quantity = const Value.absent(),
    this.carbohydrates = const Value.absent(),
    this.fats = const Value.absent(),
    this.protein = const Value.absent(),
    this.fatSaturated = const Value.absent(),
    this.fatPoly = const Value.absent(),
    this.fatMono = const Value.absent(),
    this.sodium = const Value.absent(),
    this.calories = const Value.absent(),
  });
  FoodsCompanion.insert({
    this.id = const Value.absent(),
    required String foodId,
    required String name,
    required String size,
    required String quantity,
    required String carbohydrates,
    required String fats,
    required String protein,
    required String fatSaturated,
    required String fatPoly,
    required String fatMono,
    required String sodium,
    required String calories,
  })  : foodId = Value(foodId),
        name = Value(name),
        size = Value(size),
        quantity = Value(quantity),
        carbohydrates = Value(carbohydrates),
        fats = Value(fats),
        protein = Value(protein),
        fatSaturated = Value(fatSaturated),
        fatPoly = Value(fatPoly),
        fatMono = Value(fatMono),
        sodium = Value(sodium),
        calories = Value(calories);
  static Insertable<Food> custom({
    Expression<int>? id,
    Expression<String>? foodId,
    Expression<String>? name,
    Expression<String>? size,
    Expression<String>? quantity,
    Expression<String>? carbohydrates,
    Expression<String>? fats,
    Expression<String>? protein,
    Expression<String>? fatSaturated,
    Expression<String>? fatPoly,
    Expression<String>? fatMono,
    Expression<String>? sodium,
    Expression<String>? calories,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (foodId != null) 'food_id': foodId,
      if (name != null) 'name': name,
      if (size != null) 'size': size,
      if (quantity != null) 'quantity': quantity,
      if (carbohydrates != null) 'carbohydrates': carbohydrates,
      if (fats != null) 'fats': fats,
      if (protein != null) 'protein': protein,
      if (fatSaturated != null) 'fat_saturated': fatSaturated,
      if (fatPoly != null) 'fat_poly': fatPoly,
      if (fatMono != null) 'fat_mono': fatMono,
      if (sodium != null) 'sodium': sodium,
      if (calories != null) 'calories': calories,
    });
  }

  FoodsCompanion copyWith({Value<int>? id,
    Value<String>? foodId,
    Value<String>? name,
    Value<String>? size,
    Value<String>? quantity,
    Value<String>? carbohydrates,
    Value<String>? fats,
    Value<String>? protein,
    Value<String>? fatSaturated,
    Value<String>? fatPoly,
    Value<String>? fatMono,
    Value<String>? sodium,
    Value<String>? calories}) {
    return FoodsCompanion(
      id: id ?? this.id,
      foodId: foodId ?? this.foodId,
      name: name ?? this.name,
      size: size ?? this.size,
      quantity: quantity ?? this.quantity,
      carbohydrates: carbohydrates ?? this.carbohydrates,
      fats: fats ?? this.fats,
      protein: protein ?? this.protein,
      fatSaturated: fatSaturated ?? this.fatSaturated,
      fatPoly: fatPoly ?? this.fatPoly,
      fatMono: fatMono ?? this.fatMono,
      sodium: sodium ?? this.sodium,
      calories: calories ?? this.calories,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (foodId.present) {
      map['food_id'] = Variable<String>(foodId.value);
    }
    if (name.present) {
      map['name'] = Variable<String?>(name.value);
    }
    if (size.present) {
      map['size'] = Variable<String>(size.value);
    }
    if (quantity.present) {
      map['quantity'] = Variable<String>(quantity.value);
    }
    if (carbohydrates.present) {
      map['carbohydrates'] = Variable<String>(carbohydrates.value);
    }
    if (fats.present) {
      map['fats'] = Variable<String>(fats.value);
    }
    if (protein.present) {
      map['protein'] = Variable<String>(protein.value);
    }
    if (fatSaturated.present) {
      map['fat_saturated'] = Variable<String>(fatSaturated.value);
    }
    if (fatPoly.present) {
      map['fat_poly'] = Variable<String>(fatPoly.value);
    }
    if (fatMono.present) {
      map['fat_mono'] = Variable<String>(fatMono.value);
    }
    if (sodium.present) {
      map['sodium'] = Variable<String>(sodium.value);
    }
    if (calories.present) {
      map['calories'] = Variable<String>(calories.value);
    }
    return map;
  }
}

class $FoodsTable extends Foods with TableInfo<$FoodsTable, Food> {
  final GeneratedDatabase _db;
  final String? _alias;

  $FoodsTable(this._db, [this._alias]);

  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn? _id;

  @override
  GeneratedIntColumn get id => _id ??= _constructId();

  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _foodIdMeta = const VerificationMeta('foodId');
  GeneratedTextColumn? _foodId;

  @override
  GeneratedTextColumn get foodId => _foodId ??= _constructFoodId();

  GeneratedTextColumn _constructFoodId() {
    return GeneratedTextColumn('food_id', $tableName, false, minTextLength: 1);
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn? _name;

  @override
  GeneratedTextColumn get name => _name ??= _constructName();

  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false, minTextLength: 1);
  }

  final VerificationMeta _sizeMeta = const VerificationMeta('size');
  GeneratedTextColumn? _size;

  @override
  GeneratedTextColumn get size => _size ??= _constructSize();

  GeneratedTextColumn _constructSize() {
    return GeneratedTextColumn('size', $tableName, false, minTextLength: 1);
  }

  final VerificationMeta _quantityMeta = const VerificationMeta('quantity');
  GeneratedTextColumn? _quantity;

  @override
  GeneratedTextColumn get quantity => _quantity ??= _constructQuantity();

  GeneratedTextColumn _constructQuantity() {
    return GeneratedTextColumn('quantity', $tableName, false, minTextLength: 1);
  }

  final VerificationMeta _carbohydratesMeta =
  const VerificationMeta('carbohydrates');
  GeneratedTextColumn? _carbohydrates;

  @override
  GeneratedTextColumn get carbohydrates =>
      _carbohydrates ??= _constructCarbohydrates();

  GeneratedTextColumn _constructCarbohydrates() {
    return GeneratedTextColumn('carbohydrates', $tableName, false,
        minTextLength: 1);
  }

  final VerificationMeta _fatsMeta = const VerificationMeta('fats');
  GeneratedTextColumn? _fats;

  @override
  GeneratedTextColumn get fats => _fats ??= _constructFats();

  GeneratedTextColumn _constructFats() {
    return GeneratedTextColumn('fats', $tableName, false, minTextLength: 1);
  }

  final VerificationMeta _proteinMeta = const VerificationMeta('protein');
  GeneratedTextColumn? _protein;

  @override
  GeneratedTextColumn get protein => _protein ??= _constructProtein();

  GeneratedTextColumn _constructProtein() {
    return GeneratedTextColumn('protein', $tableName, false, minTextLength: 1);
  }

  final VerificationMeta _fatSaturatedMeta =
  const VerificationMeta('fatSaturated');
  GeneratedTextColumn? _fatSaturated;

  @override
  GeneratedTextColumn get fatSaturated =>
      _fatSaturated ??= _constructFatSaturated();

  GeneratedTextColumn _constructFatSaturated() {
    return GeneratedTextColumn('fat_saturated', $tableName, false,
        minTextLength: 1);
  }

  final VerificationMeta _fatPolyMeta = const VerificationMeta('fatPoly');
  GeneratedTextColumn? _fatPoly;

  @override
  GeneratedTextColumn get fatPoly => _fatPoly ??= _constructFatPoly();

  GeneratedTextColumn _constructFatPoly() {
    return GeneratedTextColumn('fat_poly', $tableName, false, minTextLength: 1);
  }

  final VerificationMeta _fatMonoMeta = const VerificationMeta('fatMono');
  GeneratedTextColumn? _fatMono;

  @override
  GeneratedTextColumn get fatMono => _fatMono ??= _constructFatMono();

  GeneratedTextColumn _constructFatMono() {
    return GeneratedTextColumn('fat_mono', $tableName, false, minTextLength: 1);
  }

  final VerificationMeta _sodiumMeta = const VerificationMeta('sodium');
  GeneratedTextColumn? _sodium;

  @override
  GeneratedTextColumn get sodium => _sodium ??= _constructSodium();

  GeneratedTextColumn _constructSodium() {
    return GeneratedTextColumn('sodium', $tableName, false, minTextLength: 1);
  }

  final VerificationMeta _caloriesMeta = const VerificationMeta('calories');
  GeneratedTextColumn? _calories;

  @override
  GeneratedTextColumn get calories => _calories ??= _constructCalories();

  GeneratedTextColumn _constructCalories() {
    return GeneratedTextColumn('calories', $tableName, false, minTextLength: 1);
  }

  @override
  List<GeneratedColumn> get $columns =>
      [
        id,
        foodId,
        name,
        size,
        quantity,
        carbohydrates,
        fats,
        protein,
        fatSaturated,
        fatPoly,
        fatMono,
        sodium,
        calories
      ];
  @override
  $FoodsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'foods';
  @override
  final String actualTableName = 'foods';
  @override
  VerificationContext validateIntegrity(Insertable<Food> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('food_id')) {
      context.handle(_foodIdMeta,
          foodId.isAcceptableOrUnknown(data['food_id']!, _foodIdMeta));
    } else if (isInserting) {
      context.missing(_foodIdMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('size')) {
      context.handle(
          _sizeMeta, size.isAcceptableOrUnknown(data['size']!, _sizeMeta));
    } else if (isInserting) {
      context.missing(_sizeMeta);
    }
    if (data.containsKey('quantity')) {
      context.handle(_quantityMeta,
          quantity.isAcceptableOrUnknown(data['quantity']!, _quantityMeta));
    } else if (isInserting) {
      context.missing(_quantityMeta);
    }
    if (data.containsKey('carbohydrates')) {
      context.handle(
          _carbohydratesMeta,
          carbohydrates.isAcceptableOrUnknown(
              data['carbohydrates']!, _carbohydratesMeta));
    } else if (isInserting) {
      context.missing(_carbohydratesMeta);
    }
    if (data.containsKey('fats')) {
      context.handle(
          _fatsMeta, fats.isAcceptableOrUnknown(data['fats']!, _fatsMeta));
    } else if (isInserting) {
      context.missing(_fatsMeta);
    }
    if (data.containsKey('protein')) {
      context.handle(_proteinMeta,
          protein.isAcceptableOrUnknown(data['protein']!, _proteinMeta));
    } else if (isInserting) {
      context.missing(_proteinMeta);
    }
    if (data.containsKey('fat_saturated')) {
      context.handle(
          _fatSaturatedMeta,
          fatSaturated.isAcceptableOrUnknown(
              data['fat_saturated']!, _fatSaturatedMeta));
    } else if (isInserting) {
      context.missing(_fatSaturatedMeta);
    }
    if (data.containsKey('fat_poly')) {
      context.handle(_fatPolyMeta,
          fatPoly.isAcceptableOrUnknown(data['fat_poly']!, _fatPolyMeta));
    } else if (isInserting) {
      context.missing(_fatPolyMeta);
    }
    if (data.containsKey('fat_mono')) {
      context.handle(_fatMonoMeta,
          fatMono.isAcceptableOrUnknown(data['fat_mono']!, _fatMonoMeta));
    } else if (isInserting) {
      context.missing(_fatMonoMeta);
    }
    if (data.containsKey('sodium')) {
      context.handle(_sodiumMeta,
          sodium.isAcceptableOrUnknown(data['sodium']!, _sodiumMeta));
    } else if (isInserting) {
      context.missing(_sodiumMeta);
    }
    if (data.containsKey('calories')) {
      context.handle(_caloriesMeta,
          calories.isAcceptableOrUnknown(data['calories']!, _caloriesMeta));
    } else if (isInserting) {
      context.missing(_caloriesMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};

  @override
  Food map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Food.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $FoodsTable createAlias(String alias) {
    return $FoodsTable(_db, alias);
  }
}

class DMeal extends DataClass implements Insertable<DMeal> {
  final int id;
  final String? foodId;
  final String? name;

  DMeal({required this.id, required this.foodId, required this.name});

  factory DMeal.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return DMeal(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      foodId:
      stringType.mapFromDatabaseResponse(data['${effectivePrefix}food_id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || foodId != null) {
      map['food_id'] = Variable<String?>(foodId);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String?>(name);
    }
    return map;
  }

  MealsCompanion toCompanion(bool nullToAbsent) {
    return MealsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      foodId:
          foodId == null && nullToAbsent ? const Value.absent() : Value(foodId),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
    );
  }

  factory DMeal.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return DMeal(
      id: serializer.fromJson<int>(json['id']),
      foodId: serializer.fromJson<String>(json['foodId']),
      name: serializer.fromJson<String>(json['name']),
    );
  }

  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'foodId': serializer.toJson<String?>(foodId),
      'name': serializer.toJson<String?>(name),
    };
  }

  DMeal copyWith({int? id, String? foodId, String? name}) =>
      DMeal(
        id: id ?? this.id,
        foodId: foodId ?? this.foodId,
        name: name ?? this.name,
      );

  @override
  String toString() {
    return (StringBuffer('DMeal(')
      ..write('id: $id, ')..write('foodId: $foodId, ')..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(foodId.hashCode, name.hashCode)));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is DMeal &&
          other.id == this.id &&
          other.foodId == this.foodId &&
          other.name == this.name);
}

class MealsCompanion extends UpdateCompanion<DMeal> {
  final Value<int> id;
  final Value<String?> foodId;
  final Value<String?> name;

  const MealsCompanion({
    this.id = const Value.absent(),
    this.foodId = const Value.absent(),
    this.name = const Value.absent(),
  });

  MealsCompanion.insert({
    this.id = const Value.absent(),
    required String foodId,
    required String name,
  })
      : foodId = Value(foodId),
        name = Value(name);
  static Insertable<DMeal> custom({
    Expression<int>? id,
    Expression<String>? foodId,
    Expression<String>? name,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (foodId != null) 'food_id': foodId,
      if (name != null) 'name': name,
    });
  }

  MealsCompanion copyWith(
      {Value<int>? id, Value<String>? foodId, Value<String>? name}) {
    return MealsCompanion(
      id: id ?? this.id,
      foodId: foodId ?? this.foodId,
      name: name ?? this.name,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (foodId.present) {
      map['food_id'] = Variable<String?>(foodId.value);
    }
    if (name.present) {
      map['name'] = Variable<String?>(name.value);
    }
    return map;
  }
}

class $MealsTable extends Meals with TableInfo<$MealsTable, DMeal> {
  final GeneratedDatabase _db;
  final String? _alias;

  $MealsTable(this._db, [this._alias]);

  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn? _id;

  @override
  GeneratedIntColumn get id => _id ??= _constructId();

  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _foodIdMeta = const VerificationMeta('foodId');
  GeneratedTextColumn? _foodId;

  @override
  GeneratedTextColumn get foodId => _foodId ??= _constructFoodId();

  GeneratedTextColumn _constructFoodId() {
    return GeneratedTextColumn('food_id', $tableName, false, minTextLength: 1);
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn? _name;

  @override
  GeneratedTextColumn get name => _name ??= _constructName();

  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false, minTextLength: 1);
  }

  @override
  List<GeneratedColumn> get $columns => [id, foodId, name];

  @override
  $MealsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'meals';
  @override
  final String actualTableName = 'meals';
  @override
  VerificationContext validateIntegrity(Insertable<DMeal> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('food_id')) {
      context.handle(_foodIdMeta,
          foodId.isAcceptableOrUnknown(data['food_id']!, _foodIdMeta));
    } else if (isInserting) {
      context.missing(_foodIdMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};

  @override
  DMeal map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DMeal.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $MealsTable createAlias(String alias) {
    return $MealsTable(_db, alias);
  }
}

abstract class _$RealBodiesDatabase extends GeneratedDatabase {
  _$RealBodiesDatabase(QueryExecutor e)
      : super(SqlTypeSystem.defaultInstance, e);
  $FoodsTable? _foods;

  $FoodsTable get foods => _foods ??= $FoodsTable(this);
  $MealsTable? _meals;

  $MealsTable get meals => _meals ??= $MealsTable(this);

  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();

  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [foods, meals];
}
