import 'dart:collection';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'dart:async';
import 'package:audioplayers/audio_cache.dart';
import 'package:intl/intl.dart';
import 'package:real_bodies/models/exercise.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/realbodyui/dashboard.dart';
import 'package:real_bodies/realbodyui/exercise_queue.dart';

import 'package:real_bodies/theme/palette.dart';
import 'package:real_bodies/ui/screens/test_screen.dart';
import 'package:wakelock/wakelock.dart';

const alarmAudioPath = "sound/timeup.mp3";
const beepAudioPath = "sound/beep.mp3";
const oneSec = const Duration(seconds: 1);
const interval = const Duration(seconds: 5);

class QueueCountDownTimer extends StatefulWidget {
  // CountDownTimer ({Key key/*, this.duration}*/}) : super(key: key);          // Key take holds of state remove it to see if it affects
  final int? index;
  final List<ExerciseModel>? exerciseList;
  final String? userId;
  final String? calories;
  final String? weight;
  final int duration = 0;

  QueueCountDownTimer(
      {this.index, this.exerciseList, this.weight, this.calories, this.userId});

  final _QueueCountDownTimerState ct = new _QueueCountDownTimerState();

  @override
  _QueueCountDownTimerState createState() => _QueueCountDownTimerState();

  void startStopTimer() {
    // ct._startStopTimer();
  }
}

class _QueueCountDownTimerState extends State<QueueCountDownTimer>
    with TickerProviderStateMixin {
  static const TAG = 'Queue count down timer:';
  DateTime duration =
      new DateTime.fromMicrosecondsSinceEpoch(interval.inMicroseconds);
  DateFormat minutesSeconds = new DateFormat("ms");
  Timer? counterSeconds;
  late AnimationController _floatBtnAnimController;
  bool _isPlaying = false;
  bool _animationCompleted = false;
  bool beep1 = true;
  bool beep2 = true;
  bool beep3 = true;

  AnimationController? _animationController;
  static AudioCache player = new AudioCache();

  void logExercise(String? id, String exerciseId) async {
    URL urldomain = URL();
    print(exerciseId);
    var url = urldomain.domain + "update_exercise";

    final response = await http.get(
        url + "&id=" + id.toString() + "&exercise_id=" + exerciseId.toString());

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON.
      // r Post.fromJson(json.decode(response.body));
      print('Response body:${response.body}');
    } else {
      // If that call was not successful, throw an error.
      throw Exception('trainingPage:(ExerciseCard) Failed to save exercise');
    }
  }

  String get timerString {
    Duration duration =
        _animationController!.duration! * _animationController!.value;

    //DateTime duration1 = new DateTime.fromMillisecondsSinceEpoch(duration.inSeconds);
    //print("hjvjdfbsdhbfjsjbjhbjsbfjhgvbsfjbvjs"+duration1.toString());
    /* if(duration.toString()=="3"){
   player.play(beepAudioPath);
 } */

    if (_animationController!.value == 0.0) {
      player.play(alarmAudioPath);

      _animationCompleted = true;

      logExercise(
          widget.userId, widget.exerciseList![widget.index!].id.toString());
    }
    if (duration.inSeconds == 2 && beep1) {
      player.play(beepAudioPath);

      beep1 = false;
    }
    if (duration.inSeconds == 1 && beep2) {
      player.play(beepAudioPath);

      beep2 = false;
    }
    if (duration.inSeconds == 0 && beep3) {
      player.play(beepAudioPath);

      beep3 = false;
    }
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
    // return '$duration1';
  }

  @override
  void initState() {
    super.initState();
    // _actionTimer();

    _floatBtnAnimController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));

    _animationController = AnimationController(
      vsync: this,
      duration: Duration(
        seconds:
            // 5
            int.parse(widget.exerciseList![widget.index!].duration!),
      ),
    )..addListener(_stopAnimation);
    _animationController!.reverse(
        from: _animationController!.value == 0.0
            ? 1.0
            : _animationController!.value);

    /* _animationController.addStatusListener((status){
if (status == AnimationStatus.completed)
{
  setState(() {
     player.play(alarmAudioPath);

  });
 }
   }); */
  }

  @override
  void dispose() {
    _floatBtnAnimController.dispose();
    _animationController!.dispose();
    super.dispose();
  }

  void _stopAnimation() {
    if (_animationController!.isCompleted) {
      //  player.play(alarmAudioPath);
      beep1 = true;
      beep2 = true;
      beep3 = true;
      setState(() {
        _handleOnPressed();
      });
    }
  }

  void _handleOnPressed() {
    setState(() {
      _isPlaying = !_isPlaying;
      _isPlaying
          ? _floatBtnAnimController.forward()
          : _floatBtnAnimController.reverse();
    });
  }

  void handleTick() {
    print(duration);
    setState(() {
      duration = duration.subtract(oneSec);
      if (duration.second == 3) {
        player.play(beepAudioPath);
        //  stopTimer();
      }
      if (duration.second == 2) {
        player.play(beepAudioPath);
      }
      if (duration.second == 1) {
        player.play(beepAudioPath);
      }
      if (duration.second == 0) {
        player.play(alarmAudioPath);
        stopTimer();
      }
    });
  }

  void _actionTimer() {
    if (counterSeconds == null) {
      startTimer();
    } else if (counterSeconds!.isActive) {
      stopTimer();
    } else {
      startTimer();
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return AnimatedBuilder(
        animation: _animationController!,
        builder: (context, child) {
//          if(_animationCompleted) {
//            int count = 0;
//            Navigator.popUntil(context, (route) {
//              return count++ == 3;
//            });
//            print(
//                'Exercise plan full: ${widget.calories}, ${widget.userId}');
//            Navigator.of(context).pushReplacement(MaterialPageRoute(
//                builder: (context) => DashBoard(
//                    id: int.parse(widget.userId),
//                    calorie: widget.calories,
//                    indexnumber: 1)));
//          }
          return Container(
            height: 400,
            width: 400,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Center(
                    child: AnimatedBuilder(
                        animation: _animationController!,
                        builder: (context, child) {
                          if (!_animationCompleted) {
                            return SizedBox(
                              height: height * 0.055,
                              child: FittedBox(
                                child: FloatingActionButton(
                                    heroTag: 'startStopbtn',
                                    child: AnimatedIcon(
                                        icon: AnimatedIcons.play_pause,
                                        progress: _floatBtnAnimController),
                                    backgroundColor: Palette.mainPurple,
                                    foregroundColor: Colors.white,
                                    elevation: 5.0,
                                    onPressed: () {
                                      _handleOnPressed();
                                      if (_animationController!.isAnimating) {
                                        _animationController!.stop();
                                        //  stopTimer();
                                      } else {
                                        //              startTimer();
                                        // sleep(Duration(milliseconds: 1000));
                                        _animationController!.reverse(
                                            from: _animationController!.value ==
                                                    0.0
                                                ? 1.0
                                                : _animationController!.value);
                                      }
                                    }),
                              ),
                            );
                          } else {
                            print('The animation is completed in the builder');
//                              int count = 0;
//                              Navigator.popUntil(context, (route) {
//                                return count++ == 3;
//                              });
//                              print(
//                                  'Exercise plan full: ${widget.calories}, ${widget.userId}');
//                              Navigator.of(context).pushReplacement(MaterialPageRoute(
//                                  builder: (context) => DashBoard(
//                                      id: int.parse(widget.userId),
//                                      calorie: widget.calories,
//                                      indexnumber: 1)));
                            Timer(
                              Duration(milliseconds: 50),
                              () {
                                Wakelock.disable();
//                                    int count = 0;
//                                    Navigator.popUntil(context, (route) {
//                                      return count++ == 2;
//                                    });
//                                    print(
//                                        'Count Down Timer: ${widget.calories}, ${widget.userId} , ${widget.weight}');
//                                    Navigator.of(context).pushReplacement(MaterialPageRoute(
//                                        builder: (context) => DashBoard(
//                                            id: int.parse(widget.userId),
//                                            calorie: widget.calories,
//                                            weight: widget.weight,
//                                            indexnumber: 1)));
                                int queueIndex = widget.index! + 1;
                                //TODO: Here the logic has been changed test it thoroughly
                                if (queueIndex >
                                    widget.exerciseList!.length - 1) {
                                  int count = 0;
                                  Navigator.popUntil(context, (route) {
                                    return count++ == 1;
                                  });
                                  print(
                                      'Count Down Timer: ${widget.calories}, ${widget.userId} , ${widget.weight}');
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(
                                          builder: (context) => DashBoard(
                                              id: int.parse(widget.userId!),
                                              calorie: widget.calories,
                                              weight: widget.weight,
                                              indexnumber: 1)));
                                } else {
                                  Navigator.of(context)
                                      .pushReplacement(MaterialPageRoute(
                                          builder: (context) => ExerciseQueue(
                                                index: queueIndex,
                                                calories: widget.calories,
                                                weight: widget.weight,
                                                userId: widget.userId,
                                                exerciseList:
                                                    widget.exerciseList,
                                              )));
                                }
                              },
                            );

                            return FloatingActionButton(
                              heroTag: 'disabledStartStopbtn',
                              child: AnimatedIcon(
                                  icon: AnimatedIcons.play_pause,
                                  progress: _floatBtnAnimController),
                              backgroundColor: Palette.mainPurple,
                              foregroundColor: Colors.white,
                              elevation: 0.0,
                              onPressed: () {
                                int count = 0;
                                Navigator.popUntil(context, (route) {
                                  return count++ == 3;
                                });
                                print(
                                    'Exercise plan full: ${widget.calories}, ${widget.userId}');
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (context) => DashBoard(
                                            id: int.parse(widget.userId!),
                                            calorie: widget.calories,
                                            indexnumber: 1)));
                              },
                            );
                          }

//                            return FloatingActionButton.extended(
//                                onPressed: () {
//                                  if (_animationController.isAnimating)
//                                    _animationController.stop();
//                                  else {
//                                    _animationController.reverse(
//                                        from: _animationController.value == 0.0
//                                            ? 1.0
//                                            : _animationController.value);
//                                  }
//                                },
//
//                                icon: Icon(_animationController.isAnimating
//                                    ? Icons.pause
//                                    : Icons.play_arrow),
//                                label: Text(
//                                    _animationController.isAnimating ? "Pause" : "Play"));
                        }),
                  ),
                ),
                Expanded(
                  child: Center(
                    child: FittedBox(
                      fit: BoxFit.cover,
                      child: Stack(
                        children: <Widget>[
                          Container(
                            height: 200,
                            width: 200,
                            child: AnimatedBuilder(
                              animation: _animationController!,
                              builder: (BuildContext context, Widget? child) {
                                return CustomPaint(
                                  painter: CustomTimerPainter(
                                    animation: _animationController,
                                    backgroundColor: Palette.lightPurple,
                                    color: Palette.mainPurple,
                                  ),
                                );
                              },
                            ),
                          ),
                          // Text('${minutesSeconds.format(duration)}'),

                          Positioned.fill(
                            child: Center(
                              child: FittedBox(
                                fit: BoxFit.contain,
                                child: Text(
                                  //  _animationController.value==0.0?(timerString='0:00'):
                                  timerString,
                                  style: TextStyle(
                                      fontSize: 40.0, color: Colors.black),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  void startTimer() {
    if (duration.millisecondsSinceEpoch == 0) {
      duration =
          new DateTime.fromMicrosecondsSinceEpoch(interval.inMicroseconds);
    }
    counterSeconds = new Timer.periodic(oneSec, (Timer t) => handleTick());
  }

  void stopTimer() {
    counterSeconds!.cancel();
  }
}
//}

//class for painting

class CustomTimerPainter extends CustomPainter {
  CustomTimerPainter({this.animation, this.backgroundColor, this.color})
      : super(repaint: animation);

  final Animation<double>? animation;
  final Color? backgroundColor, color;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = backgroundColor!
      ..strokeWidth = 10.0
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);
    paint.color = color!;
    double progress = (1.0 - animation!.value) * 2 * math.pi;
    canvas.drawArc(Offset.zero & size, math.pi * 1.5, -progress, false, paint);
  }

  @override
  bool shouldRepaint(CustomTimerPainter old) {
    return animation!.value != old.animation!.value ||
        color != old.color ||
        backgroundColor != old.backgroundColor;
  }
}
