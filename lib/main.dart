import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:real_bodies/Data/moor_database.dart';
import 'package:real_bodies/models/url.dart';
import 'package:http/http.dart' as http;
import 'package:real_bodies/provider/date_provider.dart';
import 'dart:convert';
import 'package:real_bodies/realbodyui/dashboard.dart';
import 'package:real_bodies/realbodyui/signin.dart';
import 'package:real_bodies/realbodyui/splashscreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider<DateProvider>(create: (_) => DateProvider()),
  ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // SystemChrome.setPreferredOrientations([
    //   DeviceOrientation.portraitUp,
    //   DeviceOrientation.portraitDown,
    // ]);
    return Provider(
      create: (_) => RealBodiesDatabase(),
      child: MaterialApp(
        title: 'RealBodies',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(),
        //DashBoard(id: 1,calorie: '2480', name: 'dar', date: DateTime.now().toString(),indexnumber: 1,weight: '59', )
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  // MyHomePage({Key key, this.title}) : super(key: key);
  // always marked "final".

  //final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  Future<Null> checkIsLogin() async {
    String _token = "";
    String _email = "";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _token = prefs.getString("password");
    _email = prefs.getString("name");
    print(_token);
    print(_email);
    if (_token != "" && _token != null) {
      print("alreay login.");
      startlogin(_email, _token);
    } else {
      //replace it with the login page
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => new splashscreen()),
      );
    }
  }

  saveCreateAt(String date) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('created_at', date);
  }

  void startlogin(String _name, String _password) async {
    URL urldomain = URL();

    var url = urldomain.domain + "login";
    final response =
        await http.get(url + "&email=" + _name + "&password=" + _password);

    print('Response status: ${response.statusCode}');
    print('Response body login: ${response.body}');
    var jsonResponse = json.decode(response.body);

    var requestresponse = jsonResponse['response'];
    var date = jsonResponse['created_at'];

    if (requestresponse == "success") {
      print('main start login date: $date');
      Provider.of<DateProvider>(context, listen: false).setCurrentDate(date);
      saveCreateAt(date);
      // var image=urldomain.imgdomain.toString()+jsonResponse['image'];
      var name = jsonResponse['name'];
      var calorie = jsonResponse['calories'];
      var weight = jsonResponse['weight'];
      int id = int.parse(jsonResponse['id']);
      // var old=jsonResponse['old'];
      //var height=jsonResponse['height'];
      //var weight=jsonResponse['weight'];
      // Navigator.of(context).pushReplacementNamed('/home');
      print("Login started");
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => DashBoard(
                id: id,
                indexnumber: 0,
                name: name,
                weight: weight,
                calorie: calorie,
                date: date)),
      );
    } else if (requestresponse == "error") {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => new Signin()),
      );

      print("error login");
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkIsLogin();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Center(child: CircularProgressIndicator()),
    );
  }
}
