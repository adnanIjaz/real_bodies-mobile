import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:real_bodies/config/search_utility.dart';
import 'package:real_bodies/models/food.dart';
import 'package:real_bodies/models/meal.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/realbodyui/food/meal_details.dart';
import 'package:real_bodies/realbodyui/search_add_food.dart';
import 'package:real_bodies/theme/my_flutter_app_icons.dart';
import 'package:real_bodies/theme/palette.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:real_bodies/Data/moor_database.dart' as hFood;

import 'package:real_bodies/ui/widgets/scale_route.dart';

class ColoredTabBar extends Container implements PreferredSizeWidget {
  ColoredTabBar({this.color, this.tabBar});

  final Color? color;
  final TabBar? tabBar;

  @override
  Size get preferredSize => tabBar!.preferredSize;

  @override
  Widget build(BuildContext context) =>
      Container(
        color: color,
        child: tabBar,
      );
}

class SearchFood extends StatefulWidget {
  final String? category;
  final int? id;
  final String? calorie;
  final String? incalorie;
  final String? caloriesBurnt;
  final String? remaining;
  final String? weight;

  SearchFood({this.category,
    this.id,
    this.calorie,
    this.incalorie,
    this.weight,
    this.remaining,
    this.caloriesBurnt});

  @override
  _SearchFoodState createState() => _SearchFoodState();
}

class _SearchFoodState extends State<SearchFood> with TickerProviderStateMixin {
  Future<List<hFood.Food>>? historyFood;
  List<String?> kFoods;
  late CustomSearchDelegate _delegate;
  late CustomSearchDelegate2 _delegate2;
  int tabIndex = 0;

  URL urldomain = URL();

  _SearchFoodState()
      : kFoods = [
    'apple',
    'mango',
    'banana',
    'orange',
          'lays',
          'eggs',
          'oatmeal'
        ]..sort(
        (w1, w2) => w1!.toLowerCase().compareTo(w2!.toUpperCase()),
          ),
        super();

  void checkinfo() async {
    try {
      var url = "http://realbodies.com.au/api/food.php?f=get_food";
      final response = await http.get(url);
      print('Response body:${response.body}');
      var jsonResponse = json.decode(response.body);
      // var requestresponse=jsonResponse['response'];

      for (int i = 0; i <= 1; i++) {
        kFoods.add(jsonResponse[i]['name']);
      }
      print(kFoods);

      /*  if (requestresponse=="success")
{
 var image=urldomain.imgdomain.toString()+jsonResponse['image'];
  var name=jsonResponse['name'];
   var gender=jsonResponse['gender'];
    var old=jsonResponse['old'];
     var height=jsonResponse['height'];
      var weight=jsonResponse['weight'];

 Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Desktop(image:image,name:name,gender:gender,old:old,height:height,weight:weight )),
  );
  }
else if(requestresponse=="error")
{

  print("error login");
} 

 */
    } catch (e) {
      print("Exception on way $e");
    }
  }

  odd() {
    setState(() {
      print('Search Food in page index ${_controller!.index}');
      tabIndex = _controller!.index;
    });
  }

  TabController? _controller;

  @override
  void initState() {
    super.initState();
    print('search food init ${widget.incalorie}');
    _delegate2 = CustomSearchDelegate2(
        kFoods,
        widget.id,
        widget.category,
        widget.calorie,
        widget.incalorie,
        widget.weight,
        widget.caloriesBurnt,
        widget.remaining);

    _delegate = CustomSearchDelegate(
        kFoods,
        widget.id,
        widget.category,
        widget.calorie,
        widget.incalorie,
        widget.weight,
        widget.caloriesBurnt,
        widget.remaining);
    // checkinfo();
    historyFood = getFoodHistory();
    _controller = TabController(length: 2, vsync: this)
      ..addListener(() {
        odd();
      });
  }

  Future<List<hFood.Food>> getFoodHistory() async {
    print('entered');
    final database =
        Provider.of<hFood.RealBodiesDatabase>(context, listen: false);
    Future<List<hFood.Food>> fList = database.getAllFoods(10);
    return fList;
  }

  Future<List<hFood.DMeal>> getMealHistory() async {
    final database =
        Provider.of<hFood.RealBodiesDatabase>(context, listen: false);
    Future<List<hFood.DMeal>> mList = database.getAllMeals(10);
    return mList;
  }

  @override
  Widget build(BuildContext context) {
    // print('this is ${DefaultTabController.of(context).index}');
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Palette.mainPurple,
            title: Text('Search'),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () async {
                  final String? selected = await showSearch<String?>(
                    context: context,
                    delegate: (tabIndex == 0) ? _delegate : _delegate2,
                  );
                  if (selected != null) {
                    Scaffold.of(context).showSnackBar(
                      SnackBar(
                        content: Text('You have selected the word: $selected'),
                      ),
                    );
                  }
                },
              ),
            ],
            bottom: ColoredTabBar(
              color: Colors.white,
              tabBar: TabBar(
                controller: _controller,
                unselectedLabelColor: Colors.grey,
                tabs: <Widget>[
                  Tab(
                    text: 'Food',
                  ),
                  Tab(
                    text: 'Meals',
                  ),
                ],
                indicatorSize: TabBarIndicatorSize.tab,
                indicator: new BubbleTabIndicator(
                  indicatorHeight: 25.0,
                  indicatorColor: Palette.mainPurple,
                  tabBarIndicatorSize: TabBarIndicatorSize.tab,
                ),
              ),
            ),
          ),
          body: TabBarView(
            controller: _controller,
            children: <Widget>[
              FutureBuilder<List>(
                  future: getFoodHistory(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                          itemCount: snapshot.data!.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ListTile(
                              leading: Icon(Icons.history),
                              title: Text(snapshot.data![index].name),
                              onTap: () {
                                print(snapshot.data![index].foodId);
                                print(snapshot.data![index].quantity);
                                int quantity = 1;
                                if (snapshot.data![index].quantity == null) {
                                  quantity = 1;
                                } else {
                                  quantity =
                                      int.parse(snapshot.data![index].quantity);
                                }

                                Food historyFood = Food(
                                  id: int.parse(snapshot.data![index].foodId),
                                  quantity: quantity,
                                  name: snapshot.data![index].name,
                                  size: double.parse(
                                      snapshot.data![index].size),
                                  fat: double.parse(snapshot.data![index].fats),
                                  fatSaturate: double.parse(
                                      snapshot.data![index].fatSaturated),
                                  fatPoly: double.parse(
                                      snapshot.data![index].fatPoly),
                                  fatMono: double.parse(
                                      snapshot.data![index].fatMono),
                                  carbohydrates: double.parse(
                                      snapshot.data![index].carbohydrates),
                                  proteins: double.parse(
                                      snapshot.data![index].protein),
                                  sodium:
                                  double.parse(snapshot.data![index].sodium),
                                  calories: double.parse(
                                      snapshot.data![index].calories),
                                );
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SearchAddFood(
                                              food: historyFood,
                                              id: widget.id,
                                              category: widget.category,
                                              calorie: widget.calorie,
                                              incalorie: widget.incalorie,
                                              weight: widget.weight,
                                              remaining: widget.remaining,
                                              caloriesBurnt:
                                                  widget.caloriesBurnt,
                                              meal: true,
                                            )));
                              },
                            );
                          });
                    }
                    return Container();
                  }),
              FutureBuilder<List>(
                  future: getMealHistory(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                          itemCount: snapshot.data!.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ListTile(
                              leading: Icon(Icons.history),
                              title: Text(snapshot.data![index].name),
                              onTap: () async {
                                print(snapshot.data![index].foodId);
                                Meal? historyFood = await getASingleMeal(
                                    snapshot.data![index].foodId);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            AddMeal(
                                              mealFood: historyFood,
                                              id: widget.id,
                                              category: widget.category,
                                              calorie: widget.calorie,
                                              incalorie: widget.incalorie,
                                              weight: widget.weight,
                                              remaining: widget.remaining,
                                              caloriesBurnt:
                                                  widget.caloriesBurnt,
                                              meal: true,
                                            )));
                              },
                            );
                          });
                    }
                    return Container();
                  }),
            ],
          )
          //Center(child: Container(child: Text('No food added press search button'),)),
//      Scrollbar(
////    child: ListView.builder(
////    itemCount: kFoods.length,
////            itemBuilder: (context, idx) => ListTile(
////              title: Text(kFoods[idx]),
////            ),
////    ),
//
//    ),

          ),
    );
  }
}

Future<Meal?> getASingleMeal(String id) async {
  try {
    var url = "http://realbodies.com.au/api/food.php?f=fetch_meal";
    final response = await http.get(url + "&id=" + id);
    print('search food getASingleMeal Response body:${response.body}');
    var parsedJson = json.decode(response.body);
    Meal meal = Meal.fromJson(parsedJson[0]);
    return meal;
  } catch (e) {
    print("Exception on way $e");
    return null;
  }
}

class CustomSearchDelegate extends SearchDelegate<String?> {
  final List<String?> _words;
  final List<String> _history;
  final int? id;
  final String? category;
  final String? incalorie;
  final String? weight;
  final String? caloriesBurnt;
  final String? remaining;
  final String? calorie;

  //var food;
  Food? selectedFood;

  CustomSearchDelegate(List<String?> words, this.id, this.category,
      this.calorie,
      this.incalorie, this.weight, this.caloriesBurnt, this.remaining)
      : _words = words,
        _history = <String>[
//    'apple',
//    'Mango'
        ],
        super();

  Food getSelectedFood(String query) {
    print(
        'heedjkfldjfdljjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj');
    Food selectedFood = Food();

    for (int i = 0; i <= 1; i++) {
      if (food[i]['name'] == query) {
        selectedFood.name = food[i]['name'];
        selectedFood.quantity = int.parse(food[i]['quantity']);
        selectedFood.size = double.parse(food[i]['size']);
        selectedFood.carbohydrates = double.parse(food[i]['carbo']);
        selectedFood.proteins = double.parse(food[i]['proteins']);
        selectedFood.fat = double.parse(food[i]['fat']);
      }
    }
    print('helooooooooooooooooooooooooooo${selectedFood.name}');
    return selectedFood;
  }

  void getFood(String query) async {
    try {
      var url = "http://realbodies.com.au/api/food.php?f=get_food";
      final response = await http.get(url);
      print('Response body:${response.body}');
      var jsonResponse = json.decode(response.body);
      food = json.decode(response.body);
      print(food);
      for (int i = 0; i <= 1; i++) {
        if (!_words.contains(jsonResponse[i]['name'])) {
          _words.add(jsonResponse[i]['name']);
        }
      }
    } catch (e) {
      print("Exception on way $e");
    }
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      query.isEmpty
          ? Container()
          : IconButton(
              tooltip: 'Clear',
              icon: const Icon(Icons.clear),
              onPressed: () {
                query = '';
                showSuggestions(context);
              },
            ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      tooltip: 'Back',
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        // SearchDelegate.close() can return vlaues, similar to Navigator.pop().
        this.close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    selectedFood = getSelectedFood(this.query);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
//        child: Column(
//          mainAxisSize: MainAxisSize.min,
//          children: <Widget>[
//            Text('You have selected the word:'),
//            GestureDetector (
//              onTap: () {
//                // Returns this.query as result to previous screen, c.f.
//                // `showSearch()` above.
//
//               // getSelectedFood(this.query);
//
//                this.close(context, this.query);
//
//              },
//              child: Text(
//                //this.query,
//                selectedFood.name,
//                style: Theme.of(context)
//                    .textTheme
//                    .display1
//                    .copyWith(fontWeight: FontWeight.bold),
//              ),
//            ),
//          ],
//        ),

        child: Text('Please select a food from the list'),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
//        getFood(query);
//    final Iterable<String> suggestions = this.query.isEmpty
//        ? _history
//
//
//
//        : _words.where((word) => word.startsWith(query));

    return _SuggestionList(
      id: this.id,
      weight: this.weight,
      category: this.category,
      calorie: this.calorie,
      incalorie: this.incalorie,
      query: this.query,
      history: this._history,
      caloriesBurnt: this.caloriesBurnt,
      remaining: this.remaining,
      //suggestions: suggestions.toList(),
      onSelected: (String suggestion) {
        this.query = suggestion;
        this._history.insert(0, suggestion);
        showResults(context);
      },
    );
  }
}

var food;

Future<Iterable<String?>?> getFood(String query, List<String>? history) async {
  URL urlDomain = URL();
  try {
    Iterable<String?>? suggestions = [];
    List<String?> u = [];
    var url = urlDomain.domainfood + 'get_food';
    final response = await http.get(url + "&name=" + query);
    print('search Food Response body:${response.body}');
    var jsonResponse = json.decode(response.body) as List;
    food = json.decode(response.body);
    print(jsonResponse.length);
    //print(food);
    if (jsonResponse.length > 0) {
      for (int i = 0; i <= jsonResponse.length - 1; i++) {
        u.add(jsonResponse[i]['name']);
//      if(!_words.contains(jsonResponse[i]['name'])) {
//        _words.add(jsonResponse[i]['name']);
//      }
      }
    }
    //u..sort((w1,w2) => w1.toLowerCase().compareTo(w2.toUpperCase()),);
    print('search food food list gt selectedfood $u');
    return suggestions = query.isEmpty
        ? history!

    // : u.where((word) => word.contains(query.toLowerCase()));
        : u;
  } catch (e) {
    print("Exception on way $e");
  }
}

class _SuggestionList extends StatelessWidget {
  const _SuggestionList(
      {this.suggestions,
      this.query,
        this.history,
        this.onSelected,
        this.id,
        this.category,
        this.incalorie,
        this.calorie,
        this.weight,
        this.caloriesBurnt,
        this.remaining});

  final List<String>? suggestions;
  final String? query;
  final List<String>? history;
  final ValueChanged<String>? onSelected;
  final String? category;
  final int? id;
  final String? calorie;
  final String? incalorie;
  final String? weight;
  final String? caloriesBurnt;
  final String? remaining;

  Food getSelectedFood(String? query, int length) {
    print(
        'heedjkfldjfdljjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj');
    Food selectedFood = Food();
    print('helo');
    print(length);
    print(food);
    print(query);
    for (int i = 0; i <= length - 1; i++) {
      print("this is a noise" + food[i]['name']);
      if (food[i]['name'] == query!.toLowerCase()) {
        print(food[i]);
        selectedFood.id = int.parse(food[i]['id']);
        selectedFood.name = food[i]['name'];
        // selectedFood.quantity =int.parse(food[i]['quantity']==''? '0':food[i]['quantity']);
        selectedFood.quantity = 1;
        selectedFood.size = double.parse(food[i]['size']);
        selectedFood.carbohydrates = double.parse(food[i]['carbo']);
        selectedFood.proteins = double.parse(food[i]['proteins']);
        selectedFood.fat = double.parse(food[i]['fat']);
        selectedFood.sodium = double.parse(food[i]['sodium']);
        selectedFood.fatSaturate = double.parse(food[i]['saturated_fats']) ?? 0;
        selectedFood.fatPoly = double.parse(food[i]['poly_fats']) ?? 0;
        selectedFood.fatMono = double.parse(food[i]['mono_fats']) ?? 0;
        selectedFood.calories = double.parse(food[i]['calories']);
      }
    }
    print('helo${selectedFood.name}');
    return selectedFood;
  }

  @override
  Widget build(BuildContext context) {
    Food selectedFood;
    final textTheme = Theme
        .of(context)
        .textTheme
        .subhead;
    return FutureBuilder<Iterable<String?>?>(
        future: getFood(query!, history),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data!.toList().length, //suggestions.length,
              itemBuilder: (BuildContext context, int i) {
                final String? suggestion = snapshot.data!.toList()[i];
                return ListTile(
                  leading: query!.isEmpty
                      ? Container() /*Icon(Icons.history)*/ : Icon(
                      Icons.local_dining),
                  // Highlight the substring that matched the query.
                  title: RichText(
                    text: TextSpan(
                      children:
                      SearchUtil.highlightOccurrences(suggestion, query),
                      style: TextStyle(color: Colors.grey),
                    ),
//                  text: TextSpan(
//                    text: suggestion.substring(0, query.length),
//                    style: textTheme.copyWith(fontWeight: FontWeight.bold),
//                    children: <TextSpan>[
//                      TextSpan(
//                        text: suggestion.substring(query.length),
//                        style: textTheme,
//                      ),
//                    ],
//                  ),
                  ),
                  onTap: () {
                    //   onSelected(suggestion);
                    selectedFood = getSelectedFood(
                        suggestion, snapshot.data!.toList().length);
                    FocusScope.of(context).unfocus();
                    print('search food food in calorie $incalorie}');
                    Navigator.push(
                        context,
//                    ScaleRoute( page: SearchAddFood(food: selectedFood,id: id,category: category,calorie:calorie,
//                    )),

                        MaterialPageRoute(
                          builder: (context) =>
                              SearchAddFood(
                                food: selectedFood,
                            id: id,
                            category: category,
                            calorie: calorie,
                            incalorie: incalorie,
                            weight: weight,
                            remaining: remaining,
                            caloriesBurnt: caloriesBurnt,
                            meal: false,
                          ),
                        ));
                  },
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }
}

class CustomSearchDelegate2 extends SearchDelegate<String?> {
  final List<String?> _words;
  final List<String> _history;
  final int? id;
  final String? category;
  final String? incalorie;
  final String? weight;
  final String? caloriesBurnt;
  final String? remaining;
  final String? calorie;

  //var food;
  Food? selectedFood;

  CustomSearchDelegate2(List<String?> words,
      this.id,
      this.category,
      this.calorie,
      this.incalorie,
      this.weight,
      this.caloriesBurnt,
      this.remaining)
      : _words = words,
        _history = <String>[
//    'apple',
//    'Mango'
        ],
        super();

  Food getSelectedFood(String query) {
    print(
        'heedjkfldjfdljjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj');
    Food selectedFood = Food();

    for (int i = 0; i <= 1; i++) {
      if (food[i]['name'] == query) {
        selectedFood.name = food[i]['name'];
        selectedFood.quantity = int.parse(food[i]['quantity']);
        selectedFood.size = double.parse(food[i]['size']);
        selectedFood.carbohydrates = double.parse(food[i]['carbo']);
        selectedFood.proteins = double.parse(food[i]['proteins']);
        selectedFood.fat = double.parse(food[i]['fat']);
      }
    }
    print('helooooooooooooooooooooooooooo${selectedFood.name}');
    return selectedFood;
  }

  void getFood(String query) async {
    try {
      var url = "http://realbodies.com.au/api/food.php?f=get_food";
      final response = await http.get(url);
      print('Response body:${response.body}');
      var jsonResponse = json.decode(response.body);
      food = json.decode(response.body);
      print(food);
      for (int i = 0; i <= 1; i++) {
        if (!_words.contains(jsonResponse[i]['name'])) {
          _words.add(jsonResponse[i]['name']);
        }
      }
    } catch (e) {
      print("Exception on way $e");
    }
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      query.isEmpty
          ? Container()
          : IconButton(
              tooltip: 'Clear',
              icon: const Icon(Icons.clear),
              onPressed: () {
                query = '';
                showSuggestions(context);
              },
            ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      tooltip: 'Back',
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        // SearchDelegate.close() can return vlaues, similar to Navigator.pop().
        this.close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    selectedFood = getSelectedFood(this.query);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
//        child: Column(
//          mainAxisSize: MainAxisSize.min,
//          children: <Widget>[
//            Text('You have selected the word:'),
//            GestureDetector (
//              onTap: () {
//                // Returns this.query as result to previous screen, c.f.
//                // `showSearch()` above.
//
//               // getSelectedFood(this.query);
//
//                this.close(context, this.query);
//
//              },
//              child: Text(
//                //this.query,
//                selectedFood.name,
//                style: Theme.of(context)
//                    .textTheme
//                    .display1
//                    .copyWith(fontWeight: FontWeight.bold),
//              ),
//            ),
//          ],
//        ),

        child: Text('Please select a food from the list'),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
//        getFood(query);
//    final Iterable<String> suggestions = this.query.isEmpty
//        ? _history
//
//
//
//        : _words.where((word) => word.startsWith(query));

    return _SuggestionList2(
      id: this.id,
      weight: this.weight,
      category: this.category,
      calorie: this.calorie,
      incalorie: this.incalorie,
      query: this.query,
      history: this._history,
      caloriesBurnt: this.caloriesBurnt,
      remaining: this.remaining,
      //suggestions: suggestions.toList(),
      onSelected: (String suggestion) {
        this.query = suggestion;
        this._history.insert(0, suggestion);
        showResults(context);
      },
    );
  }
}

class _SuggestionList2 extends StatelessWidget {
  const _SuggestionList2(
      {this.suggestions,
      this.query,
        this.history,
        this.onSelected,
        this.id,
        this.category,
        this.incalorie,
        this.calorie,
        this.weight,
        this.caloriesBurnt,
        this.remaining});

  final List<String>? suggestions;
  final String? query;
  final List<String>? history;
  final ValueChanged<String>? onSelected;
  final String? category;
  final int? id;
  final String? calorie;
  final String? incalorie;
  final String? weight;
  final String? caloriesBurnt;
  final String? remaining;

  Food getSelectedFood(String query, int length) {
    print(
        'heedjkfldjfdljjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj');
    Food selectedFood = Food();
    print('helo');
    print(length);
    print(food);
    print(query);
    for (int i = 0; i <= length - 1; i++) {
      print("this is a noise" + food[i]['name']);
      if (food[i]['name'] == query.toLowerCase()) {
        print(food[i]);
        selectedFood.id = int.parse(food[i]['id']);
        selectedFood.name = food[i]['name'];
        // selectedFood.quantity =int.parse(food[i]['quantity']==''? '0':food[i]['quantity']);
        selectedFood.size = double.parse(food[i]['size']);
        selectedFood.carbohydrates = double.parse(food[i]['carbo']);
        selectedFood.proteins = double.parse(food[i]['proteins']);
        selectedFood.fat = double.parse(food[i]['fat']);
        selectedFood.sodium = double.parse(food[i]['sodium']);
        selectedFood.fatSaturate = double.parse(food[i]['saturated_fats']);
        selectedFood.fatPoly = double.parse(food[i]['poly_fats']);
        selectedFood.fatMono = double.parse(food[i]['mono_fats']);
      }
    }
    print('helooooooooooooooooooooooooooo${selectedFood.name}');
    return selectedFood;
  }

  //Future<Iterable<String>> getMeals(String query,List<String> history) async
  Future<List<Meal>?> getMeals(String query, List<String>? history) async {
    URL urlDomain = URL();
    try {
      Iterable<String> suggestions = [];
      List<String> u = [];

      var url = urlDomain.domainfood + 'get_meal';
      final response = await http.get(url + "&name=" + query);
      print('Response body:${response.body}');
      Iterable list = json.decode(response.body);
      List<Meal> mealList = list.map((model) => Meal.fromJson(model)).toList();
      mealList.forEach((element) {
        print(element.name);
        element.ingredients!.forEach((element) {
          print(element);
        });
      });
//      var jsonResponse=json.decode(response.body) as List;
//      food = json.decode(response.body);
//      print(jsonResponse.length);
//      //print(food);
//      if(jsonResponse.length >0) {
//        for (int i = 0; i <= jsonResponse.length - 1; i++) {
//          u.add(jsonResponse[i]['name']);
////      if(!_words.contains(jsonResponse[i]['name'])) {
////        _words.add(jsonResponse[i]['name']);
////      }
//        }
//      }
//      //u..sort((w1,w2) => w1.toLowerCase().compareTo(w2.toUpperCase()),);
//      print(u);
//      return  suggestions = query.isEmpty
//          ? history
//          : u.where((word) => word.startsWith(query.toLowerCase()));
      return mealList;
    } catch (e) {
      print("Exception on way $e");
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    Food selectedFood;
    final textTheme = Theme
        .of(context)
        .textTheme
        .subhead;
    return FutureBuilder<List<Meal>?>(
        future: getMeals(query!, history),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data!.length, //suggestions.length,
              itemBuilder: (BuildContext context, int i) {
                //final String suggestion = snapshot.data.toList()[i];
                return ListTile(
                  leading: query!.isEmpty
                      ? Container() /*Icon(Icons.history)*/ : Icon(
                      MyFlutterApp.food),
                  // Highlight the substring that matched the query.
                  title: RichText(
                    text: TextSpan(
                      children: SearchUtil.highlightOccurrences(
                          snapshot.data![i].name, query),
                      style: TextStyle(color: Colors.grey),
                    ),
//                      text: TextSpan(
//                      text: snapshot.data[i].name.substring(0, query.length),
//                      style: textTheme.copyWith(fontWeight: FontWeight.bold),
//                      children: <TextSpan>[
//                        TextSpan(
//                          text: snapshot.data[i].name.substring(query.length),
//                          style: textTheme,
//                        ),
//                      ],
//                    ),
                  ),
                  onTap: () {
                    //   onSelected(suggestion);
                    // selectedFood = getSelectedFood(suggestion,snapshot.data.toList().length);
                    FocusScope.of(context).unfocus();

                    Navigator.push(
                        context,
//                    ScaleRoute( page: SearchAddFood(food: selectedFood,id: id,category: category,calorie:calorie,
//                    )),

                        MaterialPageRoute(
                          builder: (context) => AddMeal(
                            mealFood: snapshot.data![i],
                            id: id,
                            category: category,
                            calorie: calorie,
                            incalorie: incalorie,
                            weight: weight,
                            remaining: remaining,
                            caloriesBurnt: caloriesBurnt,
                            meal: false,
                          ),
                        ));
                  },
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }
}

List<TextSpan> highlightOccurrences(String source, String query) {
  List<String> queryList = query.split(' ');
  if (queryList.length > 1) {
    final List<TextSpan> children = [];
    queryList.forEach((query) {
//      if (query == null || query.isEmpty ||
//          !source.toLowerCase().contains(query.toLowerCase())) {
//        return [ TextSpan(text: source)];
//      }
      final matches = query.toLowerCase().allMatches(source.toLowerCase());

      int lastMatchEnd = 0;

      //final List<TextSpan> children = [];

      for (var i = 0; i < matches.length; i++) {
        final match = matches.elementAt(i);

        if (match.start != lastMatchEnd) {
          children.add(TextSpan(
            text: source.substring(lastMatchEnd, match.start),
          ));
        }

        children.add(TextSpan(
          text: source.substring(match.start, match.end),
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
        ));

        if (i == matches.length - 1 && match.end != source.length) {
          children.add(TextSpan(
            text: source.substring(match.end, source.length),
          ));
        }

        lastMatchEnd = match.end;
      }
    });
    return children;
  } else {
    if (query == null ||
        query.isEmpty ||
        !source.toLowerCase().contains(query.toLowerCase())) {
      return [TextSpan(text: source)];
    }
    final matches = query.toLowerCase().allMatches(source.toLowerCase());

    int lastMatchEnd = 0;

    final List<TextSpan> children = [];
    for (var i = 0; i < matches.length; i++) {
      final match = matches.elementAt(i);

      if (match.start != lastMatchEnd) {
        children.add(TextSpan(
          text: source.substring(lastMatchEnd, match.start),
        ));
      }

      children.add(TextSpan(
        text: source.substring(match.start, match.end),
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
      ));

      if (i == matches.length - 1 && match.end != source.length) {
        children.add(TextSpan(
          text: source.substring(match.end, source.length),
        ));
      }

      lastMatchEnd = match.end;
    }
    return children;
  }
}
