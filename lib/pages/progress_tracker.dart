import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:real_bodies/provider/weekly_progress_tracker_provider.dart';
import 'package:real_bodies/services/real_bodies_in_app_utilities.dart';

class ProgressTracker extends StatefulWidget {
  final String? week;

  ProgressTracker({this.week});

  @override
  _ProgressTrackerState createState() => _ProgressTrackerState();
}

class _ProgressTrackerState extends State<ProgressTracker> {
  @override
  Widget build(BuildContext context) {
//    return  ChangeNotifierProvider<WeeklyProgressProvider>(
//      create: (context) => WeeklyProgressProvider(),
//      lazy: false,
    child:
    return Column(
      children: <Widget>[
        SizedBox(
          height: 3,
        ),
        Center(
          child: Consumer<WeeklyProgressProvider>(
              builder: (context, provider, child) {
            if (provider.week != null) {
              return Text(
                'Training Week ${provider.week}',
                style: TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.bold,
                ),
              );
            } else
              return Container();
          }),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 1.0, right: 1.0),
          child:

//                  for( WeekProgress wp in provider.weekProgressSummary) {
//                    tableRowsList.add( TableRow(
//
//
//                      children: [
//                        Center(
//                          child: Container(
//                            height: 37,
//                            child: Center(child: Text(wp.date,
//                              style: TextStyle(
//                                color:  Color(0xff94948d),
//                              ),
//                            )),
//
//                          ),
//                        ),
//                        Center(
//                          child: Container(
//                            height: 37,
//                            child: Center(child: Text(wp.goal,
//                              style: TextStyle(
//                                color:  Color(0xff94948d),
//                              ),
//                            )),
//
//                          ),
//                        ),
//
//                        Center(
//                          child: Container(
//                            height: 37,
//                            child: Center(child: Text(wp.actual,
//                              style: TextStyle(
//                                color:  Color(0xff94948d),
//                              ),
//                            ),
//                            ),
//
//                          ),
//                        ),
//                        Center(
//                          child: Container(
//                            height: 37,
//                            child:
//                            Center(
//                              child: Text(wp.remaining,
//                                style: TextStyle(
//                                  color:  Color(0xff94948d),
//                                ),
//                              ),
//                            ),
//
//
//
//
//                          ),
//                        ),
//
//                      ],
//                    ),);
//                  }
          Table(
                  defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
                  children: [
                TableRow(
                  decoration: BoxDecoration(
                    color: Color(0xffeeeeee),
                    border: Border.all(color: Colors.grey, width: 1),
                  ),
                  children: [
                    Center(
                      child: Container(
                        height: 37,
                        child: Center(
                            child: Text(
                          'Calories Goals',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff94948d),
                          ),
                        )),
                      ),
                    ),
                    Center(
                      child: Container(
                        height: 37,
                        child: Center(
                            child: Text(
                          'Target',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff94948d),
                          ),
                        )),
                      ),
                    ),
                    Center(
                      child: Container(
                        height: 37,
                        child: Center(
                          child: Text(
                            'Actual',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Color(0xff94948d),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Center(
                      child: Container(
                        height: 37,
                        child: Center(
                          child: Text(
                            'Remaining',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Color(0xff94948d),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ]
                  /*       TableRow(


                      children: [
                        Center(
                          child: Container(
                            height: 37,
                            child: Center(child: Text('01-11-2019',
                              style: TextStyle(
                                color:  Color(0xff94948d),
                              ),
                            )),

                          ),
                        ),
                        Center(
                          child: Container(
                            height: 37,
                            child: Center(child: Text('1600',
                              style: TextStyle(
                                color:  Color(0xff94948d),
                              ),
                            )),

                          ),
                        ),

                        Center(
                          child: Container(
                            height: 37,
                            child: Center(child: Text('1500',
                              style: TextStyle(
                                color:  Color(0xff94948d),
                              ),
                            ),
                            ),

                          ),
                        ),
                        Center(
                          child: Container(
                            height: 37,
                            child:
                                Center(
                                  child: Text('100',
                                    style: TextStyle(
                                      color:  Color(0xff94948d),
                                    ),
                                  ),
                                ),




                          ),
                        ),

                      ],
                    ),
                    TableRow(


                      children: [
                        Center(
                          child: Container(
                            height: 37,
                            child: Center(child: Text('01-11-2019',
                              style: TextStyle(
                                color:  Color(0xff94948d),
                              ),
                            )),

                          ),
                        ),
                        Center(
                          child: Container(
                            height: 37,
                            child: Center(child: Text('1600',
                              style: TextStyle(
                                color:  Color(0xff94948d),
                              ),
                            )),

                          ),
                        ),

                        Center(
                          child: Container(
                            height: 37,
                            child: Center(child: Text('1500',
                              style: TextStyle(
                                color:  Color(0xff94948d),
                              ),
                            ),
                            ),

                          ),
                        ),
                        Center(
                          child: Container(
                            height: 37,
                            child:
                            Center(
                              child: Text('100',
                                style: TextStyle(
                                  color:  Color(0xff94948d),
                                ),
                              ),
                            ),




                          ),
                        ),

                      ],
                    ),
                    TableRow(


                      children: [
                        Center(
                          child: Container(
                            height: 37,
                            child: Center(child: Text('01-11-2019',
                              style: TextStyle(
                                color:  Color(0xff94948d),
                              ),
                            )),

                          ),
                        ),
                        Center(
                          child: Container(
                            height: 37,
                            child: Center(child: Text('1600',
                              style: TextStyle(
                                color:  Color(0xff94948d),
                              ),
                            )),

                          ),
                        ),

                        Center(
                          child: Container(
                            height: 37,
                            child: Center(child: Text('1700',
                              style: TextStyle(
                                color:  Color(0xff94948d),
                              ),
                            ),
                            ),

                          ),
                        ),
                        Center(
                          child: Container(
                            height: 37,
                            child:
                            Center(
                              child: Text('-100',
                                style: TextStyle(
                                  color:  Colors.red,
                                ),
                              ),
                            ),




                          ),
                        ),

                      ],
                    ),*/

                  ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 1.0, right: 1.0),
          child:

//                  for( WeekProgress wp in provider.weekProgressSummary) {
//                    tableRowsList.add( TableRow(
//
//
//                      children: [
//                        Center(
//                          child: Container(
//                            height: 37,
//                            child: Center(child: Text(wp.date,
//                              style: TextStyle(
//                                color:  Color(0xff94948d),
//                              ),
//                            )),
//
//                          ),
//                        ),
//                        Center(
//                          child: Container(
//                            height: 37,
//                            child: Center(child: Text(wp.goal,
//                              style: TextStyle(
//                                color:  Color(0xff94948d),
//                              ),
//                            )),
//
//                          ),
//                        ),
//
//                        Center(
//                          child: Container(
//                            height: 37,
//                            child: Center(child: Text(wp.actual,
//                              style: TextStyle(
//                                color:  Color(0xff94948d),
//                              ),
//                            ),
//                            ),
//
//                          ),
//                        ),
//                        Center(
//                          child: Container(
//                            height: 37,
//                            child:
//                            Center(
//                              child: Text(wp.remaining,
//                                style: TextStyle(
//                                  color:  Color(0xff94948d),
//                                ),
//                              ),
//                            ),
//
//
//
//
//                          ),
//                        ),
//
//                      ],
//                    ),);
//                  }
              Consumer<WeeklyProgressProvider>(
                  builder: (context, provider, child) {
            if (provider.weekProgressSummary != null) {
              return Table(
                  defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
                  children:
                  List.generate(provider.weekProgressSummary!.length, (i) {
                    return TableRow(
                      children: [
                        Center(
                          child: Container(
                            height: 37,
                            child: Center(
                                child: Text(
                                  provider.weekProgressSummary![i].date!,
                                  style: TextStyle(
                                    color: Color(0xff94948d),
                                  ),
                            )),
                          ),
                        ),
                        Center(
                          child: Container(
                            height: 37,
                            child: Center(
                                child: Text(
                                  RBUtility.getPreciseCalories(
                                      provider.weekProgressSummary![i].goal!),
                                  style: TextStyle(
                                    color: Color(0xff94948d),
                                  ),
                            )),
                          ),
                        ),
                        Center(
                          child: Container(
                            height: 37,
                            child: Center(
                              child: Text(
                                provider.weekProgressSummary![i].actual!,
                                style: TextStyle(
                                  color: Color(0xff94948d),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Center(
                          child: Container(
                            height: 37,
                            child: Center(
                              child: Text(
                                RBUtility.getPreciseCalories(
                                    provider.weekProgressSummary![i]
                                        .remaining!),
                                style: TextStyle(
                                  color: Color(0xff94948d),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  }));
            } else
              return Container();
          }),
        ),
      ],
      //   ),
    );
  }
}
