import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:real_bodies/pages/video_example.dart';
import 'package:real_bodies/provider/exercise_diary_provider.dart';
import 'package:real_bodies/realbodyui/exercise_video_player.dart';
import 'package:real_bodies/services/date_time.dart';
import 'package:real_bodies/theme/palette.dart';

class ExerciseDiary extends StatefulWidget {
  final String? title;
  final String? id;

  ExerciseDiary({this.title, this.id});

  @override
  _ExerciseDiaryState createState() => _ExerciseDiaryState();
}

class _ExerciseDiaryState extends State<ExerciseDiary> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 3,
        ),
        Center(
          child: Text(
            widget.title!,
            style: TextStyle(
                fontSize: 23,
                fontWeight: FontWeight.bold,
                color: Palette.boldTextO),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 1.0, right: 1.0),
          child: Table(
            columnWidths: {
              0: FractionColumnWidth(.3),
              4: FractionColumnWidth(.4)
            },
            defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
            children: [
              TableRow(
                decoration: BoxDecoration(
                  color: Color(0xffeeeeee),
                  border: Border.all(color: Colors.grey, width: 1),
                ),
                children: [
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        'Exercise',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),

                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        'Time',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),

                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                        child: Text(
                          'Sets',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff94948d),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                        child: Text(
                          'Reps',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff94948d),
                          ),
                        ),
                      ),
                    ),
                  ),
//                    Center(
//                      child: Container(
//                        height: 37,
//                        child: Center(child: Text('Logged',
//                          style: TextStyle(
//                            fontWeight: FontWeight.bold,color:  Color(0xff94948d),
//                          ),
//                        )),
//
//                      ),
//                    ),
                ],
              ),
            ],
          ),
        ),
        Consumer<ExerciseDiaryProvider>(
          builder: (context, provider, child) {
            // provider.getLoggedExercises(widget.id);
            if (provider.loggedExercises.isNotEmpty) {
              return ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: provider.loggedExercises.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: const EdgeInsets.only(left: 1.0, right: 1.0),
                      child: Slidable(
                        actionPane: SlidableDrawerActionPane(),
                        actionExtentRatio: 0.25,
                        secondaryActions: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, right: 8.0),
                            child: Container(
                              height: 22,
                              width: 40,
                              child: FlatButton(
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                                onPressed: () {
                                  //  Navigator.push(context, MaterialPageRoute(builder: (context) => ExerciseVideoPlayer()));
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ChewieDemo()),
                                  );
                                },
                                color: Palette.boldTextO,
                                textColor: Colors.white,
                                child: FittedBox(
                                  fit: BoxFit.contain,
                                  child: Text("View".toUpperCase(),
                                      style: TextStyle(
                                        fontSize:
                                            MediaQuery.of(context).size.height /
                                                50,
                                      )),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, right: 8.0),
                            child: Container(
                              height: 22,
                              width: 40,
                              child: FlatButton(
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                                onPressed: () {},
                                color: Palette.boldTextO,
                                textColor: Colors.white,
                                child: FittedBox(
                                  fit: BoxFit.contain,
                                  child: Text("Log".toUpperCase(),
                                      style: TextStyle(
                                        fontSize:
                                            MediaQuery.of(context).size.height /
                                                50,
                                      )),
                                ),
                              ),
                            ),
                          ),
                          // SizedBox(width: 200,),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, right: 8.0),
                            child: Container(
                              height: 22,
                              width: 40,
                              child: FlatButton(
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                                onPressed: () {},
                                color: Palette.boldTextO,
                                textColor: Colors.white,
                                child: FittedBox(
                                  fit: BoxFit.contain,
                                  child: Text("Done".toUpperCase(),
                                      style: TextStyle(
                                        fontSize:
                                            MediaQuery.of(context).size.height /
                                                50,
                                      )),
                                ),
                              ),
                            ),
                          ),
                        ],
                        child: Table(
                          columnWidths: {
                            0: FractionColumnWidth(.3),
                            4: FractionColumnWidth(.4)
                          },
                          defaultVerticalAlignment:
                              TableCellVerticalAlignment.bottom,
                          children: [
                            TableRow(
                              children: [
                                Center(
                                  child: Container(
                                    height: 50,
                                    child: Center(
                                        child: Text(
                                      '${provider.loggedExercises[index].name}',
                                      style: TextStyle(
                                        color: Color(0xff94948d),
                                      ),
                                    )),
                                  ),
                                ),
                                Center(
                                  child: Container(
                                    height: 50,
                                    child: Center(
                                        child: Text(
                                          DateUtility.getTimeInMinutes(provider
                                              .loggedExercises[index]
                                              .duration!),
                                          style: TextStyle(
                                            color: Color(0xff94948d),
                                          ),
                                    )),
                                  ),
                                ),
                                Center(
                                  child: Container(
                                    height: 50,
                                    child: Center(
                                      child: Text(
                                        '${provider.loggedExercises[index].sets}',
                                        style: TextStyle(
                                          color: Color(0xff94948d),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Center(
                                  child: Container(
                                    height: 50,
                                    child: Center(
                                      child: Text(
                                        '${provider.loggedExercises[index].reps}',
                                        style: TextStyle(
                                          color: Color(0xff94948d),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  });
            } else
              return Text('No Exercise logged');
          },
        ),
      ],
    );
  }
}
