import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/theme/palette.dart';
import 'package:real_bodies/ui/widgets/count_down_timer.dart';
import 'package:wakelock/wakelock.dart';

class Exercise extends StatefulWidget {
  final String? week;
  final String? reps;
  final String? sets;
  final String? name;
  final String? exerciseId;
  final String? duration;
  final String? image;
  final int? index;
  final String? userId;
  final String? calories;
  final String? weight;

  Exercise({this.calories,
    this.week,
    this.index,
    this.reps,
    this.sets,
    this.weight,
    this.name,
    this.exerciseId,
      this.image,
      this.duration,
      this.userId});

  @override
  _ExerciseState createState() => _ExerciseState();
}

class _ExerciseState extends State<Exercise>
    with SingleTickerProviderStateMixin {
  late AnimationController _floatBtnAnimController;
  bool _isPlaying = false;

  @override
  void initState() {
    super.initState();
    _floatBtnAnimController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    print('Exercise (calories): ' + widget.calories!);
    Wakelock.enable();
  }

  @override
  void dispose() {
    Wakelock.disable();
    super.dispose();
    _floatBtnAnimController.dispose();
  }

  void _handleOnPressed() {
    setState(() {
      _isPlaying = !_isPlaying;
      _isPlaying
          ? _floatBtnAnimController.forward()
          : _floatBtnAnimController.reverse();
    });
    //CountDownTimer().startStopTimer();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            height: height * 0.03,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: IconButton(
              icon: new Icon(Icons.arrow_back, color: Palette.mainPurple),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ),
          Container(
            height: height * 0.05,
            width: width * 0.8,
            child: Center(
              child: AutoSizeText(
                '${widget.name}',
                style: TextStyle(
                    color: Palette.mainPurple,
                    fontWeight: FontWeight.w600,
                    fontSize: 18),
              ),
            ),
          ),
          Container(
            height: height * 0.03,
          ),
          Container(
            width: width * 0.8,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                AutoSizeText(
                  '${widget.sets} sets',
                  style: TextStyle(fontSize: 18, color: Palette.mainPurple),
                ),
                AutoSizeText(
                  '${widget.reps} reps',
                  style: TextStyle(fontSize: 18, color: Palette.mainPurple),
                ),
              ],
            ),
          ),
          Container(
            height: height * 0.43,
            width: width * 0.95,
//            decoration: BoxDecoration(
//                color: Palette.primaryColor,
//
//                borderRadius: BorderRadius.all(
//                  Radius.circular(10.0),
//                ),
//                boxShadow: <BoxShadow>[
//                  const BoxShadow(
//                    color: const Color(0xFF000000),
//                    offset: Offset.zero,
//                    blurRadius: 5.0,
//                    spreadRadius: 0.0,
//                  ),
//                ]),
/**************************************************** this image to be incudded*********************************************************************************************/
            // child: Image.network(widget.image),
          ),
          Container(
            height: height * 0.01,
            width: width * 0.90,
          ),
          Container(
            height: height * 0.33,
            width: width * 0.95,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Row(
              children: <Widget>[
//                Expanded(
//                  child: Center(
//                    child: FittedBox(
//                      fit: BoxFit.cover,
//                      child: FloatingActionButton(
//                        child: AnimatedIcon(icon: AnimatedIcons.pause_play, progress: _floatBtnAnimController),
//                        backgroundColor: Colors.redAccent,
//                        foregroundColor: Colors.white,
//                        elevation: 5.0,
//                        onPressed: () => _handleOnPressed(),
//                      ),
//                    ),
//                  ),
//                ),
                Expanded(
                  flex: 2,

//                    child: FittedBox(
//                      fit: BoxFit.contain,
                  child: CountDownTimer(
                      exerciseDuration: widget.duration,
                      userId: widget.userId,
                      exerciseId: widget.exerciseId,
                      calories: widget.calories,
                      weight: widget.weight),
                  //   ),
                ),
                Expanded(
                  flex: 1,
                  child: Center(
                    child: FittedBox(
                      fit: BoxFit.cover, //as small fill for as large
                      child: FloatingActionButton(
                        heroTag: 'nextbtn',
                        child: Icon(Icons.arrow_forward),
                        backgroundColor: Palette.mainPurple,
                        foregroundColor: Colors.white,
                        elevation: 5.0,
                        onPressed: () => {},
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
