import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';
import 'package:real_bodies/models/food_categories.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/pages/search_food.dart';
import 'package:real_bodies/services/date_time.dart';
import 'package:real_bodies/theme/palette.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class FoodDiary extends StatefulWidget {
  final int? id;
  final Function() notifyParent;
  final String? calorie;
  final String? incalorie;
  final String? remaining;
  final String? caloriesBurnt;
  final String? weight;

  FoodDiary({Key? key,
    required this.notifyParent,
    this.id,
    this.calorie,
    this.incalorie,
    this.weight,
    this.remaining,
    this.caloriesBurnt})
      : super(key: key);
  @override
  _FoodDiaryState createState() => _FoodDiaryState();
}

class _FoodDiaryState extends State<FoodDiary> {
  List<TableRowReturn> snackList = [];
  List<TableRowReturn> breakFastList = [];
  List<TableRowReturn> lunchList = [];
  List<TableRowReturn> dinnerList = [];
  List<String>? kFoods;

  URL urldomain = URL();

  void checkinfo() async {
    try {
      print("id diary" + widget.id.toString());
      print(DateTime.now().toString());
      var url = urldomain.domain + "get_food_record";
      final response = await http.get(url +
          "&id=" +
          widget.id.toString() +
          "&date=" +
          DateUtility.getCurrentDateWithDbFormat());
      print('Food Diary ' +
          url +
          "&id=" +
          widget.id.toString() +
          "&date=" +
          DateUtility.getCurrentDateWithDbFormat());
      print('Response body food diary:${response.body}');

      var jsonResponse = json.decode(response.body);
      for (int i = 0; i < jsonResponse.length; i++) {
        if (jsonResponse[i]['category'] == "breakfast") {
          addbreakfast(breakFastList, jsonResponse, i);
        }
        if (jsonResponse[i]['category'] == "lunch") {
          addbreakfast(lunchList, jsonResponse, i);
        }
        if (jsonResponse[i]['category'] == "dinner") {
          addbreakfast(dinnerList, jsonResponse, i);
        }
        if (jsonResponse[i]['category'] == "snacks") {
          addbreakfast(snackList, jsonResponse, i);
        }
        if (jsonResponse == "null") {
          print("NO Food in database");
        }
        // breakFastList.add(   TableRowReturn(
        //          name: jsonResponse[i]['name'],
        //          calories: jsonResponse[i]['calories'].toString(),
        //          proteins: jsonResponse[i]['protein'].toString(),
        //          carbs:jsonResponse[i]['carbo'].toString(),

        //        ),
        //        );
        //        widget.notifyParent();

      }
      print(breakFastList);

      //    var requestresponse=jsonResponse['response'];
      /*
      if (requestresponse=="success")
{
  var name=jsonResponse['name'];
   var calorie=jsonResponse['calories'];
    var weight=jsonResponse['weight'];
  int id= int.parse(jsonResponse['id']);

  print('This is the idddddd   heloo$id');


 /* Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => DashBoard(id: id,name: name,weight:weight,calorie:calorie)),
  ); */


  }
else if(requestresponse=="error")
{

  print("error login");
}
 */

    } catch (e) {
      print("Exception on way $e");
    }
  }

  void addbreakfast(listname, jsonResponse, i) {
    print('fd1: ' + jsonResponse[i]['category']);
    listname.add(
      TableRowReturn(
        f_id: jsonResponse[i]['f_id'],
        foodId: jsonResponse[i]['f_id'],
        name: jsonResponse[i]['name'],
        calories: jsonResponse[i]['calories'].toString(),
        proteins: jsonResponse[i]['protein'].toString(),
        carbs: jsonResponse[i]['carbo'].toString(),
        category: jsonResponse[i]['category'].toString(),
      ),
    );
    widget.notifyParent();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkinfo();
  }

  @override
  Widget build(BuildContext context) {
    //checkinfo();
    return Column(
      children: <Widget>[
        SizedBox(
          height: 5,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 2.0, right: 2.0),
          child: Table(
            columnWidths: {0: FractionColumnWidth(.3)},
            defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
            children: [
              TableRow(
                decoration: BoxDecoration(
                  color: Color(0xffeeeeee),
                  border: Border.all(color: Colors.grey, width: 1),
                ),
                children: [
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        'Food',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        'Calories',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                        child: Text(
                          'Proteins',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff94948d),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        'Carbs',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                ],
              ),
              TableRow(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.grey,
                      width: 1,
                    ),
                  ),
                ),
                children: [
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        'BreakFast',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        '300',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                        child: Text(
                          '29g',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff94948d),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        '13g',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: breakFastList.length,
            itemBuilder: (BuildContext context, int index) {
              final items = breakFastList;
              return Slidable(
                  key: Key(items[index].name!),
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.25,
                  actions: <Widget>[
                    IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () async {
                        var m = await deleteFood(
                            items[index].foodId, items[index].category);
                        if (m == true) {
                          setState(() {
                            items.removeAt(index);
                            widget.notifyParent();
                          });
                        } else {
                          setState(() {});
                        }
//                 widget.notifyParent();
                      },
                    ),
                  ],
//                   dismissal: SlidableDismissal(
//                     child: SlidableDrawerDismissal(),
//                     onDismissed: (actionType) async{
//                       print(
//                           actionType == SlideActionType.primary
//                               ? 'Dismiss Archive'
//                               : 'Dimiss Delete');
//                       deleteFood(items[index].foodId, items[index].category);
//                       setState(() {
//                         items.removeAt(index);
//                       });
//                     },
//                   ),
                  child: breakFastList[index]);
            }),
        SizedBox(
          height: 5,
        ),
        GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => SearchFood(
                      id: widget.id,
                      category: FoodCategories.breakfast,
                      calorie: widget.calorie,
                      incalorie: widget.incalorie,
                      caloriesBurnt: widget.caloriesBurnt,
                      remaining: widget.remaining,
                      weight: widget.weight)));
              /*  breakFastList.add(   TableRowReturn(
                 name: 'Cherry Coke',
                 calories: 45,
                 proteins: 0.5,
                 carbs: 5,

               ),
               );
               widget.notifyParent(); */
            },
            child: Center(
                child: Text(
              '+Add Food',
              style: TextStyle(fontSize: 18, color: Palette.boldTextO),
            ))),
        Padding(
          padding: const EdgeInsets.only(left: 2.0, right: 2.0),
          child: Table(
            columnWidths: {0: FractionColumnWidth(.3)},
            defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
            children: [
              TableRow(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.grey,
                      width: 1,
                    ),
                  ),
                ),
                children: [
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        'Lunch',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        '300',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                        child: Text(
                          '29g',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff94948d),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        '13g',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: lunchList.length,
            itemBuilder: (BuildContext context, int index) {
              final items = lunchList;
              return Slidable(
                  key: Key(items[index].name!),
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.25,
                  actions: <Widget>[
                    IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () async {
                        var m = await deleteFood(
                            items[index].foodId, items[index].category);
                        if (m == true) {
                          setState(() {
                            items.removeAt(index);
                            widget.notifyParent();
                          });
                        } else {
                          setState(() {});
                        }
                      },
                    ),
                  ],
//                   dismissal: SlidableDismissal(
//                     child: SlidableDrawerDismissal(),
//                     onDismissed: (actionType) {
//                       print(
//                           actionType == SlideActionType.primary
//                               ? 'Dismiss Archive'
//                               : 'Dimiss Delete');
//                       deleteFood(items[index].foodId, items[index].category);
//                       setState(() {
//                         items.removeAt(index);
//                       });
//                     },
//                   ),
                  child: lunchList[index]);
            }),
        SizedBox(
          height: 5,
        ),
        GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => SearchFood(
                      id: widget.id,
                      category: FoodCategories.lunch,
                      calorie: widget.calorie,
                      incalorie: widget.incalorie,
                      remaining: widget.remaining,
                      caloriesBurnt: widget.caloriesBurnt,
                      weight: widget.weight)));
              /*  lunchList.add(   TableRowReturn(
                 name: 'Cherry Coke',
                 calories: "45",
                 proteins: "0.5",
                 carbs: "5",

               ),
               );
               widget.notifyParent(); */
            },
            child: Center(
                child: Text(
              '+Add Food',
              style: TextStyle(fontSize: 18, color: Palette.boldTextO),
            ))),
        Padding(
          padding: const EdgeInsets.only(left: 2.0, right: 2.0),
          child: Table(
            columnWidths: {0: FractionColumnWidth(.3)},
            defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
            children: [
              TableRow(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.grey,
                      width: 1,
                    ),
                  ),
                ),
                children: [
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        'Dinner',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        '300',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                        child: Text(
                          '29g',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff94948d),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        '13g',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: dinnerList.length,
            itemBuilder: (BuildContext context, int index) {
              final items = dinnerList;
              return Slidable(
                  key: Key(items[index].name!),
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.25,
                  actions: <Widget>[
                    IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () async {
                        var m = await deleteFood(
                            items[index].foodId, items[index].category);
                        if (m == true) {
                          setState(() {
                            items.removeAt(index);
                            widget.notifyParent();
                          });
                        } else {
                          setState(() {});
                        }
                      },
                    ),
                  ],
//                   dismissal: SlidableDismissal(
//                     child: SlidableDrawerDismissal(),
//                     onDismissed: (actionType) {
//                       print(
//                           actionType == SlideActionType.primary
//                               ? 'Dismiss Archive'
//                               : 'Dimiss Delete');
//                       deleteFood(items[index].foodId, items[index].category);
//                       setState(() {
//                         items.removeAt(index);
//                       });
//                     },
//                   ),
                  child: dinnerList[index]);
            }),
        SizedBox(
          height: 5,
        ),
        GestureDetector(
            onTap: () {
              print('food diary dinner ${widget.incalorie}');
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => SearchFood(
                      id: widget.id,
                      category: FoodCategories.dinner,
                      calorie: widget.calorie,
                      incalorie: widget.incalorie,
                      remaining: widget.remaining,
                      caloriesBurnt: widget.caloriesBurnt,
                      weight: widget.weight)));
              /* dinnerList.add(   TableRowReturn(
               name: 'Cherry Coke',
               calories: "45",
               proteins: "0.5",
               carbs: "5",

             ),
             );
             widget.notifyParent(); */
            },
            child: Center(
                child: Text(
              '+Add Food',
              style: TextStyle(fontSize: 18, color: Palette.boldTextO),
            ))),
        Padding(
          padding: const EdgeInsets.only(left: 2.0, right: 2.0),
          child: Table(
            columnWidths: {0: FractionColumnWidth(.3)},
            defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
            children: [
              TableRow(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.grey,
                      width: 1,
                    ),
                  ),
                ),
                children: [
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        'Snacks',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        '300',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                        child: Text(
                          '29g',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff94948d),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: 37,
                      child: Center(
                          child: Text(
                        '13g',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xff94948d),
                        ),
                      )),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: snackList.length,
            itemBuilder: (BuildContext context, int index) {
              final items = snackList;
              return Slidable(
                  key: Key(items[index].name!),
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.25,
                  actions: <Widget>[
                    IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () async {
                        var m = await deleteFood(
                            items[index].foodId, items[index].category);
                        if (m == true) {
                          setState(() {
                            items.removeAt(index);
                            widget.notifyParent();
                          });
                        } else {
                          setState(() {});
                        }
                      },
                    ),
                  ],
//                   dismissal: SlidableDismissal(
//                     child: SlidableDrawerDismissal(),
//                     onDismissed: (actionType) {
//                       print(
//                           actionType == SlideActionType.primary
//                               ? 'Dismiss Archive'
//                               : 'Dimiss Delete');
//                       deleteFood(items[index].foodId, items[index].category);
//                       setState(() {
//                         items.removeAt(index);
//                       });
//                     },
//                   ),
                  child: snackList[index]);
            }),
        SizedBox(
          height: 5,
        ),
        GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => SearchFood(
                      id: widget.id,
                      category: FoodCategories.snacks,
                      incalorie: widget.incalorie,
                      calorie: widget.calorie,
                      caloriesBurnt: widget.caloriesBurnt,
                      remaining: widget.remaining,
                      weight: widget.weight)));
              /*   snackList.add(   TableRowReturn(
                 name: 'Cherry Coke',
                 calories: "45",
                 proteins: "0.5",
                 carbs: "5",

               ),
               );
               widget.notifyParent(); */
            },
            child: Center(
                child: Text(
              '+Add Food',
              style: TextStyle(fontSize: 18, color: Palette.boldTextO),
            ))),
      ],
    );
  }

  Future<bool> deleteFood(String? foodId, String? category) async {
    print("id diary" + widget.id.toString());
    print(DateTime.now().toString());
    var nowDate = DateTime.now();
    var formatter = DateFormat('yyyy-MM-dd');
    String formatted = formatter.format(nowDate);
    var url = urldomain.domain + "delete_food_record";
    final response = await http.get(url + '&id=$foodId');
    print(url +
        "&id=" +
        widget.id.toString() +
        '&food_id=$foodId&category=$category&date=$formatted');
    print('Response body food dairy:${response.body}');
    if (response.body == 'done') {
      widget.notifyParent();
      return true;
    } else {
      widget.notifyParent();
      return false;
    }
  }
}

class TableRowReturn extends StatelessWidget {
  final String? name;
  final String? calories;
  final String? proteins;
  final String? carbs;
  final String? category;
  final String? foodId;
  final String? f_id;

  TableRowReturn({this.name,
    this.calories,
    this.proteins,
    this.carbs,
    this.category,
    this.foodId,
    this.f_id});

  @override
  Widget build(BuildContext context) {
    print('nameeee' + name!);
    return
//      Slidable(
//        actionPane:  SlidableDrawerActionPane(),
//        actionExtentRatio: 0.25,
//        actions: <Widget>[
//            IconSlideAction(
//            caption: 'Delete',
//            color: Colors.red,
//            icon: Icons.delete,
//            onTap: () => {
//
//            },
//          ),
//        ],
        // child:
        Padding(
      padding: const EdgeInsets.only(left: 2.0, right: 2.0),
      child: Table(
        columnWidths: {0: FractionColumnWidth(.3)},
        defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
        children: [
          TableRow(
            children: [
              Center(
                child: Container(
                  height: 55,
                  child: Center(
                      child: AutoSizeText(
                    '$name',
                    style: TextStyle(
                      color: Color(0xff94948d),
                    ),
                  )),
                ),
              ),
              Center(
                child: Container(
                  height: 55,
                  child: Center(
                      child: Text(
                    '$calories',
                    style: TextStyle(
                      color: Color(0xff94948d),
                    ),
                  )),
                ),
              ),
              Center(
                child: Container(
                  height: 55,
                  child: Center(
                    child: Text(
                      '$proteins',
                      style: TextStyle(
                        color: Color(0xff94948d),
                      ),
                    ),
                  ),
                ),
              ),
              Center(
                child: Container(
                  height: 55,
                  child: Center(
                      child: Text(
                    '$carbs',
                    style: TextStyle(
                      color: Color(0xff94948d),
                    ),
                  )),
                ),
              ),
            ],
          ),
        ],
      ),
      //),
    );
  }
}
