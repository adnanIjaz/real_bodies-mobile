import 'package:charts_flutter/flutter.dart' as prefix0;
import 'package:flutter/material.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/realbodyui/fitness_level_3.dart';
import 'package:real_bodies/realbodyui/fitnesslevel3.dart';
import 'package:real_bodies/theme/palette.dart';
import 'package:real_bodies/ui/widgets/custom_alert_dialog.dart';
import 'package:real_bodies/ui/widgets/custom_text_field.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
//TODO: new commit

class FitnessLevel extends StatefulWidget {
  final int? id;
  final String? goal;
  final String? diet;
  final String? name;
  final String? email;
  final String? password;

  FitnessLevel(
      {this.id, this.goal, this.diet, this.name, this.email, this.password});

  @override
  _FitnessLevelState createState() => _FitnessLevelState();
}

class _FitnessLevelState extends State<FitnessLevel> {
  bool? level1 = false;
  bool? level2 = false;
  bool? level3 = false;
  bool? level4 = false;
  bool? level5 = false;
  String level = "";
  double? levelval;
  URL urldomain = URL();

/* void setlevel() async {
    try {
      var url = urldomain.domain + "add_level";
      final response = await http
          .get(url + "&id=" + widget.id.toString() + "&level=" + level);
      var jsonResponse = json.decode(response.body);
      var requestresponse = jsonResponse['response'];

      if (requestresponse == "success") {
        print('Added  BMI');
        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                FitnessLevelThree(id: widget.id,levelval:levelval)));
      } else if (requestresponse == "error") {
        print("error  BMI");
      }
      // print('Response body:${response.body}');
    } catch (e) {
      print(e);
    }
  } */

  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double paddingLeft =
        MediaQuery.of(context).size.width - MediaQuery.of(context).padding.left;
    double heightWithoutAppBar = MediaQuery.of(context).size.height -
        kToolbarHeight -
        MediaQuery.of(context).padding.top;

    return Scaffold(
      backgroundColor: Color(0xffDE4922),
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                //color: Colors.yellow,
                //height: height * 0.60,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        InkWell(
                          child: Container(
                            height: height * 0.10,
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                            ),
                          ),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                        Container(
                          width: width * 0.80,
                          child: Center(
                              child: Text(
                            'Step 2 of 3',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          )),
                        ),
                      ],
                    ),
                    Container(
                      height: height * 0.05,
                      child: Column(
                        //crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Container(
                            child: Text('Select your fitness level',
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold)),
                          )
                        ],
                      ),
                    ),
                    Container(
                      //height: height * 0.10,
                      width: width,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text('Sedentary',
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                      ),
                    ),
                    Container(
                      //height: height * 0.20,
                      width: width,
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                  width: width * 0.60,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Text(
                                      'Little or no exercise, e.g., walking to bus',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )),
                              Container(
                                width: width * 0.30,
                                child: Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Checkbox(
                                        activeColor: Colors.white,
                                        value: level1,
                                        onChanged: (bool? value) {
                                          setState(() {
                                            levelval = 1.2;
                                            level1 = value;
                                            level2 = false;
                                            level3 = false;
                                            level4 = false;
                                            level5 = false;
                                          });
                                        })),
                              )
                            ],
                          ),
                          Divider(
                            height: 2,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        //height: height * 0.10,
                        width: width,
                        child: Container(
                          child: Text('Lightly Active',
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                      ),
                    ),
                    Container(
                      //height: height * 0.20,
                      width: width,
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                  width: width * 0.60,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Text(
                                      'Light exercise/sports 1-3 days/week',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )),
                              Container(
                                width: width * 0.30,
                                child: Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Checkbox(
                                        activeColor: Colors.white,
                                        value: level2,
                                        onChanged: (bool? value) {
                                          setState(() {
                                            levelval = 1.4;
                                            level2 = value;
                                            level3 = false;
                                            level4 = false;
                                            level5 = false;
                                            level1 = false;
                                          });
                                        })),
                              )
                            ],
                          ),
                          Divider(
                            height: 2,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        //height: height * 0.10,
                        width: width,
                        child: Container(
                          child: Text('Moderately Active',
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                      ),
                    ),
                    Container(
                      //height: height * 0.20,
                      width: width,
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                  width: width * 0.60,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Text(
                                      "Moderate exercise/sports 3-5 days/week",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )),
                              Container(
                                width: width * 0.30,
                                child: Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Checkbox(
                                        activeColor: Colors.white,
                                        value: level3,
                                        onChanged: (bool? value) {
                                          setState(() {
                                            levelval = 1.55;
                                            level3 = value;
                                            level2 = false;
                                            level1 = false;
                                            level4 = false;
                                            level5 = false;
                                          });
                                        })),
                              )
                            ],
                          ),
                          Divider(
                            height: 2,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        //height: height * 0.10,
                        width: width,
                        child: Container(
                          child: Text('Very Active',
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                      ),
                    ),
                    Container(
                      //height: height * 0.20,
                      width: width,
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                  width: width * 0.60,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Text(
                                      "Hard exercise/sports 6-7 days/week",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )),
                              Container(
                                width: width * 0.30,
                                child: Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Checkbox(
                                        activeColor: Colors.white,
                                        value: level4,
                                        onChanged: (bool? value) {
                                          setState(() {
                                            levelval = 1.7;
                                            level4 = value;
                                            level2 = false;
                                            level1 = false;
                                            level3 = false;
                                            level5 = false;
                                          });
                                        })),
                              )
                            ],
                          ),
                          Divider(
                            height: 2,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        //height: height * 0.10,
                        width: width,
                        child: Container(
                          child: Text('Extra Active',
                              style: TextStyle(
                                  fontSize: 16.0,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                      ),
                    ),
                    Container(
                      //height: height * 0.20,
                      width: width,
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                  width: width * 0.60,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Text(
                                      "Very hard exercise & physical job or 2x daily",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )),
                              Container(
                                width: width * 0.30,
                                child: Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Checkbox(
                                        activeColor: Colors.white,
                                        value: level5,
                                        onChanged: (bool? value) {
                                          setState(() {
                                            levelval = 1.80;
                                            level5 = value;
                                            level2 = false;
                                            level1 = false;
                                            level3 = false;
                                            level4 = false;
                                          });
                                        })),
                              )
                            ],
                          ),
                          Divider(
                            height: 2,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: height * 0.03,
                    ),
                    Container(
                      //margin: EdgeInsets.only(top: 5.0),
                      height: 50,
                      width: width * 0.80,
                      child: FlatButton(
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0),
                        ),
                        onPressed: () {
                          if (level1 == true) {
                            level = "Sedentary";
                            print("level " +
                                level +
                                "level val " +
                                levelval.toString() +
                                "id " +
                                widget.id.toString());
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => FitnessLevelThreeN(
                                    id: widget.id,
                                    goal: widget.goal,
                                    diet: widget.diet,
                                    level: level,
                                    levelval: levelval,
                                    name: widget.name,
                                    email: widget.email,
                                    password: widget.password)));
                          }
                          if (level2 == true) {
                            level = "Lightly Active";
                            print("level " +
                                level +
                                "level val " +
                                levelval.toString() +
                                "id " +
                                widget.id.toString());
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => FitnessLevelThreeN(
                                    id: widget.id,
                                    goal: widget.goal,
                                    diet: widget.diet,
                                    level: level,
                                    levelval: levelval,
                                    name: widget.name,
                                    email: widget.email,
                                    password: widget.password)));
                          }
                          if (level3 == true) {
                            level = "Moderately Active";
                            print("level " +
                                level +
                                "level val " +
                                levelval.toString() +
                                "id " +
                                widget.id.toString());
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => FitnessLevelThreeN(
                                    id: widget.id,
                                    goal: widget.goal,
                                    diet: widget.diet,
                                    level: level,
                                    levelval: levelval,
                                    name: widget.name,
                                    email: widget.email,
                                    password: widget.password)));
                          }
                          {
                            level = "Very Active";
                            print("level " +
                                level +
                                "level val " +
                                levelval.toString() +
                                "id " +
                                widget.id.toString());
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => FitnessLevelThreeN(
                                    id: widget.id,
                                    goal: widget.goal,
                                    diet: widget.diet,
                                    level: level,
                                    levelval: levelval,
                                    name: widget.name,
                                    email: widget.email,
                                    password: widget.password)));
                          }
                          {
                            level = "Extra Active";
                            print("level " +
                                level +
                                "level val " +
                                levelval.toString() +
                                "id " +
                                widget.id.toString());
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => FitnessLevelThreeN(
                                    id: widget.id,
                                    goal: widget.goal,
                                    diet: widget.diet,
                                    level: level,
                                    levelval: levelval,
                                    name: widget.name,
                                    email: widget.email,
                                    password: widget.password)));
                          }
                          if (level1 == false &&
                              level2 == false &&
                              level3 == false &&
                              level4 == false &&
                              level5 == false) {
                            print("Level must Select");
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return CustomAlertDialog(
                                    title: "Alert!",
                                    content: "Must Select",
                                  );
                                });
                          }
                        },
                        color: Colors.white,
                        textColor: Palette.backGround,
                        child: Text("Next".toUpperCase(),
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold)),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
