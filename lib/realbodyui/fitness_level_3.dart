import 'package:flutter/material.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/realbodyui/bmi_creen.dart';
import 'package:real_bodies/theme/palette.dart';
import 'package:real_bodies/ui/widgets/custom_alert_dialog.dart';
import 'package:http/http.dart' as http;

class FitnessLevelThreeN extends StatefulWidget {
  final int? id;
  final double? levelval;
  final String? goal;
  final String? diet;
  final String? level;
  final String? name;
  final String? email;
  final String? password;

  FitnessLevelThreeN(
      {this.id,
      this.levelval,
      this.goal,
      this.diet,
      this.level,
      this.name,
      this.email,
      this.password});

  @override
  _FitnessLevelThreeNState createState() => _FitnessLevelThreeNState();
}

class _FitnessLevelThreeNState extends State<FitnessLevelThreeN> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _ageController = TextEditingController();
  TextEditingController _heightController = TextEditingController();
  TextEditingController _weightController = TextEditingController();

  bool wedVal = false;
  double? height;
  late String gender;

  String? calorie;
  String bmi = "";
  double? bmr;
  int age = 0;
  double heightdb = 0;
  double weight = 0;
  double rmr = 0;

  bool _gender = true;

  URL urldomain = URL();

  void setbmi() async {
    try {
      var url = urldomain.domain + "add_bmi";
      final response = await http.get(url +
          "&id=" +
          widget.id.toString() +
          "&age=" +
          age.toString() +
          "&weight=" +
          weight.toString() +
          "&gender=" +
          gender +
          "&height=" +
          heightdb.toString() +
          "&goal=" +
          widget.goal! +
          "&diet=" +
          widget.diet! +
          "&level=" +
          widget.level! +
          "&bmi=" +
          bmi +
          "&calorie=" +
          calorie!);
      print('Response body:${response.body}');
      //n var jsonResponse = json.decode(response.body);
      // var requestresponse = jsonResponse['response'];

      if (response.body == "success") {
        print('Added  BMI');
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => Bmi(
                bmi: bmi,
                id: widget.id,
                calorie: calorie,
                name: widget.name,
                email: widget.email,
                password: widget.password,
                weight: weight.toString())));
      } else if (response.body == "error") {
        print("error  BMI");
      }
      print('Response body:${response.body}');
    } catch (e) {
      print('exception fitness_level_3 $e');
    }
  }

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double paddingLeft =
        MediaQuery.of(context).size.width - MediaQuery.of(context).padding.left;
    double heightWithoutAppBar = MediaQuery.of(context).size.height -
        kToolbarHeight -
        MediaQuery.of(context).padding.top;

    return Scaffold(
      backgroundColor: Color(0xffDE4922),
      body: ListView(
        children: <Widget>[
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Container(
                //color: Colors.yellow,
                //height: height * 0.60,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: height * 0.10,
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Container(
                          width: width * 0.80,
                          child: Center(
                              child: Text(
                            'Step 3 of 3',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          )),
                        ),
                      ],
                    ),
                    Container(
                      height: height * 0.14,
                      child: Column(
                        //crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Container(
                            child: Text('Personal Details',
                                style: TextStyle(
                                    fontSize: 26.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold)),
                          ),
                          SizedBox(
                            height: height * 0.01,
                          ),
                          Container(
                            child: Text(
                                'Let us know about you to speed up the result, ',
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold)),
                          ),
                          Container(
                            child: Text('Get fit your personal workout plan!',
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold)),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: height * 0.06,
                    ),
                    Form(
                      key: _formKey,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 18.0, right: 18.0),
                        child: Column(
                          children: [
                            Theme(
                              data: ThemeData(
                                primaryColor: Colors.white,
                                hintColor: Colors.white,
                              ),
                              child: TextFormField(
                                controller: _ageController,
                                style: TextStyle(color: Colors.white),
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  labelText: 'Age',
                                  labelStyle: TextStyle(color: Colors.white),
                                  focusColor: Colors.white,
                                  enabledBorder: const UnderlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.white, width: 0.0),
                                  ),
                                ),
                                cursorColor: Colors.white,
                                //style: TextStyle(color: Colors.white),
                              ),
                            ),
                            Theme(
                              data: ThemeData(
                                primaryColor: Colors.white,
                                hintColor: Colors.white,
                              ),
                              child: TextFormField(
                                controller: _heightController,
                                keyboardType: TextInputType.number,
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                  labelText: 'Height in cm',
                                  labelStyle: TextStyle(color: Colors.white),
                                  focusColor: Colors.white,
                                  enabledBorder: const UnderlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderSide: const BorderSide(
                                        color: Colors.white, width: 0.0),
                                  ),
                                ),
                                cursorColor: Colors.white,
                                //style: TextStyle(color: Colors.white),
                              ),
                            ),
                            Theme(
                              data: ThemeData(
                                primaryColor: Colors.white,
                                hintColor: Colors.white,
                              ),
                              child: TextFormField(
                                controller: _weightController,
                                keyboardType: TextInputType.number,
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                  labelText: 'Weight in Kgs',
                                  labelStyle: TextStyle(color: Colors.white),
                                  focusColor: Colors.white,
                                  enabledBorder: const UnderlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderSide: const BorderSide(
                                        color: Colors.white, width: 0.0),
                                  ),
                                ),
                                cursorColor: Colors.white,
                                //style: TextStyle(color: Colors.white),
                              ),
                            ),
                            SizedBox(
                              height: height * 0.04,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Gender',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: width * 0.045),
                                ),
                                Row(
                                  children: [
                                    _gender
                                        ? GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                _gender = false;
                                                print('selected mb $_gender');
                                              });
                                            },
                                            child: Container(
                                              height: height * 0.04,
                                              width: width * 0.15,
                                              decoration: BoxDecoration(
                                                  border: Border.all(
                                                      color: Colors.white)),
                                              child: Center(
                                                  child: Text(
                                                'Male',
                                                style: TextStyle(
                                                    color: Colors.white),
                                              )),
                                            ),
                                          )
                                        : GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                _gender = false;
                                                print('seleted mp $_gender');
                                              });
                                            },
                                            child: Container(
                                              height: height * 0.04,
                                              width: width * 0.15,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: Border.all(
                                                      color: Colors.white)),
                                              child:
                                                  Center(child: Text('Male')),
                                            ),
                                          ),
                                    _gender == true
                                        ? GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                _gender = true;
                                                print('selected bb');
                                              });
                                            },
                                            child: Container(
                                              height: height * 0.04,
                                              width: width * 0.15,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  border: Border.all(
                                                      color: Colors.white)),
                                              child:
                                                  Center(child: Text('Female')),
                                            ),
                                          )
                                        : GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                _gender = true;
                                                print('selected pb $_gender');
                                              });
                                            },
                                            child: Container(
                                              height: height * 0.04,
                                              width: width * 0.15,
                                              decoration: BoxDecoration(
                                                  border: Border.all(
                                                      color: Colors.white)),
                                              child: Center(
                                                  child: Text('Female',
                                                      style: TextStyle(
                                                          color:
                                                              Colors.white))),
                                            ),
                                          )
                                  ],
                                )
                              ],
                            ),
                            SizedBox(
                              height: height * 0.1,
                            ),
                            Container(
                              //margin: EdgeInsets.only(top: 5.0),
                              height: 50,
                              width: 350,
                              // color: Colors.green,
                              child: FlatButton(
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0),
                                ),
                                onPressed: () {
                                  print(" age " +
                                      age.toString() +
                                      " height " +
                                      heightdb.toString() +
                                      " weight " +
                                      weight.toString());

                                  try {
                                    age = int.parse(_ageController.text);
                                  } catch (e) {}

                                  try {
                                    weight =
                                        double.parse(_weightController.text);
                                  } catch (e) {}

                                  try {
                                    heightdb =
                                        double.parse(_heightController.text);
                                  } catch (e) {}
                                  //print("goal "+widget.goal+"diet "+widget.diet+"id "+widget.id.toString()+"level "+widget.level+" gender "+gender+" age "+age.toString()+" height "+heightdb);
                                  if (age != 0 &&
                                      heightdb != 0 &&
                                      weight != 0) {
                                    if (!_gender) {
                                      print("maaaleeeee");
                                      // double weightpoint = double.parse(weight);
                                      setState(() {
                                        double bmrval = 13.397 * weight +
                                            4.799 * height -
                                            5.677 * age +
                                            88.362;
                                        double total =
                                            bmrval * widget.levelval!;
                                        bmr = total;
                                        rmr = (9.99 * weight) +
                                            (6.25 * heightdb) -
                                            (4.92 * age) +
                                            5;
                                        rmr = rmr * widget.levelval!;
                                        gender = "Gender.Male";

                                        print(
                                            'fitness_level_3 male rmr = $rmr , fitness level ${widget.levelval}');
                                      });
                                    }
                                    if (_gender) {
                                      print("femmmmaaleee");
                                      setState(() {
                                        double bmrval = 9.247 * weight +
                                            3.098 * height -
                                            4.330 * age +
                                            447.593;
                                        double total =
                                            bmrval * widget.levelval!;

                                        rmr = (9.99 * weight) +
                                            (6.25 * heightdb) -
                                            (4.92 * age) +
                                            161;
                                        rmr = rmr * widget.levelval!;
                                        gender = "Gender.Female";
                                        // bmr=total.toStringAsFixed(2);
                                        bmr = total;
                                        print(
                                            'fitness_level_3 female bmr  = $bmr');
                                      });
                                    }
                                    if (widget.goal == "Weight Loss") {
                                      double cal = rmr - (rmr / 100 * 20);
                                      calorie = cal.toStringAsFixed(2);
                                    } else if (widget.goal == "Mass Gain") {
                                      double cal = rmr + (rmr / 100 * 20);
                                      calorie = cal.toStringAsFixed(2);
                                    } else {
                                      double cal = rmr;
                                      calorie = cal.toStringAsFixed(2);
                                    }
                                    double heightsuqare = height * height;
                                    // double bmival = weight / heightsuqare;
                                    double bmival = weight /
                                        ((heightdb / 100) * (heightdb / 100));
                                    bmi = bmival.toStringAsFixed(2);
                                    setbmi();
                                    print("Data Added");
                                    print("kkkkkkkk" + calorie!);
                                    print("gendre" + _gender.toString());
                                    print("age" + age.toString());
                                  } else {
                                    print("bahrrr");
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return CustomAlertDialog(
                                            title: "Alert!",
                                            content: "Must Fill the Fields",
                                          );
                                        });
                                  }
                                },
                                color: Colors.white,
                                textColor: Palette.backGround,
                                child: Text("Almost there !".toUpperCase(),
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold)),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
