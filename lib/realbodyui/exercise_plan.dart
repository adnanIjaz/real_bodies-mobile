import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:real_bodies/models/exercise.dart';
import 'package:real_bodies/models/program.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/provider/date_provider.dart';
import 'package:real_bodies/realbodyui/program_information.dart';
import 'package:real_bodies/services/date_time.dart';
import 'package:real_bodies/services/development_utilities/custom_print.dart';
import 'package:real_bodies/services/text_utility.dart';
import 'package:real_bodies/theme/palette.dart';
import 'package:real_bodies/ui/screens/trainingpage.dart';
import 'package:http/http.dart' as http;

class ExercisePlan extends StatefulWidget {
  final int? id;
  final String? name;
  final String? calories;
  final String? weight;
  final String? weekDate;

  ExercisePlan({this.id, this.calories, this.name, this.weight, this.weekDate});

  @override
  _ExercisePlanState createState() => _ExercisePlanState();
}

class _ExercisePlanState extends State<ExercisePlan> {
  static const TAG = 'Exercise Plan';
  Future? selectedProgram;
  Future<List<ExerciseModel>>? exerciseModelList;
  Future<List?>? plan;
  CarouselSlider? carouselSlider;
  int _current = 0;
  List weeklyPlan = [
    //TrainingPage(),
    //TrainingPage(),
    Container(),
  ];

  List<T?> map<T>(List list, Function handler) {
    List<T?> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  void refresh() {
    setState(() {});
  }

  Future<List?> getExercisePlan(int? id) async {
    try {
      var currentWeek = DateUtility.getWeek(
          Provider.of<DateProvider>(context, listen: false).createdAtDate!);
      print('Exercise Plan current week: $currentWeek');

      print(id);
      print(currentWeek);
      URL urldomain = URL();
      var url = urldomain.domain + "get_exercise_plan_new";

      final response = await http.get(url + "&user_id=$id&week=$currentWeek");
      //  final response2 = await http.get(URL.urlmain+"get_exercise_week_plan"+"&id=2");
      //CustomPrint.printWrapped('$TAG ${response2.body}');
      CustomPrint.printWrapped(
          'exercise plan fetch plan Response body1:${response.body}');

      Iterable? list = json.decode(response.body);
      var parsedJson = json.decode(response.body) as List;
      print(parsedJson);
      List<ExerciseModel> completeExercise =
          parsedJson.map((e) => ExerciseModel.fromJson(e)).toList();

      //completeExercise.forEach((element) {print('exercise plan ${element.name}');});

      List<ExerciseModel> day1 = [];
      List<ExerciseModel> day2 = [];
      List<ExerciseModel> day3 = [];
      List<ExerciseModel> day4 = [];
      List<ExerciseModel> day5 = [];
      List<ExerciseModel> day6 = [];
      List<ExerciseModel> day7 = [];

      completeExercise.forEach((element) {
        print(element.runtimeType);
        // if(element.day == 1) {
        //   day1.add(element);
        // }
        switch (element.day) {
          case 1:
            {
              day1.add(element);
            }
            break;

          case 2:
            {
              day2.add(element);
            }
            break;

          case 3:
            {
              day3.add(element);
            }
            break;

          case 4:
            {
              day4.add(element);
            }
            break;

          case 5:
            {
              day5.add(element);
            }
            break;

          case 6:
            {
              day6.add(element);
            }
            break;

          case 7:
            {
              day7.add(element);
            }
            break;

          default:
            {
              print("Invalid choice");
            }
            break;
        }
      });
      print('length of day 1: ${day1.length}');

      List<List> tempDays = [day1, day2, day3, day4, day5, day6, day7];

      List views = [];
      tempDays.forEach((element) {
        if (element.isNotEmpty) {
          views.add(TrainingPage(
            week: currentWeek,
            exerciseList: element as List<ExerciseModel>?,
            trainingDay: '',
            calories: widget.calories,
            weight: widget.weight,
            id: widget.id,
          ));
        }
      });

      return views;
    } catch (e) {
      print('ExercisePlan getExercisePlan $e');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectedProgram = getProgramPlan(widget.id);
    plan = getExercisePlan(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Palette.mainPurple,
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: Center(
            child: Text("Exercise Plan"),
          )),
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 12,
                ),
                Center(
                    child: Text(
                  'Your Current Plan',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                )),
                SizedBox(
                  height: 12,
                ),
                FutureBuilder<ProgramPlan>(
                    future: selectedProgram as Future<ProgramPlan>?,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Padding(
                          padding:
                              const EdgeInsets.only(left: 40.0, right: 40.0),
                          child: Container(
                            height: 150,
                            decoration: BoxDecoration(
                              color: Color(0xffBFAAA5),
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(right: 20.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                    flex: 9,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 20.0, bottom: 8.0, left: 8.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          AutoSizeText(
                                            '${snapshot.data!.title}',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 22),
                                          ),
                                          AutoSizeText(
                                            TextUtils.parseHtmlString(
                                                snapshot.data!.description),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15),
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.center,
                                            maxLines: 3,
                                          ),
                                          Container(
                                            height: 30,
                                            width: 100,
                                            child: FlatButton(
                                              shape: new RoundedRectangleBorder(
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        30.0),
                                              ),
                                              onPressed: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ProgramInfo(
                                                            title: snapshot
                                                                .data!.title,
                                                            description: snapshot
                                                                .data!
                                                                .description,
                                                          )),
                                                );
                                              },
                                              color: Palette.buttonjColor,
                                              textColor: Colors.white,
                                              child: FittedBox(
                                                fit: BoxFit.contain,
                                                child: Text("View Your Plan",
                                                    style: TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }
                      return CircularProgressIndicator();
                    }),
                SizedBox(
                  height: 1,
                ),
              ],
            ),
          ),
          Expanded(
            flex: 6,
            child: FutureBuilder<List?>(
                future: plan,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return carouselSlider = CarouselSlider(
                      options: CarouselOptions(height: 370),
                      // height: 370.0,
                      // initialPage: 0,
                      // enlargeCenterPage: true,
                      // autoPlay: false,
                      // reverse: false,
                      // enableInfiniteScroll: false,
                      // autoPlayInterval: Duration(seconds: 2),
                      // autoPlayAnimationDuration: Duration(milliseconds: 2000),
                      // pauseAutoPlayOnTouch: Duration(seconds: 10),
                      // scrollDirection: Axis.horizontal,
                      // onPageChanged: (index) {
                      //   setState(() {
                      //     _current = index;
                      //   });
                      // },
                      items: snapshot.data!.map((weekPlan) {
                        return Builder(
                          builder: (BuildContext context) {
                            return Container(
                              width: MediaQuery.of(context).size.width,
                              child: weekPlan,
                            );
                          },
                        );
                      }).toList(),
                    );
                  } else {
                    Center(child: CircularProgressIndicator());
                  }
                  return Center(child: CircularProgressIndicator());
                }),
          ),
        ],
      ),
    );
  }
}

Future<ProgramPlan?> getProgramPlan(int? id) async {
  try {
    print(id);
    URL urldomain = URL();
    var url = urldomain.domain + "get_program";
    final response = await http.get(url + "&id=$id");
    print('exercise plan Response body:${response.body}');
    print('yes');
    var jsonResponse = json.decode(response.body);
    print('no');
    ProgramPlan p = ProgramPlan();
    if (jsonResponse.length > 0) {
      for (int i = 0; i <= jsonResponse.length - 1; i++) {
        p.title = jsonResponse[i]['title'];
        p.description = jsonResponse[i]['description'];
//      if(!_words.contains(jsonResponse[i]['name'])) {
//        _words.add(jsonResponse[i]['name']);
//      }

      }
    }
    return p;
  } catch (e) {
    print('Error ouccured this $e');
  }
}
