import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:real_bodies/models/exercise.dart';
import 'package:real_bodies/provider/exercise_queue_provider.dart';
import 'package:real_bodies/realbodieWidgets/queue_count_down_timer.dart';
import 'package:real_bodies/realbodieWidgets/rest_count_down_timer.dart';
import 'package:real_bodies/realbodyui/dashboard.dart';
import 'package:real_bodies/theme/palette.dart';
import 'package:wakelock/wakelock.dart';
import 'package:real_bodies/config/extentions.dart';

class ExerciseQueue extends StatefulWidget {
  //TODO: add round rest
  final int? index;
  final String? userId;
  final String? calories;
  final String? weight;
  final List<ExerciseModel>? exerciseList;

  ExerciseQueue(
      {this.calories, this.index, this.weight, this.userId, this.exerciseList});

  @override
  _ExerciseQueueState createState() => _ExerciseQueueState();
}

class _ExerciseQueueState extends State<ExerciseQueue>
    with SingleTickerProviderStateMixin {
  bool rest = true;

  finishRest() {
    setState(() {
      rest = false;
    });
    print('worked rest');
  }

  late AnimationController _floatBtnAnimController;
  bool _isPlaying = false;

  static const TAG = "Exercise Queue:";

  @override
  void initState() {
    super.initState();
    _floatBtnAnimController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    widget.exerciseList!.forEach((element) {
      print('$TAG ${element.name}');
    });
    print('$TAG length ${widget.exerciseList!.length}');
    setState(() {
      Wakelock.enable();
    });

    // print('Exercise (calories): '+widget.calories);
  }

  @override
  void dispose() {
    super.dispose();
    _floatBtnAnimController.dispose();
  }

  void _handleOnPressed() {
    setState(() {
      _isPlaying = !_isPlaying;
      _isPlaying
          ? _floatBtnAnimController.forward()
          : _floatBtnAnimController.reverse();
    });
    //CountDownTimer().startStopTimer();
  }

  getMessage() {
    if (widget.index == 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          AutoSizeText(
            'Get Ready For The Workout!',
            style: TextStyle(fontSize: 18, color: Palette.mainPurple),
          ),
          SizedBox(
            height: 8,
          ),
          AutoSizeText(
            'Next Exercise: ${widget.exerciseList![widget.index!].name}',
            style: TextStyle(fontSize: 18, color: Palette.mainPurple),
          ),
        ],
      );
    } else {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          AutoSizeText(
            'Take Rest',
            style: TextStyle(fontSize: 18, color: Palette.mainPurple),
          ),
          AutoSizeText(
            '&',
            style: TextStyle(fontSize: 18, color: Palette.mainPurple),
          ),
          AutoSizeText(
            'Get Ready For Next Exercise',
            style: TextStyle(fontSize: 18, color: Palette.mainPurple),
          ),
          SizedBox(
            height: 8,
          ),
          AutoSizeText(
            'Next Exercise: ${widget.exerciseList![widget.index!].name}',
            style: TextStyle(fontSize: 18, color: Palette.mainPurple),
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return ChangeNotifierProvider<ExerciseQueueProvider>(
      create: (context) => ExerciseQueueProvider(),
      child: SafeArea(
        child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  height: height * 0.01,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: IconButton(
                    icon: new Icon(Icons.arrow_back, color: Palette.mainPurple),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                ),
                Stack(
                  children: [
                    Column(
                      children: [
                        Container(
                          height: height * 0.05,
                          width: width * 0.8,
                          child: Center(
                            child: AutoSizeText(
                              '${widget.exerciseList![widget.index!].name!.capitalize()}',
                              style: TextStyle(
                                  color: Palette.mainPurple,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18),
                            ),
                          ),
                        ),
                        Container(
                          height: height * 0.03,
                        ),
                        Container(
                          width: width * 0.8,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              AutoSizeText(
                                '${widget.exerciseList![widget.index!].sets} sets',
                                style: TextStyle(
                                    fontSize: 18, color: Palette.mainPurple),
                              ),
                              AutoSizeText(
                                '${widget.exerciseList![widget.index!].reps} reps',
                                style: TextStyle(
                                    fontSize: 18, color: Palette.mainPurple),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: height * 0.43,
                          width: width * 0.95,
//            decoration: BoxDecoration(
//                color: Palette.primaryColor,
//
//                borderRadius: BorderRadius.all(
//                  Radius.circular(10.0),
//                ),
//                boxShadow: <BoxShadow>[
//                  const BoxShadow(
//                    color: const Color(0xFF000000),
//                    offset: Offset.zero,
//                    blurRadius: 5.0,
//                    spreadRadius: 0.0,
//                  ),
//                ]),
                          //TODO: replace the correct image
                          child: Image.network(
                              'https://www.topendsports.com/fitness/images/push-up-pixabay.jpg'),
                          //  child: Image.network(widget.exerciseList[widget.index].image),
                        ),
                      ],
                    ),
                    Consumer<ExerciseQueueProvider>(
                        builder: (context, exerciseQueueProvider, child) {
                      if (exerciseQueueProvider.rest) {
                        return Column(
                          children: [
                            Container(
                              height: height * 0.05,
                              width: width * 0.8,
                              color: Colors.grey[50],
                            ),
                            Container(
                              height: height * 0.03,
                            ),
                            Container(
                              width: width * 0.8,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  AutoSizeText(
                                    '${widget.exerciseList![widget.index!].sets} sets',
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.grey[50],
                                    ),
                                  ),
                                  AutoSizeText(
                                    '${widget.exerciseList![widget.index!].reps} reps',
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.grey[50],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              height: height * 0.43,
                              width: width * 0.95,
                              color: Colors.grey[50],
                              child: Center(
                                child: getMessage(),
                              ),
                            ),
                          ],
                        );
                      } else
                        return Container();
                    }),
                  ],
                ),
                Container(
                  height: height * 0.01,
                  width: width * 0.90,
                ),
                Container(
                  height: height * 0.33,
                  width: width * 0.95,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 2,

//                    child: FittedBox(
//                      fit: BoxFit.contain,
                        child: Consumer<ExerciseQueueProvider>(
                            builder: (context, exerciseQueueProvider, child) {
                          if (exerciseQueueProvider.rest) {
                            if (widget.index == 0) {
                              return RestCountDownTimer(
                                restDuration: 10,
                              );
                            } else if (widget
                                    .exerciseList![widget.index!].round !=
                                widget.exerciseList![widget.index! - 1].round) {
                              return RestCountDownTimer(restDuration: 50);
                            } else {
                              return RestCountDownTimer(restDuration: 30);
                            }
                          } else {
                            return QueueCountDownTimer(
                                userId: widget.userId,
                                calories: widget.calories,
                                weight: widget.weight,
                                exerciseList: widget.exerciseList,
                                index: widget.index);
                          }
                        }),
                        //   ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: SizedBox(
                            height: height * 0.055,
                            //  width:  height * 0.05,
                            child: FittedBox(
                              child: Consumer<ExerciseQueueProvider>(builder:
                                  (context, exerciseQueueProvider, child) {
                                if (exerciseQueueProvider.rest) {
                                  return FloatingActionButton(
                                    heroTag: 'nextbtn',
                                    child: Icon(Icons.arrow_forward),
                                    backgroundColor: Palette.mainPurple,
                                    foregroundColor: Colors.white,
                                    elevation: 5.0,
                                    onPressed: () {
                                      exerciseQueueProvider.finishRest();
                                    },
                                  );
                                } else {
                                  return FloatingActionButton(
                                    heroTag: 'nextbtn',
                                    child: Icon(Icons.arrow_forward),
                                    backgroundColor: Palette.mainPurple,
                                    foregroundColor: Colors.white,
                                    elevation: 5.0,
                                    onPressed: () {
                                      int queueIndex = widget.index! + 1;
                                      if (queueIndex <=
                                          widget.exerciseList!.length - 1) {
                                        Navigator.of(context).pushReplacement(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ExerciseQueue(
                                                      index: queueIndex,
                                                      calories: widget.calories,
                                                      weight: widget.weight,
                                                      userId: widget.userId,
                                                      exerciseList:
                                                          widget.exerciseList,
                                                    )));
                                      } else {
                                        Navigator.of(context)
                                            .pushReplacement(MaterialPageRoute(
                                                builder: (context) => DashBoard(
                                                  weight: widget.weight,
                                                      indexnumber: 1,
                                                      id: int.parse(
                                                          widget.userId!),
                                                      calorie: widget.calories,
                                                    )));
                                      }
                                    },
                                  );
                                }
                              }),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
