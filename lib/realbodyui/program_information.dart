import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:real_bodies/config/screen_utility.dart';
import 'package:real_bodies/services/text_utility.dart';
import 'package:real_bodies/theme/TextStyle.dart';
import 'package:real_bodies/theme/palette.dart';

class ProgramInfo extends StatelessWidget {
  final String? title;
  final String? description;

  ProgramInfo({this.title, this.description});

  @override
  Widget build(BuildContext context) {
    ScreenUtility().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Palette.mainPurple,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Center(
          child: Text(this.title!),
        ),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: ScreenUtility.blockSizeVertical * 5,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                'About this Program',
                style: TextStyle(
                  fontSize: ScreenUtility.blockSizeHorizontal * 5,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: ScreenUtility.blockSizeHorizontal * 80,
                height: ScreenUtility.blockSizeVertical * 50,
                child: AutoSizeText(
                  TextUtils.parseHtmlString(this.description),
                  style: CustomTextStyle.body1,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
