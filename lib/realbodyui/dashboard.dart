import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/models/user.dart';
import 'package:real_bodies/provider/exercise_diary_provider.dart';
import 'package:real_bodies/provider/food_log_provider.dart';
import 'package:real_bodies/provider/user_provider.dart';
import 'package:real_bodies/provider/weekly_progress_tracker_provider.dart';
import 'package:real_bodies/realbodyui/calender.dart';
import 'package:real_bodies/realbodyui/exercise_plan.dart';
import 'package:real_bodies/realbodyui/food_exercise_diary.dart';
import 'package:real_bodies/realbodyui/meal_plan.dart';
import 'package:real_bodies/realbodyui/setting.dart';
import 'package:real_bodies/realbodyui/shop.dart';
import 'package:real_bodies/realbodyui/show_weight.dart';
import 'package:real_bodies/realbodyui/support.dart';
import 'package:real_bodies/realbodyui/tracker_progress_weight.dart';
import 'package:real_bodies/theme/my_flutter_app_icons.dart';
import 'package:real_bodies/theme/palette.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class DashBoard extends StatefulWidget {
  final int? id;
  final String? name;
  final String? weight;
  final String? calorie;
  final String? date;
  final int? indexnumber;

  DashBoard({this.id,
    this.name,
    this.weight,
    this.calorie,
    this.indexnumber,
    this.date});

  @override
  _DashBoardState createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  Future<String>? userName;
  int? _selectedIndex = 0;
  List imgList = [];
  List weightList = [];
  URL urldomain = URL();

  checkinfo() async {
    try {
      print("id weight" + widget.id.toString());
      print(DateTime.now().toString());
      var url = urldomain.domain + "get_weight_record";
      final response = await http.get(url + "&id=" + widget.id.toString());
      print('Response body:${response.body}');
      var jsonResponse = json.decode(response.body);
      for (int i = 0; i < jsonResponse.length; i++) {
        imgList.add([
          urldomain.imgdomain + jsonResponse[i]['image'],
          jsonResponse[i]['date'],
          jsonResponse[i]['weight']
        ]);
// weightList.add(jsonResponse[i]['weight']);

      }
      print(imgList);
//return imgList;
    } catch (e) {
      print("Exception on way $e");
    }
  }

  Future<String> loadPasswordPreference() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String password = pref.getString('userName');
    return password;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print('Dashboard calories: ${widget.calorie}');
    // checkinfo();
    _selectedIndex = widget.indexnumber;

    userName = loadPasswordPreference();
    print('yesssssssssssssssssssssssssssss${widget.id}');
    print('calories ${widget.calorie}');
  }

  @override
  Widget build(BuildContext context) {
    final _tabs = [
      storeTab(context, widget.name, userName, widget.id, widget.calorie,
          widget.weight),
      ExercisePlan(
          id: widget.id,
          calories: widget.calorie,
          weight: widget.weight,
          weekDate: widget.date),
      FoodExerciseDiary(
          id: widget.id, calorie: widget.calorie, weight: widget.weight),
      ProgressTrackerWeight(id: widget.id, weight: widget.weight),
      MealPlan(id: widget.id),
      ShowWeight(
        id: widget.id,
        weight: widget.weight,
      )
    ];

    return Scaffold(
        backgroundColor: Palette.boldTextO,
        body: MultiProvider(providers: [
          ChangeNotifierProvider<FoodLogProvider>(
            create: (context) => FoodLogProvider(widget.id),
            lazy: false,
          ),
          ChangeNotifierProvider<ExerciseDiaryProvider>(
            create: (context) => ExerciseDiaryProvider(widget.id.toString()),
            lazy: false,
          ),
          ChangeNotifierProvider<WeeklyProgressProvider>(
            create: (context) => WeeklyProgressProvider(widget.id),
            lazy: false,
          ),
        ], child: _tabs[_selectedIndex!]),
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: Text(
                  'Home',
                )),
            BottomNavigationBarItem(
                icon: Icon(Icons.directions_run),
                title: Text(
                  'Training',
                )),
            BottomNavigationBarItem(
                icon: Icon(Icons.developer_board),
                title: Text(
                  'Dairy',
                )),
            BottomNavigationBarItem(
                icon: Icon(Icons.equalizer),
                title: Text(
                  'Progress',
                )),
            BottomNavigationBarItem(
                icon: Icon(MyFlutterApp.dinner), title: Text("Food")),
            BottomNavigationBarItem(
                icon: Icon(Icons.shutter_speed),
                title: Text(
                  'Weight',
                )),
          ],
          currentIndex: _selectedIndex!,
          type: BottomNavigationBarType.fixed,
          fixedColor: Palette.boldTextO,
          onTap: _onItemTapped,
        ));
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}

Widget storeTab(BuildContext context, String? name, Future<String>? userName,
    int? id, String? calorie, String? weight) {
  double width = MediaQuery.of(context).size.width;
  double height = MediaQuery.of(context).size.height;
  // will pick it up from here
  // am to start another template

  return ListView(children: <Widget>[
    Container(
      width: width,
      height: height * 0.22,
      //color: Colors.blue,
      child: Column(
        children: <Widget>[
          Center(
              child: Text(
            "REAL BODIES",
            style: TextStyle(
                fontSize: 34.0,
                fontWeight: FontWeight.w700,
                color: Colors.white),
          )),
          Padding(
            padding: EdgeInsets.all(15.0),
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(height: 60.0, width: 60.0),
                    InkWell(
                      splashColor: Colors.blue,
                      borderRadius: BorderRadius.circular(15.0),
                      onTap: () {
                        print("tapped");
                        /*  Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => UserProfile(image:widget.image,name:widget.name,gender:widget.gender,old:widget.old,height:widget.height,weight:widget.weight)));
                   */
                      },
                      child: Container(
                        height: 60.0,
                        width: 60.0,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/images/fit2.jpg'),
                                //NetworkImage(widget.image) ,//
                                fit: BoxFit.cover),
                            borderRadius: BorderRadius.circular(30.0)),
                      ),
                    ),
                    Positioned(
                      left: 5.0,
                      top: 40.0,
                      child: Container(
                        height: 15.0,
                        width: 15.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(7.5),
                            color: Colors.green,
                            border: Border.all(
                                color: Colors.white,
                                style: BorderStyle.solid,
                                width: 1.0)),
                      ),
                    )
                  ],
                ),
                FutureBuilder(
                    future: userName,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return SizedBox(
                          height: height * 0.12,
                          width: width * 0.6,
                          child: FittedBox(
                            fit: BoxFit.contain,
                            child: Text(
                              "  WELCOME ${snapshot.data.toString().toUpperCase()}",
                              style: TextStyle(
                                  fontSize: 30.0,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white),
                            ),
                          ),
                        );
                      } else {
                        return Text(
                          "WELCOME ", //$name",
                          style: TextStyle(
                              fontSize: 30.0,
                              fontWeight: FontWeight.w700,
                              color: Colors.white),
                        );
                      }
                    })
              ],
            ),
          ),
        ],
      ),
    ),
    Container(
      height: height * 0.70,
      // color: Colors.yellow,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Card(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                  child: GestureDetector(
                    onTap: () {},
                    child: Container(
                      //  color: Colors.blue,
                      height: height * 0.1,
                      width: width * 0.2,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.home,
                            color: Colors.white,
                          ),
                          Text(" "),
                          Text(
                            "Home",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Card(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ShowWeight(id: id)));
                    },
                    child: Container(
                      //  color: Colors.blue,
                      height: height * 0.1,
                      width: width * 0.2,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.shutter_speed,
                            color: Colors.white,
                          ),
                          Text(" "),
                          Text(
                            "Weight",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Card(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ExercisePlan(id: id)));
                    },
                    child: Container(
                      //  color: Colors.blue,
                      height: height * 0.1,
                      width: width * 0.2,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.list,
                            color: Colors.white,
                          ),
                          Text(" "),
                          FittedBox(
                              fit: BoxFit.contain,
                              child: Text(
                                "Training Plan ",
                                style: TextStyle(color: Colors.white),
                              )),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Card(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) => MultiProvider(
                                    providers: [
                                      ChangeNotifierProvider<
                                          WeeklyProgressProvider>(
                                        create: (context) =>
                                            WeeklyProgressProvider(id),
                                        lazy: false,
                                      ),
                                      ChangeNotifierProvider<FoodLogProvider>(
                                        create: (context) =>
                                            FoodLogProvider(id),
                                        lazy: false,
                                      ),
                                      ChangeNotifierProvider<
                                          ExerciseDiaryProvider>(
                                        create: (context) =>
                                            ExerciseDiaryProvider(
                                                id.toString()),
                                        lazy: false,
                                      ),
                                    ],
                                    child: ProgressTrackerWeight(
                                      id: id,
                                      weight: weight,
                                    ))),
                      );
                    },
                    child: Container(
                      //  color: Colors.blue,
                      height: height * 0.1,
                      width: width * 0.2,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.equalizer,
                            color: Colors.white,
                          ),
                          Text(" "),
                          FittedBox(
                              fit: BoxFit.contain,
                              child: Text(
                                "Training Stats",
                                style: TextStyle(color: Colors.white),
                              )),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Card(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => MealPlan(id: id)));
                    },
                    child: Container(
                      //  color: Colors.blue,
                      height: height * 0.1,
                      width: width * 0.2,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            MyFlutterApp.free_breakfast,
                            color: Colors.white,
                          ),
                          Text(" "),
                          Text(
                            "Meal Plan",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Card(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => Calender()));
                    },
                    child: Container(
                      //  color: Colors.blue,
                      height: height * 0.1,
                      width: width * 0.2,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.calendar_today,
                            color: Colors.white,
                          ),
                          Text(" "),
                          Text(
                            "Schedule",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Card(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => Shop(id: id)));
                    },
                    child: Container(
                      //  color: Colors.blue,
                      height: height * 0.1,
                      width: width * 0.2,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.local_grocery_store,
                            color: Colors.white,
                          ),
                          Text(" "),
                          Text(
                            "Shop",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Card(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ExercisePlan(id: id)));
                    },
                    child: Container(
                      //  color: Colors.blue,
                      height: height * 0.1,
                      width: width * 0.2,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.directions_walk,
                            color: Colors.white,
                          ),
                          Text(" "),
                          Text(
                            "Exercise",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Card(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                  child: Container(
                    //  color: Colors.blue,
                    height: height * 0.1,
                    width: width * 0.2,

                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.lightbulb_outline,
                          color: Colors.white,
                        ),
                        Text(" "),
                        Text(
                          "Tips",
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Card(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => Setting()));
                    },
                    child: Container(
                      //  color: Colors.blue,
                      height: height * 0.1,
                      width: width * 0.2,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.settings_applications,
                            color: Colors.white,
                          ),
                          Text(" "),
                          Text(
                            "Setting",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Card(
                  color: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => Support()));
                    },
                    child: Container(
                      //  color: Colors.blue,
                      height: height * 0.1,
                      width: width * 0.2,

                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.not_listed_location,
                            color: Colors.white,
                          ),
                          Text(" "),
                          Text(
                            "Support",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Container(
                  //  color: Colors.blue,
                  height: height * 0.1,
                  width: width * 0.2,

                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      /* Icon(Icons.home,color: Colors.white,),
                              Text(" "),
                              Text("Home",style: TextStyle(color: Colors.white),), */
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    )
  ]);
}
