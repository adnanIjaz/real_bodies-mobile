import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:real_bodies/models/chart_data.dart';
import 'package:real_bodies/models/macro_nutrient.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/models/weekly_meal.dart';
import 'package:real_bodies/pages/exercise_diary.dart';
import 'package:real_bodies/pages/food_diary.dart';
import 'package:real_bodies/pages/progress_tracker.dart';
import 'package:real_bodies/provider/exercise_diary_provider.dart';
import 'package:real_bodies/provider/weekly_progress_tracker_provider.dart';
import 'package:real_bodies/services/database.dart';
import 'package:real_bodies/services/date_time.dart';
import 'package:real_bodies/services/development_utilities/custom_print.dart';
import 'package:real_bodies/services/real_bodies_in_app_utilities.dart';
import 'package:real_bodies/theme/palette.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:shimmer/shimmer.dart';

import '../services/date_time.dart';

class FoodExerciseDiary extends StatefulWidget {
  final int? id;
  final String? weight;
  final String? calorie;

  FoodExerciseDiary({this.id, this.calorie, this.weight});

  @override
  _FoodExerciseDiaryState createState() => _FoodExerciseDiaryState();
}

class _FoodExerciseDiaryState extends State<FoodExerciseDiary>
    with WidgetsBindingObserver {
  static const String TAG = 'Food_Exercise_Diary:';

  final dbHelper = DatabaseHelper.instance;

  MacroNutrients targetNutrients =
      MacroNutrients(proteins: 0, carbohydrates: 0, fats: 0);
  MacroNutrients actualNutrients =
      MacroNutrients(proteins: 0, carbohydrates: 0, fats: 0);

  double totalNutrients = 0;
  String takeCal = "0";
  String remaining = "0";
  String? trainingWeek;
  String? caloriesBurnt = "0";

  int? touchedIndex;

  var sample;

  void refresh() {
    setState(() {
      checkinfo();
    });
  }

  getNutrients() async {
    try {
      print('food_exercise_diary fetch meal plan: ${widget.id}');
      String date = await DateUtility.getCreatedAtDate();
      print('meal plan created at date: $date');
      //print('Time before call ${DateTime.now()}');
      final response2 = await http.get(URL.mealUrl +
          'get_nutrients' +
          "&user_id=${widget.id}" +
          "&week=" +
          DateUtility.getWeek(date));
      //print('Time after call ${DateTime.now()}');
      //print('Response2 body: ${response2.body}');
      CustomPrint.printWrapped('response2:${response2.body}');
      var jsonResponse = json.decode(response2.body);
      var meal = jsonResponse['meal'];

      print(json.decode(json.encode(meal)));
      Iterable list = json.decode(json.encode(meal));

      List<WeeklyMealPlan> wmpl =
          list.map((model) => WeeklyMealPlan.fromJson(model)).toList();
      wmpl.forEach((f) {
        print(f.carbohydrate);

        targetNutrients.proteins =
            targetNutrients.proteins! + double.parse(f.proteins!);

        targetNutrients.fats = targetNutrients.fats! + double.parse(f.fats!);
        targetNutrients.carbohydrates =
            targetNutrients.carbohydrates! + double.parse(f.carbohydrate!);
//      print(f.carbohydrate);
      });
      print('nutrients carbs ' + targetNutrients.carbohydrates.toString());
      // if(this.mounted)
      setState(() {
        totalNutrients = targetNutrients.proteins! +
            targetNutrients.fats! +
            targetNutrients.carbohydrates!;
      });
      return wmpl;
      //  wmpl.forEach((f)=>print(f.type));

    } catch (e) {
      print('Food Exercise Diary Error ouccured $e');
      //throw Exception("End of line error");

    }
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    try {
      if (int.parse(s) is int) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print('food_exercise_diary (isNumeric): cannot see for numerics');
      return false;
    }
  }

  URL urldomain = URL();

  void addWeeklySummary(String actual, String? remaining) async {
    try {
      String week = await DateUtility.getWeekDirect();
      String date = DateUtility.getCurrentDateWithDbFormat();
      final response = await http.get(URL.weeklyProgress +
          "insert_weekly_progress" +
          "&user_id=" +
          widget.id.toString() +
          "&week=$week" +
          "&date=$date" +
          "&goal=${widget.calorie}" +
          "&actual=$actual" +
          "&remaining=$remaining");
      print('addWeeklySummary Response body = ${response.body}');
    } catch (e) {
      print('$TAG addWeeklySummary: $e');
    }
  }

  Future checkinfo() async {
    try {
      trainingWeek = await DateUtility.getWeekDirect();
      print("id diary" + widget.id.toString());
      //print(DateTime.now().toString());
      var url = urldomain.domain + "get_food_record";
      final response = await http.get(url + "&id=" + widget.id.toString());
      print(url + "&id=" + widget.id.toString());
      print('$TAG Response body  hh:${response.body}');

      if (isNumeric(response.body)) {
        print('integer parsed');
        takeCal = '0';
        actualNutrients.proteins = 0;
        actualNutrients.carbohydrates = 0;
        actualNutrients.fats = 0;
        caloriesBurnt = response.body;
        //takeCal = (double.parse(takeCal) -  double.parse(caloriesBurnt)).toInt().toString();
        remaining = (double.parse(widget.calorie!) -
                double.parse(takeCal) +
                double.parse(caloriesBurnt!))
            .toStringAsFixed(2);

        addWeeklySummary(takeCal, remaining);
        String date = DateUtility.getCurrentDateWithDbFormat();

        int count = await dbHelper.alreadyPresent(date);
        print('$TAG count = $count');
        if (count != null) {
          print('$TAG: yes');
          // _update(takeCal,remaining);
        } else {
          print('$TAG: No');
          //_insert(takeCal, remaining);
        }
      } else if (response.body == "null") {
        takeCal = '0';
        actualNutrients.proteins = 0;
        actualNutrients.carbohydrates = 0;
        actualNutrients.fats = 0;
        print(takeCal + " val");
        addWeeklySummary(takeCal, widget.calorie);
      } else {
        print('Food exercise diary Else has been entered ');
        var jsonResponse = json.decode(response.body);
        var takecalo = jsonResponse[0]["totalIntaked"];
        var calorieExercise = jsonResponse[0]["excerciseburned"];
        actualNutrients.proteins =
            double.parse(jsonResponse[0]["totalConsumed_protein"]);
        actualNutrients.fats =
            double.parse(jsonResponse[0]["totalConsumed_fats"]);
        actualNutrients.carbohydrates =
            double.parse(jsonResponse[0]["totalConsumed_carbo"]);
        takeCal = takecalo;
        caloriesBurnt = calorieExercise;
        if (caloriesBurnt == null || caloriesBurnt == "") {
          caloriesBurnt = "0";
        }
        //takeCal = (double.parse(takeCal) -  double.parse(caloriesBurnt)).toInt().toString();
        // await DBProvider.db.insertTarget(takeCal);
        print("$TAG takecal" + takecalo);
        remaining = (double.parse(widget.calorie!).toInt() -
            double.parse(takeCal) +
            double.parse(caloriesBurnt!))
            .toInt()
            .toString();
        var date = DateUtility.getCurrentDateWithDbFormat();
        int count = await dbHelper.alreadyPresent(date);
        print('$TAG count = $count');
        addWeeklySummary(takeCal, remaining);
        if (count != null) {
          //_update(takeCal,remaining);
          print('$TAG: Already present');
        } else {
          print('$TAG: Not present');
          //_insert(takeCal, remaining);
        }
      }
      Provider.of<WeeklyProgressProvider>(context, listen: false)
          .query(widget.id);
//  setState(() {

//  });

      //refresh();
      return 's';
    } catch (e) {
      print(" food exercise diary $e");
    }
  }

  void _insert(String actual, String remaining) async {
    // row to insert
    String date = DateUtility.getCurrentDateWithDbFormat();
    String week = await DateUtility.getWeekDirect();
    Map<String, dynamic> row = {
      DatabaseHelper.columnDate: date,
      DatabaseHelper.columnGoal: widget.calorie,
      DatabaseHelper.columnActual: actual,
      DatabaseHelper.columnRemaining: remaining,
      DatabaseHelper.columnWeek: week
    };
    final id = await dbHelper.insert(row);
    print('food_exercise_diray: inserted row id: $id');
  }

  void _update(String actual, String remaining) async {
    String date = DateUtility.getCurrentDateWithDbFormat();
    String week = await DateUtility.getWeekDirect();
    Map<String, dynamic> row = {
      DatabaseHelper.columnDate: date,
      DatabaseHelper.columnGoal: widget.calorie,
      DatabaseHelper.columnActual: actual,
      DatabaseHelper.columnRemaining: remaining,
      DatabaseHelper.columnWeek: week
    };
    final rowsAffected = await dbHelper.update(row);
    print('$TAG updated $rowsAffected row(s)');
  }

  late List<charts.Series<ChartData, String>> _seriesPieData;

  _generateData() {
    var pieData = [
      ChartData(
          'Protein',
          double.parse(((targetNutrients.proteins! / totalNutrients) * 100)
              .toStringAsFixed(1)),
          Colors.blue),
      ChartData(
          'Carbs',
          double.parse(((targetNutrients.carbohydrates! / totalNutrients) * 100)
              .toStringAsFixed(1)),
          Colors.green),
      ChartData(
          'Fats',
          double.parse(((targetNutrients.fats! / totalNutrients) * 100)
              .toStringAsFixed(1)),
          Colors.amber),
    ];
    _seriesPieData.add(charts.Series(
      data: pieData,
      domainFn: (ChartData chartData, _) => chartData.nutrient,
      measureFn: (ChartData chartData, _) => chartData.value!,
      colorFn: (ChartData chartData, _) =>
          charts.ColorUtil.fromDartColor(chartData.color),
      id: 'Nutrients graph',
      labelAccessorFn: (ChartData chartData, _) => '${chartData.value}',
    ));
  }

  @override
  void initState() {
    super.initState();
    Provider.of<ExerciseDiaryProvider>(context, listen: false)
        .getLoggedExercises(widget.id.toString());
    WidgetsBinding.instance!.addObserver(this);
    print('this is calorie${widget.calorie}');
    print(widget.id);
    sample = getNutrients();
    var s = checkinfo();
    // checkinfo();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance!.removeObserver(this);

    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      //if(this.mounted)
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    _seriesPieData = [];
    _generateData();

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Palette.mainPurple,
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: Center(
            child: Text("Food/Exercise Diary"),
          )),
      body: FutureBuilder(
          future: checkinfo(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: <Widget>[
                  Column(
                    children: <Widget>[
//        Container(
//          height: height * 0.06,
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//
//                 FittedBox(
//                  fit: BoxFit.cover,
//                  child: IconButton(icon: Icon(Icons.arrow_back_ios),
//                      color: Colors.black,
//                      onPressed: ()=>{}),
//                ),
//              Text('Today', style: TextStyle(color: Colors.black),),
//              FittedBox(
//                fit: BoxFit.cover,
//                child: IconButton(icon: Icon(Icons.arrow_forward_ios),
//                    color: Colors.black,
//                    onPressed: ()=>{}),
//              ),
//            ],
//          ),
//        ),

                      Container(
                        // color: Colors.blue,
                        height: 20,

                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  convertDateFromString(
                                      DateTime.now().toString()),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17.0),
                                )),
                            Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "Training Week $trainingWeek",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17.0),
                                )),
                          ],
                        ),
                      ),
                      Container(
                        // color: Colors.blue,
                        height: height * 0.04,
                        width: width * 0.95,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "Total Calories Aim Today",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                            Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  RBUtility.getPreciseCalories(widget.calorie!),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.red),
                                )),
                          ],
                        ),
                      ),
                      Container(
                        //  color: Colors.blue,
                        height: height * 0.04,
                        width: width * 0.95,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "Food Calories",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                            Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  takeCal,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                          ],
                        ),
                      ),
                      Container(
                        //  color: Colors.blue,
                        height: height * 0.04,
                        width: width * 0.95,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "Exercise",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                            Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "$caloriesBurnt",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                          ],
                        ),
                      ),

                      Container(
                        //  color: Colors.blue,
                        height: height * 0.04,
                        width: width * 0.95,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "Remaining",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                            Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  RBUtility.getPreciseCalories(remaining),
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                          ],
                        ),
                      ),

                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                        child: Card(
                          color: Colors.grey[300],
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                            side: BorderSide(
                              color: Colors.black,
                              width: 1.0,
                            ),
                          ),
                          elevation: 4.0,
                          child: Row(
                            children: <Widget>[
                              Container(
                                // color: Colors.blue,
                                height: height * 0.22,
                                width: width * 0.35,
//                              width:270.0/MediaQuery.of(context).devicePixelRatio,
//                              height:270.0/MediaQuery.of(context).devicePixelRatio,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FutureBuilder(
                                      future: sample,
                                      builder: (context, snapshot) {
                                        if (snapshot.hasData) {
                                          return Container(
                                            child: Row(
                                              children: <Widget>[
                                                const SizedBox(
                                                  height: 18,
                                                ),
                                                Expanded(
                                                  child: PieChart(
                                                    PieChartData(
//                                                        pieTouchData: PieTouchData(
//                                                            touchCallback: (pieTouchResponse) {
//                                                              //if(this.mounted) {
//                                                                setState(() {
//                                                                  if (pieTouchResponse
//                                                                      .touchInput is FlLongPressEnd ||
//                                                                      pieTouchResponse
//                                                                          .touchInput is FlPanEnd) {
//                                                                    touchedIndex =
//                                                                    -1;
//                                                                  } else {
//                                                                    touchedIndex =
//                                                                        pieTouchResponse
//                                                                            .touchedSectionIndex;
//                                                                  }
//                                                                });
//                                                              //}
//                                                            }),
                                                        borderData:
                                                            FlBorderData(
                                                          show: false,
                                                        ),
                                                        sectionsSpace: 0,
                                                        centerSpaceRadius: 0,
                                                        sections: showingSections()
                                                            as List<
                                                                PieChartSectionData>),
                                                  ),
                                                ),
//                                              Column(
//                                                mainAxisSize: MainAxisSize.max,
//                                                mainAxisAlignment: MainAxisAlignment.end,
//                                                crossAxisAlignment: CrossAxisAlignment.start,
//                                                children: const <Widget>[
//                                                  Indicator(
//                                                    color: Color(0xff0293ee),
//                                                    text: 'First',
//                                                    isSquare: true,
//                                                  ),
//                                                  SizedBox(
//                                                    height: 4,
//                                                  ),
//                                                  Indicator(
//                                                    color: Color(0xfff8b250),
//                                                    text: 'Second',
//                                                    isSquare: true,
//                                                  ),
//                                                  SizedBox(
//                                                    height: 4,
//                                                  ),
//                                                  Indicator(
//                                                    color: Color(0xff845bef),
//                                                    text: 'Third',
//                                                    isSquare: true,
//                                                  ),
//                                                  SizedBox(
//                                                    height: 4,
//                                                  ),
//                                                  Indicator(
//                                                    color: Color(0xff13d38e),
//                                                    text: 'Fourth',
//                                                    isSquare: true,
//                                                  ),
//                                                  SizedBox(
//                                                    height: 18,
//                                                  ),
//                                                ],
//                                              ),
//                                           const SizedBox(
//                                             width: 28,
//                                           ),
                                              ],
                                            ),
                                          );
                                        } else
                                          return SizedBox(
                                            width: 200.0,
                                            height: 100.0,
                                            child: Shimmer.fromColors(
                                              baseColor: Colors.grey[400]!,
                                              highlightColor: Colors.grey[200]!,
                                              child: Container(
                                                  decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.black,
                                              )),
                                            ),
                                          );
                                      }),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  // color: Colors.blueGrey,
                                  height: height * 0.22,
                                  width: width * 0.50,
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        // color: Colors.blue,
                                        height: height * 0.03,
                                        width: width * 0.95,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Text(
                                                  "Macro Breakdown",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 18.0),
                                                )),
                                            Align(
                                                alignment: Alignment.center,
                                                child: Text(
                                                  "",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.grey),
                                                )),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        // color: Colors.blue,
                                        height: height * 0.02,
                                        width: width * 0.95,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Text(
                                                  "                    ",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                )),
                                            Align(
                                                alignment: Alignment.center,
                                                child: Text(
                                                  "Target",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.grey),
                                                )),
                                            Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Text(
                                                  "Actual",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.grey),
                                                )),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Divider(
                                          height: 2.0,
                                          thickness: 4.0,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      Container(
                                        // color: Colors.blue,
                                        height: height * 0.03,
                                        width: width * 0.95,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Row(
                                                  children: <Widget>[
                                                    Container(
                                                      height: 5,
                                                      width: 5,
                                                      decoration: BoxDecoration(
                                                          color: Colors.blue,
                                                          shape:
                                                              BoxShape.circle),
                                                    ),
                                                    Text(
                                                      " Proteins",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.grey),
                                                    ),
                                                  ],
                                                )),
                                            Align(
                                                alignment: Alignment.center,
                                                child: Text(
                                                  ((targetNutrients.proteins! /
                                                                  totalNutrients) *
                                                              100)
                                                          .isNaN
                                                      ? '--'
                                                      : ((targetNutrients.proteins! /
                                                                      totalNutrients) *
                                                                  100)
                                                              .toStringAsFixed(
                                                                  1) +
                                                          "%",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.grey),
                                                )),
                                            Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Text(
                                                  ((actualNutrients.proteins! /
                                                                      totalNutrients) *
                                                                  100)
                                                              .isInfinite ||
                                                          ((actualNutrients
                                                                          .proteins! /
                                                                      totalNutrients) *
                                                                  100)
                                                              .isNaN
                                                      ? '--'
                                                      : ((actualNutrients.proteins! /
                                                                      totalNutrients) *
                                                                  100)
                                                              .toStringAsFixed(
                                                                  1) +
                                                          "%",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.grey),
                                                )),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        // color: Colors.blue,
                                        height: height * 0.03,
                                        width: width * 0.95,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Row(
                                                  children: <Widget>[
                                                    Container(
                                                      height: 5,
                                                      width: 5,
                                                      decoration: BoxDecoration(
                                                          color: Colors.green,
                                                          shape:
                                                              BoxShape.circle),
                                                    ),
                                                    Text(
                                                      " Carbs    ",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.grey),
                                                    ),
                                                  ],
                                                )),
                                            Align(
                                                alignment: Alignment.center,
                                                child: Text(
                                                  ((targetNutrients.carbohydrates! /
                                                                  totalNutrients) *
                                                              100)
                                                          .isNaN
                                                      ? '--'
                                                      : ((targetNutrients.carbohydrates! /
                                                                      totalNutrients) *
                                                                  100)
                                                              .toStringAsFixed(
                                                                  1) +
                                                          "%",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.grey),
                                                )),
                                            Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Text(
                                                  ((actualNutrients.carbohydrates! /
                                                                      totalNutrients) *
                                                                  100)
                                                              .isInfinite ||
                                                          ((actualNutrients
                                                                          .carbohydrates! /
                                                                      totalNutrients) *
                                                                  100)
                                                              .isNaN
                                                      ? '--'
                                                      : ((actualNutrients.carbohydrates! /
                                                                      totalNutrients) *
                                                                  100)
                                                              .toStringAsFixed(
                                                                  1) +
                                                          "%",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.grey),
                                                )),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        // color: Colors.blue,
                                        height: height * 0.03,
                                        width: width * 0.95,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Row(
                                                  children: <Widget>[
                                                    Container(
                                                      height: 5,
                                                      width: 5,
                                                      decoration: BoxDecoration(
                                                          color: const Color(
                                                              0xfff8b250),
                                                          shape:
                                                              BoxShape.circle),
                                                    ),
                                                    Text(
                                                      " Fats      ",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.grey),
                                                    ),
                                                  ],
                                                )),
                                            Align(
                                                alignment: Alignment.center,
                                                child: Text(
                                                  ((targetNutrients.fats! /
                                                                  totalNutrients) *
                                                              100)
                                                          .isNaN
                                                      ? '--'
                                                      : ((targetNutrients.fats! /
                                                                      totalNutrients) *
                                                                  100)
                                                              .toStringAsFixed(
                                                                  1) +
                                                          "%",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.grey),
                                                )),
                                            Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Text(
                                                  ((actualNutrients.fats! /
                                                                      totalNutrients) *
                                                                  100)
                                                              .isInfinite ||
                                                          ((actualNutrients
                                                                          .fats! /
                                                                      totalNutrients) *
                                                                  100)
                                                              .isNaN
                                                      ? '--'
                                                      : ((actualNutrients.fats! /
                                                                      totalNutrients) *
                                                                  100)
                                                              .toStringAsFixed(
                                                                  1) +
                                                          "%",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.grey),
                                                )),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),

                      Container(
                        height: height * 0.1,
                        width: width * 0.97,
                        decoration: BoxDecoration(
                          // color: Palette.greyBackground,
                          borderRadius: BorderRadius.all(
                            Radius.circular(10.0),
                          ),
                        ),
                      ),
                    ],
                  ),

                  /////////// Progress Tracker (Training Week)/////////
                  ProgressTracker(
                    week: trainingWeek,
                  ),

                  ////////////// Food Diary////////////
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                        color: Palette.mainPurple,
                        borderRadius: BorderRadius.circular(15.0)),
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: Text(
                          'Food Diary',
                          style: TextStyle(color: Colors.white, fontSize: 30),
                        )),
                  ),
                  FoodDiary(
                      notifyParent: refresh,
                      id: widget.id,
                      calorie: widget.calorie,
                      incalorie: takeCal,
                      caloriesBurnt: caloriesBurnt,
                      remaining: remaining,
                      weight: widget.weight),

                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                        color: Palette.mainPurple,
                        borderRadius: BorderRadius.circular(15.0)),
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: Text(
                          'Exercise Diary',
                          style: TextStyle(color: Colors.white, fontSize: 30),
                        )),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  ExerciseDiary(
                    title: "Training Week $trainingWeek",
                    id: widget.id.toString(),
                  ),
                ],
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
    );
  }

  List<PieChartSectionData?> showingSections() {
    return List.generate(3, (i) {
      final isTouched = i == touchedIndex;
      final double fontSize = isTouched ? 25 : 16;
      final double radius = isTouched ? 60 : 50;
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: Colors.blue,
            value: double.parse(
                ((targetNutrients.proteins! / totalNutrients) * 100)
                    .toStringAsFixed(1)),
            title: double.parse(
                ((targetNutrients.proteins! / totalNutrients) * 100)
                        .toStringAsFixed(1))
                .toString(),
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 1:
          return PieChartSectionData(
            color: Colors.green,
            value: double.parse(
                ((targetNutrients.carbohydrates! / totalNutrients) * 100)
                    .toStringAsFixed(1)),
            title: double.parse(
                ((targetNutrients.carbohydrates! / totalNutrients) * 100)
                        .toStringAsFixed(1))
                .toString(),
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );
        case 2:
          return PieChartSectionData(
            color: const Color(0xfff8b250),
            value: double.parse(((targetNutrients.fats! / totalNutrients) * 100)
                .toStringAsFixed(1)),
            title: double.parse(((targetNutrients.fats! / totalNutrients) * 100)
                .toStringAsFixed(1))
                .toString(),
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize,
                fontWeight: FontWeight.bold,
                color: const Color(0xffffffff)),
          );

        default:
          return null;
      }
    });
  }
}

String convertDateFromString(String strDate) {
  DateTime todayDate = DateTime.parse(strDate);
  String formattedDate = DateFormat('EEEE ,MMM dd').format(todayDate);
  return formattedDate;
}
