import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:real_bodies/models/chart_data.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:real_bodies/models/food.dart';
import 'package:real_bodies/models/meal.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/pages/search_food.dart';
import 'package:real_bodies/provider/meal_data_provider.dart';
import 'package:real_bodies/realbodyui/dashboard.dart';
import 'package:real_bodies/realbodyui/food_exercise_diary.dart';
import 'package:real_bodies/theme/palette.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:http/http.dart' as http;
import 'package:real_bodies/ui/widgets/custom_alert_dialog.dart';
import 'package:real_bodies/Data/moor_database.dart' as hFood;
import 'package:real_bodies/ui/widgets/meal_dialogue.dart';



class AddMeal extends StatefulWidget {
  final Meal? mealFood;
  final int? id;
  final String? category;
  final String? calorie;
  final String? incalorie;
  final String? weight;
  final String? remaining;
  final String? caloriesBurnt;
  final bool? meal;

  AddMeal(
      {this.mealFood,
      this.id,
      this.category,
      this.calorie,
      this.incalorie,
      this.weight,
      this.remaining,
      this.caloriesBurnt,
      this.meal});

  @override
  _AddMealState createState() => _AddMealState();
}

class _AddMealState extends State<AddMeal> {

  int _quantity = 1;

  Map<String, double> dataMap = Map();
  List<Color> colorList = [
    Colors.red,
    Colors.green,
    Colors.blue,
    Colors.teal,
    Colors.purple,
    Colors.yellowAccent,
    Colors.deepOrange,
    // Colors.yellow,
  ];

  late List<charts.Series<ChartData, String>> _seriesPieData;

  _generateData() {
    var pieData = [
      ChartData(
          'Protein', double.parse(widget.mealFood!.protein!), Colors.blue),
      ChartData(
          'Carbs', double.parse(widget.mealFood!.carbohydrates!), Colors.green),
      ChartData('Fats', double.parse(widget.mealFood!.fat!), Colors.amber),
    ];
    _seriesPieData.add(charts.Series(
      data: pieData,
      domainFn: (ChartData chartData, _) => chartData.nutrient,
      measureFn: (ChartData chartData, _) => chartData.value!,
      colorFn: (ChartData chartData, _) =>
          charts.ColorUtil.fromDartColor(chartData.color),
      id: 'Nutrients graph',
      labelAccessorFn: (ChartData chartData, _) => '${chartData.value}',
    ));
  }





  @override
  Widget build(BuildContext context) {
    dataMap.putIfAbsent(
        "Flutter",
        () => (double.parse(widget.mealFood!.protein!) /
            (double.parse(widget.mealFood!.protein!) +
                double.parse(widget.mealFood!.carbohydrates!) +
                double.parse(widget.mealFood!.fat!)) *
            10));
    dataMap.putIfAbsent(
        "React",
        () =>
            double.parse(widget.mealFood!.carbohydrates!) /
            (double.parse(widget.mealFood!.protein!) +
                double.parse(widget.mealFood!.carbohydrates!) +
                double.parse(widget.mealFood!.fat!)) *
            10);
    dataMap.putIfAbsent(
        "Xa",
        () =>
            double.parse(widget.mealFood!.fat!) /
            (double.parse(widget.mealFood!.protein!) +
                double.parse(widget.mealFood!.carbohydrates!) +
                double.parse(widget.mealFood!.fat!)) *
            10);
    //dataMap.putIfAbsent("marin", () => widget.food.sodium * 0.01);
    //dataMap.putIfAbsent("Xamrin", () =>  double.parse(widget.mealFood.fat)Poly * 0.01);
    // dataMap.putIfAbsent("Xrin", () =>  double.parse(widget.mealFood.fat)Mono * 0.01);
    //  dataMap.putIfAbsent("Xn", () =>  double.parse(widget.mealFood.fat)Saturate * 0.01);
    //   dataMap.putIfAbsent("Ionic", () => 2);

    _seriesPieData = [];
    _generateData();

    void addFood() async {
      URL urlDomain = URL();
      try {
        var url = urlDomain.domainfood + 'get_food';
        final response =
            await http.get(url + "&name=" + widget.mealFood!.name!);
        print('Response body:${response.body}');
        if (response.body == 'sucess') {
          print('yes');
        }
      } catch(e){

      }
    }

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return ChangeNotifierProvider<MealDataProvider>(
      create: (context) => MealDataProvider(widget.mealFood!),
      lazy: false,
      child: Scaffold(
          appBar: AppBar(
              backgroundColor: Palette.mainPurple,
              automaticallyImplyLeading: false,
              centerTitle: true,
              title: Center(
                child: Text('Search Add Food'),
              )),
          body: //87
              Container(
            // height: height - kToolbarHeight -  MediaQuery.of(context).padding.top,
            width: width,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
//        Container(
//          height: height * 0.06,
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//
//                 FittedBox(
//                  fit: BoxFit.cover,
//                  child: IconButton(icon: Icon(Icons.arrow_back_ios),
//                      color: Colors.black,
//                      onPressed: ()=>{}),
//                ),
//              Text('Today', style: TextStyle(color: Colors.black),),
//              FittedBox(
//                fit: BoxFit.cover,
//                child: IconButton(icon: Icon(Icons.arrow_forward_ios),
//                    color: Colors.black,
//                    onPressed: ()=>{}),
//              ),
//            ],
//          ),
//        ),



                  Container(
                    // color: Colors.blue,
                    height: height * 0.04,
                    width: width * 0.95,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,

                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerRight,
                          child: Text(
                            "Total Calories Aim Today",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Text(

                              widget.calorie!,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red),
                            )),
                      ],
                    ),
                  ),
                  Container(
                    //  color: Colors.blue,
                    height: height * 0.04,
                    width: width * 0.95,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              "Food Calories",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              widget.incalorie!,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                      ],
                    ),
                  ),
                  Container(
                    //  color: Colors.blue,
                    height: height * 0.04,
                    width: width * 0.95,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,

                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              "Exercise",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                        Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              "-${widget.caloriesBurnt}",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                      ],
                    ),
                  ),
                  Container(
                    //  color: Colors.blue,
                    height: height * 0.04,
                    width: width * 0.95,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,

                      children: <Widget>[
                        Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              "Remaining",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Text(
                            widget.remaining!,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                  Container(
                    height: height * 0.22,
                    width: width * 0.95,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "${widget.mealFood!.name}",
                                style: TextStyle(
                                    // fontWeight: FontWeight.bold,
                                    fontSize: 22),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ),
                              Consumer<MealDataProvider>(
                                  builder: (context, provider, snapshot) {
                                    return InkWell(
                                      onTap: () {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return ChangeNotifierProvider<MealDataProvider>.value(
                                                child: MealDialogue(),
                                                value: provider,
                                              );
                                            }
                                        );
                                      },
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text('Size/Qty'),
                                          Consumer<MealDataProvider>(
                                              builder: (context, provider, snapshot) {
                                                return Text(
                                            '${provider.size!.round()}g/${provider.meal!.quantity}');
                                      }
                                          )
                                        ],
                                      ),
                                    );
                                  }
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text('Calories'),
                                  Consumer<MealDataProvider>(
                                      builder: (context, provider, snapshot) {
                                        return Text('${provider.meal!.calories}');
                                  }
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: PieChart(
                            dataMap: dataMap,
                            animationDuration: Duration(milliseconds: 800),
                            chartLegendSpacing: 32.0,
                            chartRadius:
                                MediaQuery.of(context).size.width / 2.7,
                            showChartValuesInPercentage: true,
                            showChartValues: true,
                            showChartValuesOutside: true,
                            chartValueBackgroundColor: Colors.grey[200]!,
                            colorList: colorList,
                            showLegends: false,
                            legendPosition: LegendPosition.right,
                            decimalPlaces: 1,
                            showChartValueLabel: true,
                            initialAngle: 0,
                            chartValueStyle: defaultChartValueStyle.copyWith(
                              color: Colors.blueGrey[900]!.withOpacity(0.9),
                            ),
                            chartType: ChartType.disc,
                          ),
//                    child: Container(height: 250,
//                      width: 250,
//                      child:  charts.PieChart(
//                        _seriesPieData,
//                        animate: true,
//                        animationDuration: Duration(seconds: 1),
//                          behaviors: [
//                                charts.DatumLegend(
////                                  outsideJustification:
////                                      charts.OutsideJustification.middle,
//                                  horizontalFirst: true,
//                                  desiredMaxRows: 1,
//                                  cellPadding: EdgeInsets.only(right: 2.0,),
//                                  entryTextStyle: charts.TextStyleSpec(
//                                    color: charts.MaterialPalette.black.darker,
//                                    fontFamily: 'Georgia',
//                                    fontSize: 8,
//                                  ),
//                                )
//                              ],
//                        defaultRenderer: new charts.ArcRendererConfig(
//                          arcWidth: 100,
//                          arcRendererDecorators: [
//                            charts.ArcLabelDecorator(
//                                labelPosition: charts.ArcLabelPosition.auto),
//                          ],
//                        ),
//                      ),
//                    ),
                        ),
                      ],
                    ),
                  ),




                  Divider(height: height * 0.01),
                  SizedBox(height: height * 0.01,),
                  Container(
                    height: height * 0.05,
                    width: width * 0.60,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('Protein'),
                        Text('${double.parse(widget.mealFood!.protein!)}g'),
                      ],
                    ),
                  ),

                  Container(
                    height: height * 0.05,
                    width: width * 0.60,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('Carbohydrates'),
                        Text(
                            '${double.parse(widget.mealFood!.carbohydrates!)}g'),
                      ],
                    ),),
                  Container(
                    height: height * 0.05,
                    width: width * 0.60,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('Fat'),
                        Text('${double.parse(widget.mealFood!.fat!)}g'),
                      ],
                    ),),
               Divider(),
                  Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        child:Text('Ingredients',style: TextStyle(fontWeight: FontWeight.bold),)
                      )),
                  SizedBox(height: 4,),
                  ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: widget.mealFood!.ingredients!.length,
                      itemBuilder: (BuildContext context, index) {
                        return Center(
                            child: Text(
                                '${widget.mealFood!.ingredients![index]}'));
                      }),

                  Container(
                    height: height * 0.12,
                    width: width * 0.8,
                    child: Center(
                      child:  Container(
                        //  height: height * 0.13,
                        //width: width * 0.8,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Consumer<MealDataProvider>(
                              builder: (context, provider, snapshot) {
                                return Container(
                                  height: height * 0.08,
                                  width:  width * 0.8,
                                  child: FlatButton(
                                    shape: new RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(30.0),
                                    ),
                                    onPressed: () async{
                                      saveMealOnServer(
                                        widget.id,
                                        widget.category!,
                                        widget.mealFood!.id,
                                        context,
                                        widget.mealFood!,
                                        provider.meal!.calories!,
                                        provider.meal!.protein!,
                                        provider.meal!.carbohydrates!);
                                    /*  showDialog(context: context,
builder: (BuildContext context) {
  return CustomAlertDialog(title: "Success!",content: "Food Added",);
});
print("calllll"+widget.calorie); */
//print("calllll"+widget.name);
                                    int count = 0;
                                    if (widget.meal!) {
                                      Navigator.popUntil(context, (route) {
                                        return count++ == 2;
                                      });
                                    } else if (widget.meal == null) {
                                      Navigator.popUntil(context, (route) {
                                        return count++ == 3;
                                      });
                                    } else {
                                      Navigator.popUntil(context, (route) {
                                          return count++ == 3;
                                        });
                                      }
                                      Navigator.of(context)
                                          .pushReplacement(MaterialPageRoute(builder: (context) => DashBoard(weight: widget.weight,id: widget.id,calorie: widget.calorie,indexnumber: 2)));

                                    },
                                    color: Palette.mainPurple,
                                    textColor: Colors.white,
                                    child: Text("add food".toUpperCase(),
                                        style: TextStyle(fontSize: 14,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                );
                              }
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void saveMealOnServer(
      int? id,
      String category,
      String? foodId,
      BuildContext context,
      Meal food,
      String calories,
      String proteins,
      String carbohydrates) async {
    hFood.DMeal historyMeal = hFood.DMeal(
      id: DateTime.now().microsecondsSinceEpoch,
      name: food.name,
      foodId: food.id,
    );
    final database =
        Provider.of<hFood.RealBodiesDatabase>(context, listen: false);
    print('meal details (AddMeal) ${widget.mealFood!.id}');
    print(id.toString());
    print(category);
    DateTime nowTime = DateTime.now();
    URL urldomain = URL();
    var url = urldomain.domainfood + "add_meals";
    var formatter = DateFormat('yyyy-MM-dd');
    String formatted = formatter.format(nowTime);
    final response = await http.get(url +
        "&id=" +
        id.toString() +
        "&food_id=" +
        foodId.toString() +
        "&category=" +
        category +
        "&meal=meal" +
        "&calories=" +
        calories +
        "&protein=" +
        proteins +
        "&carbohydrates=" +
        carbohydrates);
    print(url +
        "&id=" +
        id.toString() +
        "&food_id=" +
        foodId.toString() +
        "&category=" +
        category +
        "&meal=meal" +
        "&calories=" +
        calories +
        "&protein=" +
        proteins +
        "&carbohydrates=" +
        carbohydrates);
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON.
      // r Post.fromJson(json.decode(response.body));
      print('meal details (AddMeal) Response body:${response.body}');
      database.insertMeal(historyMeal);

    //  database.insertFood(historyFood);

    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }
}
