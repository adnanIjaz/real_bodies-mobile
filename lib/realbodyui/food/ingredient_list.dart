import 'package:flutter/material.dart';
import 'package:real_bodies/models/weekly_meal.dart';
import 'package:real_bodies/theme/palette.dart';

class IngredientScreen extends StatelessWidget {
  final List<Ingredients>? ingredientList;
  final String? title;

  IngredientScreen({this.ingredientList, this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Palette.mainPurple,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text(this.title!),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: 10,),
          Text('Ingredients',style: TextStyle(fontWeight: FontWeight.bold),),
          SizedBox(height: 10,),
          ListView.builder(
              shrinkWrap: true,
              itemCount: ingredientList!.length,
              itemBuilder: (BuildContext context, int index) {
                return Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    ingredientList![index].name!,
                    style: TextStyle(
                      color: Color(0xff94948d),
                    ),
                  ),
                );
              }),
        ],

      ),
    );
  }
}
