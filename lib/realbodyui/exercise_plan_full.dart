import 'package:flutter/material.dart';
import 'package:real_bodies/models/url.dart';
import 'package:real_bodies/pages/exercise.dart';
import 'package:real_bodies/realbodyui/dashboard.dart';
import 'package:real_bodies/services/date_time.dart';
import 'package:real_bodies/theme/palette.dart';
import 'package:http/http.dart' as http;

class ExerciseInfo extends StatefulWidget {
  final String? week;
  final String? reps;
  final String? sets;
  final String? weight;
  final String? name;
  final String? exerciseId;
  final String? duration;
  final String? image;
  final int? index;
  final String? userId;
  final String? calories;

  ExerciseInfo({this.calories,
    this.week,
    this.index,
    this.weight,
    this.reps,
    this.sets,
    this.name,
    this.exerciseId,
    this.image,
      this.duration,
      this.userId});
  @override
  _ExerciseInfoState createState() => _ExerciseInfoState();
}

class _ExerciseInfoState extends State<ExerciseInfo> {
  String? durationText;
  bool m = false;

  void logExercise(String? id, String? exerciseId) async {
    URL urldomain = URL();
    print(exerciseId);
    var url = urldomain.domain + "update_exercise";

    final response = await http.get(
        url + "&id=" + id.toString() + "&exercise_id=" + exerciseId.toString());
    print('Exercise plan full log exercise' +
        url +
        "&id=" +
        id.toString() +
        "&exercise_id=" +
        exerciseId.toString());

    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON.
      // r Post.fromJson(json.decode(response.body));
      print('Exercise plan full log exercise Response body:${response.body}');
    } else {
      // If that call was not successful, throw an error.
      throw Exception(
          'exercise plan full Failed to log exercise save exercise');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    durationText = DateUtility.getTimeInMinutes(widget.duration!);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Palette.mainPurple,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text("Exercise Plan Week ${widget.week}"),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
            child: Text(
              'Exercise No.${widget.index}. ${widget.name} ',
              style: TextStyle(fontSize: 30),
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              maxLines: 3,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 60.0, right: 90.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('${widget.sets} Sets', style: TextStyle(fontSize: 20)),
                Text('${widget.reps} Reps', style: TextStyle(fontSize: 20)),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: 150,
                  height: 50,
                  // color: Colors.orange,
                  child: m
                      ? Icon(Icons.done)
                      : FlatButton(
                          color: Palette.mainPurple,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12.0),
                            child: FittedBox(
                              fit: BoxFit.contain,
                              child: Text(
                                "Mark Complete",
                                softWrap: true,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  decoration: TextDecoration.none,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w800,
                                  fontFamily: "OpenSans",
                                ),
                              ),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                            side: BorderSide(
                              color: Colors.white,
                              width: 2,
                            ),
                          ),
                          onPressed: () {
                            m = true;
                            logExercise(widget.userId, widget.exerciseId);

                            int count = 0;
                            Navigator.popUntil(context, (route) {
                              return count++ == 1;
                            });
                            print(
                                'Exercise plan full: ${widget.calories}, ${widget.userId} weight= ');
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (context) => DashBoard(
                                        id: int.parse(widget.userId!),
                                        calorie: widget.calories,
                                        weight: widget.weight,
                                        indexnumber: 2)));
                          },
                        ),
                ),
                Container(
                  width: 150,
                  height: 50,
                  // color: Colors.orange,
                  child: FlatButton(
                    color: Palette.mainPurple,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 12.0),
                      child: FittedBox(
                        fit: BoxFit.contain,
                        child: Text(
                          "View Instructions",
                          softWrap: true,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            decoration: TextDecoration.none,
                            fontSize: 16,
                            fontWeight: FontWeight.w800,
                            fontFamily: "OpenSans",
                          ),
                        ),
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                      side: BorderSide(
                        color: Colors.white,
                        width: 2,
                      ),
                    ),
                    onPressed: () {},
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 100.0, right: 100.0),
            child: Container(
              height: 50,
              // color: Colors.orange,
              child: FlatButton(
                color: Color(0xffe0e0e0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12.0),
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.access_time),
                        Text(
                          " $durationText Mins",
                          softWrap: true,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            fontSize: 16,
                            fontWeight: FontWeight.w800,
                            fontFamily: "OpenSans",
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                  side: BorderSide(
                    color: Colors.white,
                    width: 2,
                  ),
                ),
                onPressed: () {},
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Center(
              child: Text(
            'Exercise ${widget.index} ${widget.name}',
            style: TextStyle(fontSize: 30),
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            maxLines: 3,
          )),
          SizedBox(
            height: 5,
          ),
          Center(
              child: Text(
            '${widget.sets} Sets',
            style: TextStyle(fontSize: 25),
          )),
          SizedBox(
            height: 5,
          ),
          Center(
              child: Text(
            '${widget.reps} Reps',
            style: TextStyle(fontSize: 25),
          )),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 80,
            //  child: CountDownTimer()
          ),
          Center(
            child: Container(
              width: 150,
              height: 50,
              // color: Colors.orange,
              child: FlatButton(
                color: Palette.mainPurple,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12.0),
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Text(
                      "Start",
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        decoration: TextDecoration.none,
                        fontSize: 16,
                        fontWeight: FontWeight.w800,
                        fontFamily: "OpenSans",
                      ),
                    ),
                  ),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                  side: BorderSide(
                    color: Colors.white,
                    width: 2,
                  ),
                ),
                onPressed: () {
                  //   print('Exercise_Plan_full image:'+widget.image);
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => Exercise(
                            image: widget.image,
                            name: widget.name,
                            duration: widget.duration,
                            reps: widget.reps,
                            sets: widget.sets,
                            exerciseId: widget.exerciseId,
                            userId: widget.userId,
                            calories: widget.calories,
                            weight: widget.weight,
                          )));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
